/**
 * to allow the angular cli to build the server-side bundle
 */
export { 
    AppServerModule
} from "./app/app.server.module";