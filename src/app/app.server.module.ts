import { 
    NgModule 
} from '@angular/core';

import { 
    ServerModule, 
    ServerTransferStateModule 
} from '@angular/platform-server';

import { 
    ModuleMapLoaderModule 
} from '@nguniversal/module-map-ngfactory-loader';

import { 
    AppBrowserModule 
} from './app.browser.module';

import { 
    AppComponent 
} from './app.component';

@NgModule({
    imports: [
        // The AppServerModule should import your AppModule followed
        // by the ServerModule from @angular/platform-server.
        AppBrowserModule,
        ServerModule, 
        ServerTransferStateModule, // PARA EVITAR QUE RESPONSE DE HTTP SEJAM DUPLICADOS (UM NO CLIENTE E UM NO SERVIDOR)
        ModuleMapLoaderModule // <-- *Important* to have lazy-loaded routes work
    ],
    providers: [

    ],
    // Since the bootstrapped component is not inherited from your
    // imported AppModule, it needs to be repeated here.
    bootstrap: [
        AppComponent
    ],
})
export class AppServerModule {};