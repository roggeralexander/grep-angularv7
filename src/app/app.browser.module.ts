import { 
  BrowserModule 
} from '@angular/platform-browser';

import { 
  APP_ID, 
  Inject, 
  NgModule, 
  PLATFORM_ID 
} from '@angular/core';

import { 
  isPlatformServer
} from "@angular/common"; 

import { 
  HttpClientModule
} from "@angular/common/http"; 

import { 
  FormsModule, 
  ReactiveFormsModule
} from "@angular/forms"; 

import { 
  BrowserAnimationsModule
} from "@angular/platform-browser/animations"; 

// Environment variables 
import { 
  environment
} from "../environments/environment"; 

// Modulos customizados 
/*
import { 
  MyCoreAudioModule
} from "@my-core/audio"; 

import { 
  DocViewerModule
} from "@my-core/doc"; 

import { 
  MyCoreGoogleMapsModule
} from "@my-core/google-maps"; 

import { 
  MyCoreImgModule
} from "@my-core/img"; 

import {
  MyCorePaginatorModule
} from "@my-core/paginator";

import {
  MyCoreSliderModule
} from "@my-core/slider";

import { 
  MyCoreStorageModule
} from "@my-core/storage";

import {
  MyCoreTreeModule
} from "@my-core/tree";
*/

import {
  MyCorePaginatorModule
} from "ids-ng7-paginator";

import { 
  MyCoreWordsCloudModule
} from "ids-ng7-words-cloud";

/*
import { 
  CommonVideoPlayerModule, 
  EmbeddedVideoPlayerModule, 
  VideoCaptureModule
} from "@my-core/video"; 

import { 
  WordCloudModule
} from "@my-core/words-cloud";

import {
  WatermarkModule
} from "@my-core/watermark";
*/

// Components
import { 
  AppComponent 
} from './app.component';

import {
  StatisticCardComponent
} from "./components/statistic-card/statistic-card.component";

@NgModule({
  imports: [
    // Add .withServerTransition() to support Universal rendering.
    // The application ID can be any identifier which is unique on
    // the page.
    BrowserModule.withServerTransition({
      appId: "grep"
    }),
    HttpClientModule, 
    FormsModule, 
    ReactiveFormsModule, 
    BrowserAnimationsModule, 
    MyCorePaginatorModule, 
    MyCoreWordsCloudModule
    /*
    DocViewerModule.forRoot(
      environment.DOCS_PLUGINS_VIEWER_URLS
    ), 
    MyCoreGoogleMapsModule.forRoot(
      environment.MAPS_CONFIGS
    ), 
    MyCoreAudioModule,
    MyCoreImgModule, 
    CommonVideoPlayerModule, 
    EmbeddedVideoPlayerModule.forRoot(
      environment.EMBEDDED_VIDEOS_API_KEYS
    ), 
    VideoCaptureModule,
    MyCorePaginatorModule.forRoot({}), 
    MyCoreSliderModule, 
    MyCoreStorageModule.forRoot({
      name: "grep-core"
    }),
    MyCoreTreeModule, 
    MyCoreUtilsModule.forRoot(), 
    WordCloudModule, 
    WatermarkModule
    */
  ],
  declarations: [
    AppComponent, 
    StatisticCardComponent
  ],
  providers: [],
  entryComponents: [
  ],
  bootstrap: [
    AppComponent
  ]
})
export class AppBrowserModule { 
  constructor(
    @Inject(APP_ID) appId : string, 
    @Inject(PLATFORM_ID) platformId : Object 
  ) {
    // Get runtime information about the current platform and appId by injection 
    const platform : string = isPlatformServer(platformId) ? 
      "on the server" : 
      "in the browser / phone";    

    console.log(`Running ${ platform } with appId="${ appId }"`);      
  };
};
