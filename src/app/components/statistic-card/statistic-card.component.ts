import {
    Component, 
    Inject, 
    Input, 
    PLATFORM_ID
} from "@angular/core";

import { 
    isPlatformServer
} from "@angular/common";

@Component({
    selector: "statistic-card", 
    templateUrl: "./statistic-card.component.html", 
    styleUrls: [
        "./statistic-card.component.scss"
    ]
})
export class StatisticCardComponent {
    @Input() 
    get title() : string {
        return this._title;
    };
    set title(title : string) {
        if(title === undefined || !title || !title.trim().length || this._title === title) {
            return ;
        }

        this._title = title;
    };
    protected _title : string = null; 

    @Input() 
    get subtitle() : string {
        return this._title;
    };
    set subtitle(title : string) {
        if(title === undefined || !title || !title.trim().length || this._subtitle === title) {
            return ;
        }

        this._subtitle = title;
    };
    protected _subtitle : string = null; 

    @Input() 
    get body() : string {
        return this._body;
    };
    set body(value : string) {
        if(value === undefined || !value || !value.trim().length || this._body === value) {
            return ;
        }

        this._body = value;
    };
    protected _body : any = null;
    
    constructor(
        @Inject(PLATFORM_ID) protected _platformId : Object
    ) { 
        if(isPlatformServer(this._platformId)) {
            return ; 
        }
    };
};