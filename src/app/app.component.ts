import { 
  AfterViewInit, 
  ChangeDetectionStrategy, 
  ChangeDetectorRef,
  Component,
  ElementRef, 
  Inject, 
  OnDestroy, 
  PLATFORM_ID, 
  Renderer2, 
  ViewChild
} from '@angular/core';

import { 
  isPlatformServer
} from "@angular/common";

import { 
  Observable, 
  of, 
  Subscription, 
  timer, 
  throwError
} from "rxjs"; 

import { 
  catchError, 
  skipWhile, 
  switchMap
} from "rxjs/operators"; 

// Models 
import {
  MyCorePaginatorOutput
} from "ids-ng7-paginator";

// Google maps 
/*
import {
  Coords, 
  GoogleMapsService
} from "@my-core/google-maps"; 

// Models
import { 
  MyCorePaginatorOutput
} from "@my-core/paginator"; 

import { 
  TreeDataKeys
} from "@my-core/tree";

// Services 
import { 
  StorageService
} from "@my-core/storage";

// Utilities 
import { 
  CaptureOutput
} from "@my-core/utils";
*/

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'], 
  changeDetection: ChangeDetectionStrategy.Default
})
export class AppComponent implements AfterViewInit, OnDestroy {
  // Data para o statistic card 01
  public scData01 : any = [{
    title: "CUSTO TOTAL", 
    subtitle: "Ligações", 
    body: "R$ 372,65"
  }, {
    title: "DURAÇÃO TOTAL", 
    subtitle: "Minutos", 
    body: "22.562"
  }, {
    title: "QUANTIDADE TOTAL", 
    subtitle: "Ligações", 
    body: "17.140"
  }];

  // Propriedade para referenciar o page number 
  public pageNumber : number = 3;

  // Propriedade para especificar o page size 
  public pageSize : number = 75;

  // Data para o statistic card 02
  public scData02 : any = [{
    title: "0800", 
    subtitle: "274 ligações", 
    body: "GRATUITO"
  }, {
    title: "CELULAR VC1", 
    subtitle: "1.845 ligações", 
    body: "R$ 239,70"
  }, {
    title: "CELULAR VC2", 
    subtitle: "12 ligações", 
    body: "R$ 8,03"
  }, {
    title: "CELULAR VC3", 
    subtitle: "63 ligações", 
    body: "R$ 33,84"
  }, {
    title: "DDD", 
    subtitle: "107 ligações", 
    body: "R$ 17,70"
  }, {
    title: "DDD", 
    subtitle: "107 ligações", 
    body: "R$ 17,70"
  }, {
    title: "Entrada", 
    subtitle: "3490 ligações", 
    body: "GRATUITO"
  }, {
    title: "Local", 
    subtitle: "1988 ligações", 
    body: "R$ 73,38"
  }];


  public file : File = null;

  /*
  // Propriedade para referenciar as configurações de keys do Tree 
  public treeDataKeys : TreeDataKeys = {
    idKey: "id",
    pathKey: "path", 
    nameKey: "name", 
    parentIdKey: "parentId", 
    separator: "#"
  };

  // Propriedade para especificar a data para o tree
  public treeData01 : any[] = [{
    id: 1,
    name: "Documents",
    path: "1"
  }, {
    id: 2,
    name: "Work",
    path: "1#2", 
    parentId: 1
  }, {
    id: 3,
    name: "Expenses.doc",
    path: "1#2#3", 
    parentId: 2
  }, {
    id: 4,
    name: "Resume.doc",
    path: "1#2#4", 
    parentId: 2
  }, {
    id: 5,
    name: "Home",
    path: "1#5", 
    parentId: 1
  }, {
    id: 6,
    name: "Invoices.txt",
    path: "1#5#6", 
    parentId: 5
  }, {
    id: 7,
    name: "Pictures",
    path: "7"
  }, {
    id: 8,
    name: "barcelona.jpg",
    path: "7#8", 
    parentId: 7
  }, {
    id: 9,
    name: "logo.jpg",
    path: "7#9", 
    parentId: 7
  }, {
    id: 10,
    name: "Movies",
    path: "10",
  }, {
    id: 11,
    name: "Al Pacino",
    path: "10#11", 
    parentId: 10
  }, {
    id: 12,
    name: "Scarface",
    path: "10#11#12", 
    parentId: 11
  }, {
    id: 13,
    name: "Serpico",
    path: "10#11#13", 
    parentId: 11
  }, {
    id: 14,
    name: "Robert De Niro",
    path: "10#14", 
    parentId: 10
  }, {
    id: 15,
    name: "Goodfellas",
    path: "10#14#15", 
    parentId: 14
  }, {
    id: 16,
    name: "Untouchables",
    path: "10#14#16", 
    parentId: 14
  }];

  // Propriedade para especificar a data para o tree
  public treeData03 : any[] = [{
    id: 1,
    name: {
      "en": "Documents", 
      "es": "Documentos", 
      "pt-br": "Documentos"
    },
    path: "1"
  }, {
    id: 2,
    name: {
      "en": "Work", 
      "es": "Trabajo", 
      "pt-br": "Trabalho"
    },
    path: "1#2"
  }, {
    id: 3,
    name: {
      "en": "Expenses.doc", 
      "es": "Expenses.doc", 
      "pt-br": "Expenses.doc"
    },
    path: "1#2#3"
  }, {
    id: 4,
    name: {
      "en": "Resume.doc", 
      "es": "Resume.doc", 
      "pt-br": "Resume.doc"
    },
    path: "1#2#4"
  }, {
    id: 5,
    name: {
      "en": "Home", 
      "es": "Casa", 
      "pt-br": "Hogar"
    },
    path: "1#5"
  }, {
    id: 6,
    name: {
      "en": "Invoices.txt", 
      "es": "Invoices.txt", 
      "pt-br": "Invoices.txt"
    },
    path: "1#5#6"
  }, {
    id: 7,
    name: {
      "en": "Pictures", 
      "es": "Imágenes", 
      "pt-br": "Imagens"
    },
    path: "7"
  }, {
    id: 8,
    name: {
      "en": "barcelona.jpg", 
      "es": "barcelona.jpg", 
      "pt-br": "barcelona.jpg"
    },
    path: "7#8"
  }, {
    id: 9,
    name: {
      "en": "logo.jpg", 
      "es": "logo.jpg", 
      "pt-br": "logo.jpg"
    },
    path: "7#9"
  }, {
    id: 10,
    name: {
      "en": "Movies", 
      "es": "Películas", 
      "pt-br": "Filmes"
    },
    path: "10"
  }, {
    id: 11,
    name: {
      "en": "Al Pacino", 
      "es": "Al Pacino", 
      "pt-br": "Al Pacino"
    },
    path: "10#11"
  }, {
    id: 12,
    name: {
      "en": "Scarface", 
      "es": "Cara cortada", 
      "pt-br": "Scarface"
    },
    path: "10#11#12"
  }, {
    id: 13,
    name: {
      "en": "Serpico", 
      "es": "Serpico", 
      "pt-br": "Serpico"
    },
    path: "10#11#13"
  }, {
    id: 14,
    name: {
      "en": "Robert De Niro", 
      "es": "Robert De Niro", 
      "pt-br": "Robert De Niro"
    },
    path: "10#14"
  }, {
    id: 15,
    name: {
      "en": "Goodfellas", 
      "es": "Goodfellas", 
      "pt-br": "Goodfellas"
    },
    path: "10#14#15"
  }, {
    id: 16,
    name: {
      "en": "Untouchables", 
      "es": "Los intocables", 
      "pt-br": "Os intocáveis"
    },
    path: "10#14#16"
  }];

  // Propriedade para especificar a data para o tree
  public treeData02 : any[] = [{
    id: 1,
    name: {
      "en": "Documents", 
      "es": "Documentos", 
      "pt-br": "Documentos"
    },
    path: "1"
  }, {
    id: 2,
    name: {
      "en": "Work", 
      "es": "Trabajo", 
      "pt-br": "Trabalho"
    },
    path: "1#2", 
    parentId: 1
  }, {
    id: 3,
    name: {
      "en": "Expenses.doc", 
      "es": "Expenses.doc", 
      "pt-br": "Expenses.doc"
    },
    path: "1#2#3", 
    parentId: 2
  }, {
    id: 4,
    name: {
      "en": "Resume.doc", 
      "es": "Resume.doc", 
      "pt-br": "Resume.doc"
    },
    path: "1#2#4", 
    parentId: 2
  }, {
    id: 5,
    name: {
      "en": "Home", 
      "es": "Casa", 
      "pt-br": "Hogar"
    },
    path: "1#5", 
    parentId: 1
  }, {
    id: 6,
    name: {
      "en": "Invoices.txt", 
      "es": "Invoices.txt", 
      "pt-br": "Invoices.txt"
    },
    path: "1#5#6", 
    parentId: 5
  }, {
    id: 7,
    name: {
      "en": "Pictures", 
      "es": "Imágenes", 
      "pt-br": "Imagens"
    },
    path: "7"
  }, {
    id: 8,
    name: {
      "en": "barcelona.jpg", 
      "es": "barcelona.jpg", 
      "pt-br": "barcelona.jpg"
    },
    path: "7#8", 
    parentId: 7
  }, {
    id: 9,
    name: {
      "en": "logo.jpg", 
      "es": "logo.jpg", 
      "pt-br": "logo.jpg"
    },
    path: "7#9", 
    parentId: 7
  }, {
    id: 10,
    name: {
      "en": "Movies", 
      "es": "Películas", 
      "pt-br": "Filmes"
    },
    path: "10",
  }, {
    id: 11,
    name: {
      "en": "Al Pacino", 
      "es": "Al Pacino", 
      "pt-br": "Al Pacino"
    },
    path: "10#11", 
    parentId: 10
  }, {
    id: 12,
    name: {
      "en": "Scarface", 
      "es": "Cara cortada", 
      "pt-br": "Scarface"
    },
    path: "10#11#12", 
    parentId: 11
  }, {
    id: 13,
    name: {
      "en": "Serpico", 
      "es": "Serpico", 
      "pt-br": "Serpico"
    },
    path: "10#11#13", 
    parentId: 11
  }, {
    id: 14,
    name: {
      "en": "Robert De Niro", 
      "es": "Robert De Niro", 
      "pt-br": "Robert De Niro"
    },
    path: "10#14", 
    parentId: 10
  }, {
    id: 15,
    name: {
      "en": "Goodfellas", 
      "es": "Goodfellas", 
      "pt-br": "Goodfellas"
    },
    path: "10#14#15", 
    parentId: 14
  }, {
    id: 16,
    name: {
      "en": "Untouchables", 
      "es": "Los intocables", 
      "pt-br": "Os intocáveis"
    },
    path: "10#14#16", 
    parentId: 14
  }];

  // Propriedade para testar a duration 
  public duration : number = 850;

  public html : string = '<a href="https://example.com">' +
    'lorem ipsum <strong>dolor</strong> <em>sit</em> amet' +
  '</a>';

  public imgSrc = "assets/parks/headers/americansamoa.jpg";

  @ViewChild(
    "mapCanvas", 
    { read: ElementRef }
  ) protected _mapRef : ElementRef = null; 

  protected _subs : Subscription[] = [];

  // Propriedade para referenciar o watermarl url data 
  public wDataUrl : string = null; 

  // Array de números
  public numbers : number[] = [0, 1, 2, 3, 4, 5, 6];

  // Array de cores
  public colors : string[] = [
    "blue", 
    "olivedrab", 
    "orangered", 
    "orange", 
    "yellow", 
    "yellowgreen", 
    "green"
  ];

  @ViewChild(
    "sliderContainer", 
    { read : ElementRef }
  ) protected _sliderContainer : ElementRef = null; 
  */

 public dataCloud01 = [{ text: 'key01', size: 20 }, { text: 'key02', size: 30}, { text: 'key03', size: 25 }, { text: 'key04', size: 55 }];

 public dataCloud02 = "Of course that’s your contention. You’re a first year grad student. You just got finished readin’ some Marxian historian, Pete Garrison probably. You’re gonna be convinced of that ’til next month when you get to James Lemon and then you’re gonna be talkin’ about how the economies of Virginia and Pennsylvania were entrepreneurial and capitalist way back in 1740. That’s gonna last until next year. You’re gonna be in here regurgitating Gordon Wood, talkin’ about, you know, the Pre-Revolutionary utopia and the capital-forming effects of military mobilization… ‘Wood drastically underestimates the impact of social distinctions predicated upon wealth, especially inherited wealth.’ You got that from Vickers, Work in Essex County, page 98, right? Yeah, I read that, too. Were you gonna plagiarize the whole thing for us? Do you have any thoughts of your own on this matter? Or do you, is that your thing? You come into a bar. You read some obscure passage and then pretend, you pawn it off as your own, as your own idea just to impress some girls and embarrass my friend? See, the sad thing about a guy like you is in 50 years, you’re gonna start doin’ some thinkin’ on your own and you’re gonna come up with the fact that there are two certainties in life. One: don’t do that. And two: you dropped a hundred and fifty grand on a fuckin’ education you coulda got for a dollar fifty in late charges at the public library.";

 public dataCloud03 = new Map([
   [ "Russia", 60 ],
   [ "Belgium", 45 ],
   [ "Turkey", 48 ],
   [ "Germany", 55 ],
   [ "Hungary", 20 ],
   [ "Belarus", 20 ],
   [ "France", 60 ],
   [ "Greece", 45 ],
   [ "Italy", 48 ],
   [ "England", 55 ],
   [ "Austria", 20 ],
   [ "Switzerland", 20 ],
   [ "Spain", 60 ],
   [ "keyword01", 20 ],
   [ "keyword02", 20 ],
   [ "Ukraine", 60 ],
   [ "Bulgaria", 45 ],
   [ "Serbia", 48 ],
   [ "Czech Republic", 55 ],
   [ "Denmark", 20 ],
   [ "Finland", 20 ],
   [ "Azerbaijan", 45 ],
   [ "Kazakhstan", 48 ],
   [ "Portugal", 55 ],
   [ "Slovakia", 20 ],
   [ "Norway", 20 ],
   [ "Poland", 60 ],
   [ "Ireland", 20 ],
   [ "Croatia", 20 ],
   [ "Wales", 60 ],
   [ "Sweeden", 45 ],
   [ "Romania", 48 ],
   [ "Netherlands", 55 ],
   [ "North Ireland", 20 ],
   [ "Scotland", 45 ],
   [ "Georgia", 48 ],
   [ "Bosnia and Herzegovina", 55 ],
   [ "Armenia", 20 ],
   [ "Moldova", 20 ],
   [ "Albania", 60 ],
   [ "Lithuania", 20 ],
   [ "Macedonia", 20 ],
   [ "Slovenia", 60 ],
   [ "Latvia", 45 ],
   [ "Kosovo", 48 ],
   [ "Estonia", 55 ],
   [ "Cyprus", 20 ],
   [ "Montenegro", 45 ],
   [ "Luxembourg", 48 ],
   [ "Malta", 55 ],
   [ "Iceland", 20 ],
   [ "Andorra", 20 ],
   [ "Faroe Island", 60 ],
   [ "Liechtenstein", 20 ],
   [ "Gibraltar", 20 ],
   [ "San Marino", 60 ],
   [ "Nigeria", 45 ],
   [ "Ethiopia", 48 ],
   [ "Egypt", 55 ],
   [ "Democratic Republic of the Congo", 20 ],
   [ "South Africa", 45 ],
   [ "Tanzania", 48 ],
   [ "Kenya", 55 ],
   [ "Sudan", 20 ],
   [ "Algeria", 20 ],
   [ "Uganda", 60 ],
   [ "Morocco", 20 ],
   [ "Mozambique", 20 ],
   [ "Ghana", 60 ],
   [ "Angola", 45 ],
   [ "Ivory Coast", 48 ],
   [ "Madagascar", 55 ],
   [ "Cameroon", 20 ],
   [ "Niger", 45 ],
   [ "Burkina Faso", 48 ],
   [ "Mali", 55 ],
   [ "Malawi", 20 ],
   [ "Zambia", 20 ],
   [ "Somalia", 60 ],
   [ "Senegal", 20 ],
   [ "Chad", 20 ],
   [ "Zimbabwe", 60 ],
   [ "South Sudan", 45 ],
   [ "Rwanda", 48 ],
   [ "Tunisia", 55 ],
   [ "Guinea", 20 ],
   [ "Benin", 45 ],
   [ "Burundi", 48 ],
   [ "Togo", 55 ],
   [ "Eritrea", 20 ],
   [ "Sierra Leone", 20 ],
   [ "Libya", 60 ],
   [ "Republic of the Congo", 20 ],
   [ "Central African Republic", 20 ],
   [ "Liberia", 60 ],
   [ "Mauritania", 45 ],
   [ "Namibia", 48 ],
   [ "Botswana", 55 ],
   [ "Gambia", 20 ],
   [ "Equatorial Guinea", 45 ],
   [ "Lesotho", 48 ],
   [ "Gabon", 55 ],
   [ "Guinea-Bissau", 20 ],
   [ "Mauritius", 20 ],
   [ "Swaziland", 60 ],
   [ "Djibouti", 20 ],
   [ "Comoros", 20 ],
   [ "Cape Verde", 60 ],
   [ "Sao Tome and Principe", 45 ],
   [ "Seychelles", 48 ],
   [ "Mayotte", 45 ],
   [ "Saint Helena", 48 ],
 ]);

 public allowedWords = "poop,i,me,my,myself,we,us,our,ours,ourselves,you,your,yours,yourself,yourselves,he,him,his,himself,she,her,hers,herself,it,its,itself,they,them,their,theirs,themselves,what,which,who,whom,whose,this,that,these,those,am,is,are,was,were,be,been,being,have,has,had,having,do,does,did,doing,will,would,should,can,could,ought,i'm,you're,he's,she's,it's,we're,they're,i've,you've,we've,they've,i'd,you'd,he'd,she'd,we'd,they'd,i'll,you'll,he'll,she'll,we'll,they'll,isn't,aren't,wasn't,weren't,hasn't,haven't,hadn't,doesn't,don't,didn't,won't,wouldn't,shan't,shouldn't,can't,cannot,couldn't,mustn't,let's,that's,who's,what's,here's,there's,when's,where's,why's,how's,a,an,the,and,but,if,or,because,as,until,while,of,at,by,for,with,about,against,between,into,through,during,before,after,above,below,to,from,up,upon,down,in,out,on,off,over,under,again,further,then,once,here,there,when,where,why,how,all,any,both,each,few,more,most,other,some,such,no,nor,not,only,own,same,so,than,too,very,say,says,said,shall";

  constructor(
    protected _changeDetector : ChangeDetectorRef, 
//    protected _googleMaps : GoogleMapsService,
    @Inject(PLATFORM_ID) protected _platformId : Object, 
    protected _renderer : Renderer2, 
//    protected _storage : StorageService
  ) { 
    if(isPlatformServer(this._platformId)) {
      return ; 
    }

    /*
    this._storage.setItems(
      "lang", 
      "pt-br"
    ).subscribe(
      (lang : string) => console.log("Idioma salvo no cache", lang), 
      (err : Error) => console.log("Error salvando registro no cache", err.message)
    );

    setTimeout(() => {
      this._renderer.setStyle(
        this._sliderContainer.nativeElement, 
        "width", 
        "90%"
      );

      console.log("Aplicando style no container");
    }, 5000);
    */
  };

  ngAfterViewInit() {
    /*
    if(isPlatformServer(this._platformId) || this._mapRef === undefined || !this._mapRef) {
      return ; 
    }

    // Solicitando a geração do maps 
    this._subs.push(
      timer(5000).pipe(
        switchMap(() => this._prepareMap({
          "lat": -27.58212108,
          "lng": -48.6402822
        }))
      ).subscribe(
        (isOk : boolean) => {
          console.log("Geolocation e google maps gerado", isOk);
          this._changeDetector.markForCheck();
        }, 
        (err : any) => console.log(`Error code: ${ err.code } - Message: ${ err.message }`)
      )
    )
    */
  };

  ngOnDestroy() {
    /*
    this._subs.forEach((sub : Subscription) => {
      sub.unsubscribe();
    });
    */
  };

  /**
   * Método para preparar a data para geração do map, a partir das coordenadas obtidas, 
   * sendo gerado um window info aberto com o endereço da posição atual 
   * @param coords | Coords
   */
  protected _prepareMap = (coords : any) : Observable<boolean> => {
    return of(
      false
    );

    /*
    if(this._mapRef === undefined || !this._mapRef) {
      return of(false); 
    }

    const self = this;

    console.log("Coordenadas atuais", coords);

    return this._googleMaps.apiReady.pipe(
      skipWhile((isOk : any) => isOk === null), 
      switchMap((isOk : boolean) => {
        if(!isOk) {
          return throwError(
            new Error(
              "Google Maps API is not loaded"
            )
          );
        }

        return self._googleMaps.getAddress(
          coords
        );          
      }), 
      switchMap((address : string) => {
        // Criando a instância do map
        const map = self._googleMaps.createMap(
          <HTMLDivElement>self._mapRef.nativeElement, 
          coords, 
          7
        );

        if(map === undefined || !map) {
          return throwError(
            new Error(
              "Google map instance not created"
            )
          );
        }

        let infoWindow : any = null;

        if(address !== undefined && address && address.trim().length) {
          infoWindow = self._googleMaps.createInfoWindow(
            `<h6>${ address }</h6>`
          );
        }

        const marker : any = self._googleMaps.createMarker(
          map, 
          coords
        );

        if(marker === undefined || !marker) {
          return throwError(
            new Error(
              "Google map marker not created"
            )
          );        }

        if(infoWindow === undefined || !infoWindow) {
          return throwError(
            new Error(
              "Google map info window not created"
            )
          );        
        }

        infoWindow.open(
          map, 
          marker
        );
        
        // Adicionando event listeners ao marker 
        self._googleMaps.setListeners(
          marker, 
          [{
            event: "click", 
            cb: () => {
              setTimeout(() => {
                infoWindow.open(
                  map, 
                  marker
                );
        
                map.setZoom(16);
                map.setCenter(
                  marker.getPosition()
                );
              })
            }
          }]
        );
        
        return of(true);
      }),
      catchError((err : any) => {
        console.log("Error loading google maps", err);
        return of(false);
      })
    );
    */
  };

  /**
   * Callback quando emitido o output do watermark 
   */
  /*
  public onWatermarkFile = ($event : CaptureOutput) : void => {
    this.wDataUrl = URL.createObjectURL($event["file"]);

    this._changeDetector.markForCheck();
  };

  /**
   * Callback para quando a seleção do tree mudar
   */
  /*
  public onTreeSelectionChange = ($event : string[]) : void => {
    console.log("Itens selecionadas da árvore", $event);
  };

  /**
   * Método listener quando houver alguma mudança emitida pelo paginator component
   * @param $event
   */
  /*
  public handlePaginatorChange = ($event : MyCorePaginatorOutput) : void => {
    console.log("Output emitido pelo paginator: ", $event);
  };

  /**
   * Callback para quando atualizado o índice do item
   * @param action
   * @param inex
   */
  /*
  public onUpdateItemIndex = (action : "previous" | "current", index : number) : void => {
    console.log(`${ action } - ${ index }`);
  };

  /**
   * Callback para saber o progress do interval para o próximo item
   * @param $event
   */
  /*
  public onProgress = ($event : number) : void => {
    console.log(`Progress: ${ $event }`);
  };

  /**
   * Callback para saber se o slider está pronto para uso ou não
   * @param $event
   */
  /*
  public onIsReady = ($event : boolean) : void => {
    console.log(`Is slider ready?: ${ $event }`);
  };

  /**
   * Callback para saber se o slider está em reprodução ou não
   * @param $event
   */
  /*
  public onIsPlaying = ($event : boolean) : void => {
    console.log(`Is slider playing?: ${ $event }`);
  };
  */
 
  public onPan = ($event : any) : void => {
    console.log("Aconteceu o evento desde o app component:", $event["type"]);
  };

  /**
   * Callback para quando uma página mudar
   */
  public onPageChange = ($event : MyCorePaginatorOutput) : void => {
    console.log("Objeto mudou quando escolhida a página:", $event);
    this.pageNumber = $event["pageNumber"];
    this.pageSize = $event["pageSize"];
  };
};