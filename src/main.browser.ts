import { 
  enableProdMode 
} from '@angular/core';

import { 
  platformBrowserDynamic 
} from '@angular/platform-browser-dynamic';

import { 
  AppBrowserModule 
} from './app/app.browser.module';

import { 
  environment 
} from './environments/environment';

import "hammerjs";

if (environment.production) {
  enableProdMode();

  /*
  // Substituindo o console.log quando gerado o build de produção
  if(window !== undefined && window && window["console"] !== undefined && window["console"]) {
    window.console.log = function() { };
  } 
  */
}

platformBrowserDynamic()
  .bootstrapModule(AppBrowserModule)
  .catch(err => console.error(err));
