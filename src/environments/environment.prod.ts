export const environment = {
  production: true,

  // Variáveis de ambiente com o url dos plugins do viewer de docs
  DOCS_PLUGINS_VIEWER_URLS: {
    google: "https://docs.google.com/gview?url=$src&embedded=true", 
    "ms-office": "https://view.officeapps.live.com/op/embed.aspx?src=$src"
  },

  // Embedded videos api keys 
  EMBEDDED_VIDEOS_API_KEYS: {
    dailymotion: "dbed5e40316b703a4224"
  },

  // Google Maps 
  MAPS_CONFIGS: {
    apiKey: "AIzaSyB8pf6ZdFQj5qw7rc_HSGrhUwQKfIe9ICw", 
    scriptUrl: "https://maps.googleapis.com/maps/api/js?key=$apiKey&v=quarterly"
  }, 

  // Retry options 
  RETRY_OPTIONS: {
    maxRetryAttempts: 5, 
    scalingDuration: 2000
  }
};
