// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,

  // Variáveis de ambiente com o url dos plugins do viewer de docs
  DOCS_PLUGINS_VIEWER_URLS: {
    google: "https://docs.google.com/gview?url=$src&embedded=true", 
    "ms-office": "https://view.officeapps.live.com/op/embed.aspx?src=$src"
  },

  // Embedded videos api keys 
  EMBEDDED_VIDEOS_API_KEYS: {
    dailymotion: "dbed5e40316b703a4224"
  },

  // Google Maps 
  MAPS_CONFIGS: {
    apiKey: "AIzaSyB8pf6ZdFQj5qw7rc_HSGrhUwQKfIe9ICw", 
    scriptUrl: "https://maps.googleapis.com/maps/api/js?key=$apiKey&v=quarterly"
  },

  // Retry options 
  RETRY_OPTIONS: {
    maxRetryAttempts: 5, 
    scalingDuration: 2000
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
