# My Core Angular v7

Projeto gerado usando [Angular CLI](https://github.com/angular/angular-cli) version 7.2.3.

## Development server

Executar npm start para abrir no ambiente de desenvolvimento, e acessar ao link `http://localhost:3500/`.

## Build

Para gerar a versão "build" deverão ser executados os seguintes passos: 
- Executar o arquivo bash .build-project.sh 
- Executar o script npm run build:ssr

Caso deseje testar a versão de produção, executar o script `npm run serve:ssr` e acessar posteriormente ao link `http://localhost:4000`
