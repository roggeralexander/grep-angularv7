// These are important and needed before anything else
// Wrapper para simular o pacote http client no servidor node
// (global as any).XMLHttpRequest = require("xmlhttprequest").XMLHttpRequest;

import 'zone.js/dist/zone-node';
import 'reflect-metadata';

const domino = require('domino');
const fs = require('fs');
const path = require('path');

// Constants 
const PORT = process.env.PORT || 4000;

const DIST_FOLDER = path.join(
    process.cwd(), 
    "dist"
);

// Our index.html we'll use as our template
const INDEX_HTML_PATH = path.join(
    DIST_FOLDER, 
    "browser", 
    "index.html"
);

const template = fs.readFileSync(
    INDEX_HTML_PATH
).toString();

const win = domino.createWindow(
    template
);

// Mock window
global["window"] = win;

Object.defineProperty(
    win.document.body.style, 
    "transform", 
    {
        value: () => {
            return {
                enumerable: true, 
                configurable: true
            };
        }
    }
);

// Mock document
global['document'] = win.document;

// Mock document
global['Node'] = win.Node;

// Mock css
global['CSS'] = null;

// Mock Prism
global['Prism'] = null;

// Mock navigator
global['navigator'] = win.navigator;

// Mock event
global['Event'] = win.Event;

// Mock event prototype
global['Event']['prototype'] = win.Event.prototype;

import { 
    enableProdMode 
} from '@angular/core';

import * as express from 'express';

// Import module map for lazy loading
import { 
    provideModuleMap 
} from '@nguniversal/module-map-ngfactory-loader';

import { 
    renderModuleFactory 
} from '@angular/platform-server';

// Express engine tokens 
import { 
    REQUEST, 
    RESPONSE 
} from '@nguniversal/express-engine/tokens';

// * NOTE :: leave this as require() since this file is built Dynamically from webpack
const { 
    AppServerModuleNgFactory, 
    LAZY_MODULE_MAP 
} = require('./dist/server/main');


// Faster server renders w/ Prod mode (dev mode never needed)
enableProdMode();

// Express server 
const app = express();

app.engine(
    "html", 
    (params, options, callback) => {
        renderModuleFactory(
            AppServerModuleNgFactory, 
            {
                document: template, 
                url: options.req.url, 
                extraProviders: [
                    provideModuleMap(
                        LAZY_MODULE_MAP
                    )
                ]
            }
        ).then(html => {
            callback(
                null, 
                html
            );
        }).catch(err => {
            callback(
                err, 
                null
            );
        });
    }
);

app.set(
    "view engine", 
    "html"
);

app.set(
    "views", 
    path.join(
        DIST_FOLDER, 
        "browser"
    )
);

// Server static files from /browser
app.get(
    '*.*', 
    express.static(
        path.join(
            DIST_FOLDER, 
            'browser'
        )
    )
);

// All regular routes use the Universal engine
app.get(
    "*", 
    (req, res) => {
        // Mock navigator
        global["navigator"] = req['headers']['user-agent'];

        const http = req.headers['x-forwarded-proto'] === undefined ? 
            'http' : 
            req.headers['x-forwarded-proto'];
    
        const url = req.originalUrl;        

        console.timeEnd(`GET: ${ url }`);

        res.render(
            INDEX_HTML_PATH, 
            { 
                req: req, 
                res: res, 
                providers: [
                    {
                        provide: REQUEST, 
                        useValue: req
                    }, 
                    {
                        provide: RESPONSE, 
                        useValue: res
                    }, 
                    {
                        provide: "ORIGIN_URL", 
                        useValue: `${ http }://${ req.headers.host }`
                    }
                ] 
            }, 
            (err, html) => {
                if(!!err) {
                    throw err;
                }

                console.timeEnd(`GET: ${ url }`);
                res.send(html);
            }
        );
    }
);

// Start up the Node server
app.listen(
    PORT, 
    () => {
        console.log(`Node server listening on http://0.0.0.0:${PORT}`);
    }
);