/*
 * Public API Surface of my-core-words-cloud
 */

// Directives 
export {
    WordCloudDirective as MyCoreWordsCloudDirective
} from "./lib/directives/word-cloud.directive";

// Modules
export * from './lib/my-core-words-cloud.module';
