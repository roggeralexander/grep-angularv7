import { 
  NgModule 
} from '@angular/core';

import {
  CommonModule
} from "@angular/common"; 

// Angular CDK Modules 
import { 
  ScrollDispatchModule
} from "@angular/cdk/scrolling";

// Directives
import { 
  WordCloudDirective 
} from './directives/word-cloud.directive';

@NgModule({
  imports: [
    CommonModule, 
    ScrollDispatchModule
  ],
  declarations: [
    WordCloudDirective
  ],
  exports: [
    WordCloudDirective
  ]
})
export class MyCoreWordsCloudModule { };
