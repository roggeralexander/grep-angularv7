import { 
  Directive, 
  ElementRef, 
  Inject, 
  Input,
  OnDestroy, 
  PLATFORM_ID, 
  Renderer2 
} from '@angular/core';

import { 
  isPlatformServer
} from "@angular/common";

import { 
  ViewportRuler
} from "@angular/cdk/scrolling";

import { 
  Subject, 
  Subscription
} from "rxjs";

// Classes 
import { 
  WordCloud
} from "../classes/word-cloud";

@Directive({
  selector: '[mcWordCloud]'
})
export class WordCloudDirective implements OnDestroy {
  // Input para a data do word cloud
  @Input() 
  get data() : any | any[] {
    return this._data;
  };
  set data(data : any | any[]) {
    if(this._data === data) {
      return ;
    }    

    this._data = data;

    if(!isPlatformServer(this._platformId)) {
      setTimeout(() => {
        this._worker.next();
      });
    }
  };
  protected _data : any | any[] = null;

  // Input para as palavras comuns ou permitidas
  @Input() 
  get allowedWords() : string | string[] {
    return this._allowedWords;
  };
  set allowedWords(aw : string | string[]) {
    if(this._allowedWords === aw) {
      return ;
    }

    this._allowedWords = aw;
  };
  protected _allowedWords : string | string[] = null;

  // Input para determinar o tamanho máximo da fonte 
  @Input()
  get maxFontSize() : number {
    return this._maxFontSize;
  };
  set maxFontSize(maxFS : number) {
    if(this._maxFontSize === maxFS) {
      return ;
    }

    if(maxFS === undefined || maxFS === null || maxFS < 10) {
      return ;
    }

    this._maxFontSize = maxFS;
  };
  protected _maxFontSize : number = 50;

  // Instância do word cloud 
  protected _wordCloud : WordCloud = null;

  // Subjects & Subscriptions 
  protected _worker : Subject<void> = new Subject();
  protected _subs : Subscription[] = [];

  constructor(
    protected _elmRef : ElementRef, 
    @Inject(PLATFORM_ID) protected _platformId : Object, 
    protected _renderer : Renderer2, 
    protected _ruler : ViewportRuler
  ) { 
    if(isPlatformServer(this._platformId)) {
      return ;
    }

    this._subs.push(
      this._worker.subscribe(() => {
        this._generate();
      })
    );
  };

  ngOnDestroy() {
    this._subs.forEach((sub : Subscription) => {
      sub.unsubscribe();
    });

    this._worker && !this._worker.closed ?
      this._worker.complete() : 
      null;

    this._wordCloud ?
      this._wordCloud.destroy() : 
      null;
  };

  /**
   * Método para gerar o word cloud
   */
  protected _generate = () : void => {
    if(this._wordCloud) {
      this._wordCloud.destroy();
      this._wordCloud = null;
    }

    this._wordCloud = new WordCloud(
      this._data, 
      this._elmRef.nativeElement, 
      this._platformId, 
      this._renderer, 
      this._ruler, 
      this._maxFontSize, 
      this._allowedWords
    );
  };
};
