import { 
    Observable, 
    of
} from "rxjs";

import { 
    switchMap
} from "rxjs/operators";

// Third party libraries
import randomColor from "randomcolor";

// Models 
import { 
    WordCloudData
} from "../models";

// Utilities 
import { 
    removeAcentSync
} from "ids-ng7-utils";

/**
 * Método para obter a data processada, quando a data for de tipo array
 * @param data 
 */
export const getArrayDataProcessed = (data : any[]) : Observable<WordCloudData[]> => {
    if(!data.length) {
        return of(
            undefined
        );
    }

    let processed : WordCloudData[] = [];

    data.forEach(item => {
        if(item["text"] !== undefined && item["text"] && typeof item["text"] === "string" && item["size"] !== undefined && item["size"] !== null && typeof item["size"] === "number") {
            processed.push(
                item
            );
        } else {
            // Conferir como gerar a respectiva data
        }
    });

    return of(
        processed
    );
};

/**
 * Método para gerar a estrutura do word cloud a partir de um Map informado
 * @param data 
 */
export const getMapDataProcessed = (data : Map<string, number>) : Observable<WordCloudData[]> => {
    if(data === undefined || !data) {
        return of(
            undefined
        );
    }

    let processed : WordCloudData[] = [];

    // Percorrendo todos os items do map 
    data.forEach((value : number, key : string) => {
        const obj : WordCloudData = {
            text: key, 
            size: value
        };

        processed.push(
            obj
        );
    });

    console.log("Data processada do map: ", processed);

    return of(
        processed
    );
};

/**
 * Método para retornar um array de text e frequence quando informado um objeto que tem como key o text e como value o size
 * @param data 
 */
export const getObjectDataProcessed = (data : any) : Observable<WordCloudData[]> => {
    if(!data || !Object.keys(data).length) {
        return of(
            undefined
        );
    }

    let processed : WordCloudData[] = [];

    Object.keys(data).forEach((key : string) => {
        const obj : WordCloudData = {
            text: key, 
            size: data[key]
        }

        processed.push(
            obj
        );
    });

    return of(
        processed
    );
};

/**
 * Método para obter a data processada, quando a data for de tipo string
 * @param data 
 */
export const getStringDataProcessed = (data : string) : Observable<WordCloudData[]> => {
    if(!data.trim().length) {
        return of(
            undefined
        );
    }

    let word_count = {};

    let words = removeAcentSync(data.toLowerCase()).split(
        /[ '\-\(\)\*":;\[\]|{},.!?]+/
    );

    if(words.length === 1) {
        word_count[words[0]] = 1;
    } else {
        words.forEach((word : string) => {
            if(isNaN(parseInt(word)) && word.length && word !== "'") {
                if(word_count[word] !== undefined && word_count[word]) {
                    word_count[word]++;
                } else {
                    word_count[word] = 1;
                }
            }
        });
    }

    console.log("Objeto de key-values de palavras e o seu total de reincidências: ", word_count);
    
    return getObjectDataProcessed(
        word_count
    ).pipe(
        switchMap((results : WordCloudData[]) => of(
            results
        ))
    );
};

/**
 * Método para aplicar a filtragem da data processada com o array de palavras permitidas
 * @param data 
 * @param allowedWords 
 */
export const filterDataProcessed = (data : WordCloudData[], allowedWords? : string | string[]) : Observable<WordCloudData[]> => {
    if(allowedWords === undefined || !allowedWords || !allowedWords.length) {
        return of(
            data
        );
    }

    let processed : WordCloudData[] = data.filter((item : WordCloudData) => allowedWords.indexOf(item.text) !== -1);

    return of(
        processed
    );
};

/**
 * Método para obter o máximo de frequências
 * @param data 
 */
export const getMaxFrequence = (data : WordCloudData[]) : number => data.reduce((a : WordCloudData, b : WordCloudData) => a.size < b.size ? b : a, { text: "", size: -1 } ).size;

/**
 * Método para gerar as cores para o array de data e atribuir o atributo scale
 * @param data 
 * @param maxFontSize
 */
export const attrColorAndScale = (data : WordCloudData[], maxFontSize : number = 100) : Observable<any[]> => {
    const colors : string[] = randomColor({
        count: data.length, 
        luminosity: 'dark',
        hue: 'random'
    });

    let maxFrequence : number = getMaxFrequence(
        data
    );

    let processed : any[] = data.map((item : WordCloudData, index : number) => {
        item["color"] = colors[index];
        item["scale"] = parseFloat(((item.size / maxFrequence * maxFontSize) / 100).toFixed(2));
        return item;
    });

    return of(
        processed
    );
};


/**
 * Método para processar a data com o total de frequência por texto.
 * Caso existir a restrição de allowed words, ele só retornará a data processada após filtragem com o allowed words
 * @param data 
 * @param maxFontSize
 * @param allowedWords
 */
export const processData = (data : any, maxFontSize : number = 75, allowedWords?: string | string[]) : Observable<any[]> => {
    console.log("Data recebida pelo process data: ", data);
    console.log("Máximo de tamanho da fonte", maxFontSize);
    console.log("Solicitando à filtragem dos dados para o word cloud, sendo tipo de dados: ", data.constructor.name);

    if(data === undefined || !data) {
        return of(
            undefined
        );
    }

    let observable = null; 

    if(data.constructor.name === "Array") {
        observable = getArrayDataProcessed(
            data
        );
    } else if(data.constructor.name === "Map") {
        observable = getMapDataProcessed(
            data
        );
    } else if(data.constructor.name === "String") {
        observable = getStringDataProcessed(
            data
        );
    } else if(data.constructor.name === "Object") {
        observable = getObjectDataProcessed(
            data
        );
    }

    if(!observable) {
        return of(
            undefined
        );
    }

    return observable.pipe(
        switchMap((results : WordCloudData[]) => filterDataProcessed(
            results, 
            allowedWords
        )), 
        switchMap((results : WordCloudData[]) => attrColorAndScale(
            results, 
            maxFontSize
        )), 
        switchMap((results : any[]) => of(
                results
            )
        )
    );
};
