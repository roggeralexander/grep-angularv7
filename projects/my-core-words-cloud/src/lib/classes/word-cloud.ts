import { 
    Renderer2
} from "@angular/core";

import { 
    isPlatformServer
} from "@angular/common";

import { 
    ViewportRuler
} from "@angular/cdk/scrolling";

import { 
    Observable, 
    of, 
    Subscription, 
    timer
} from "rxjs";

import { 
    switchMap
} from "rxjs/operators";

// Third party library 
import d3 from "d3";

import cloud from "d3-cloud";

import uuid from "uuid";

// Utils 
import { 
    processData
} from "../utils";

// Declarando o tipo de layout 
d3.layout.cloud = cloud;

/**
 * Classe para a geração do word cloud
 */
export class WordCloud {
    // Id para o word cloud 
    protected _id : string = `wc-` + uuid.v4().replace(
        new RegExp("-", "gi"), 
        ""
    );
        
    // Propriedade para especificar a data processada 
    protected _processedData : any[] = null;

    // Propriedade para especificar o layout 
    protected _layout : any = null;

    // Propriedade para especificar as dimensões que o svg deve ter 
    protected _width : number = 1024;
    protected _height : number = 500;

    // Propriedade para especificar o svg gerado 
    protected _svg : any = null;
    protected _focus : any = null;

    // Propriedade para referenciar o element child 
    protected _divCloudElm : HTMLElement = null;
    
    // Propriedade para indicar que o setup foi executado 
    protected _isReady : boolean = false;

    // Subjects & Subscriptions 
    protected _subs : Subscription[] = [];

    constructor(
        protected _data : any | any[],
        protected _containerElm : HTMLElement, 
        protected _platformId : Object, 
        protected _renderer : Renderer2, 
        protected _ruler : ViewportRuler, 
        protected _maxFontSize : number = 100, 
        protected _allowedWords : string | string[] = null
    ) {
        if(isPlatformServer(this._platformId) || this._containerElm === undefined || !this._containerElm) {
            return ; 
        }

        this._subs.push(
            processData(
                this._data, 
                this._maxFontSize, 
                this._allowedWords
            ).pipe(
                switchMap((items : any[]) => {
                    console.log("Data enviada para processamento", this._data);
                    console.log("Após processamento da data para o word cloud", items);

                    if(items === undefined || !items || !items.length) {
                        return of(false);
                    }
    
                    this._processedData = items;
    
                    console.log(`Total de palavras processadas: ${ this._processedData.length }`);
    
                    return this._setUp();
                })
            ).subscribe((isOk : boolean) => {
                if(!isOk) {
                    console.log("Não foi possível processar as palavras para o word cloud");
                    return ;
                }
    
                console.log("Seteando os elementos para o word cloud");
    
                this._update();
            })
        );
    
        // Ruler sub
        this._subs.push(
            this._ruler.change().subscribe(() => {
                this._update();
            })
        );    
    };

    /**
     * Método para atualizar o conteúdo do word cloud
     */
    protected _update = () : void => {
        if(isPlatformServer(this._platformId) || this._layout === undefined || !this._layout) {
            return ;
        }

        if(!this._isReady) {
            this._layout.stop()
                .words(this._processedData)
                .start();

            this._isReady = true;
        }

        const scaleX : number = this._containerElm.offsetWidth <= this._width ? 
            this._containerElm.offsetWidth / this._width : 
            1;

        const scaleY : number = this._containerElm.offsetHeight <= this._height ? 
            this._containerElm.offsetHeight / this._height : 
            1;

        this._renderer.setStyle(
            this._divCloudElm, 
            "transform", 
            `scale(${ scaleX }, ${ scaleY })`
        );

        console.log("Atualizando o conteúdo do word cloud");
    };

    /**
     * Método para gerar o div html element que vai conter o svg do word cloud 
     */
    protected _createDivCloudElm = () : Observable<void> => {
        this._renderer.setStyle(
            this._containerElm, 
            "overflow", 
            "hidden"
        );

        this._divCloudElm = this._renderer.createElement("div");

        this._renderer.appendChild(
            this._containerElm, 
            this._divCloudElm
        );

        return timer(25).pipe(
            switchMap(() => {
                this._renderer.setAttribute(
                    this._divCloudElm, 
                    "id", 
                    this._id
                );
        
                let objStyle : any = {
                    "float": "left", 
                    "display": "block", 
                    "position": "relative",
                    "width": `${ this._width }px`, 
                    "height": `${ this._height }px`, 
                    "font-family": "Impact"
                };
    
                for(let [key, value] of Object.entries(objStyle)) {
                    this._renderer.setStyle(
                        this._divCloudElm, 
                        key, 
                        value
                    );
                }

                return of(
                    undefined
                );
            })
        );
    };
    
    /**
     * Método para solicitar a construção do svg
     */
    protected _buildSVG = () : Observable<void> => {
        this._svg = d3.select(this._divCloudElm)
            .append("svg")
            .attr("width", this._width)
            .attr("height", this._height)
            .attr(
                "viewBox", 
                `0 0 ${ this._width } ${ this._height }`
            );

        this._layout = d3.layout.cloud()
            .timeInterval(Infinity)
            .size([ this._width, this._height ])
            .padding(5)
            .rotate(() => (~~(Math.random() * 6) - 3) * 30)
            .font("Impact")
            .spiral("rectangular")
            .fontSize((d : any) => Math.round(
                this._maxFontSize * d["scale"]
            ))
            .text((d : any) => d["text"])
            .on(
                "end", 
                this._draw
            );

        return of(
            undefined
        );
    };

    /**
     * Método para setear a instância
     */
    protected _setUp = () : Observable<boolean> => this._createDivCloudElm().pipe(
        switchMap(() => this._buildSVG()), 
        switchMap(() => of(
            true
        ))
    );

    /**
     * Método para gerar o draw das palavras chaves no svg para retornar o word cloud 
     * @param outputs
     * @param bounds
     */
    protected _draw = (outputs : any[], bounds?: any) : void => {
        this._svg
            .selectAll("g")
            .remove();

        this._focus = this._svg
            .append("g")
            // Sem o tranform, os words poderiam aparecer fora da área de cobertura do svg
            .attr(
                "transform", 
                "translate(" + [this._width >> 1, this._height >> 1] + ")"
            );

        console.log(`Total de palavras para montar o word cloud: ${ outputs.length }`);

        const cloud = this._focus.selectAll(
            "text"
        ).data(
            outputs, 
            (d : any) => d.text.toLowerCase()
        );

        // update 
        cloud.transition()
            .duration(750)
            .style(
                "font-size", 
                (d : any) => `${ Math.round(this._maxFontSize * d["scale"]) }px`
            )
            .attr(
                "transform", 
                (d : any) => `translate(${ d["x"] }, ${ d["y"] }) rotate(${ d["rotate"] })`
            );

        // create
        cloud.enter()
            .append("text")
            .style("opacity", 1)
            .style(
                "font-family", 
                "Impact !important"
            )
            .style(
                "fill", 
                (d : any) => d["color"]
            )
            .attr(
                "text-anchor", 
                "middle"
            )
            .style(
                "font-size", 
                (d : any) => `${ Math.round(this._maxFontSize * d["scale"]) }px`
            )
            .attr(
                "transform", 
                (d : any) => `translate(${ d["x"] }, ${ d["y"] }) rotate(${ d["rotate"] })`
            )
            .text(
                (d : any) => d.text
            );
    };
    
    /**
     * Método para eliminar a instância
     */
    public destroy = () : void => {
        this._subs.forEach((sub : Subscription) => {
            sub.unsubscribe();
        });
    };
};