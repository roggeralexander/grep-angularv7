## Customized Word Cloud module form for Angular 7

### Importing: 
Add the MyCoreWordCloudModule to the imports of the module which will be using it.
```
import { 
    NgModule 
} from '@angular/core';

import { 
    MyCoreWordCloudModule 
} from 'ids-ng7-words-cloud';

@NgModule({
    imports: [
        ...
        MyCoreWordCloudModule
    ],
    declarations: [
        ...
    ],
    exports: [
        ...
    ],
    providers: [
        ...
    ]
})
export class AppModule {
}
```
### Example use - Component which require the rich text editor: 
```
import {
    Component, 
    ElementRef, 
    Inject,
    OnDestroy, 
    OnInit, 
    PLATFORM_ID
} from "@angular/core";

import { 
    isPlatformServer
} from "@angular/common";

@Component({
    selector: "ex-component", 
    templateUrl: "....", 
    styleUrls: ["....."]
})
export class ExampleComponent implements OnInit, OnDestroy {
    public dataCloud01 = [{ text: 'key01', size: 20 }, { text: 'key02', size: 30}, { text: 'key03', size: 25 }, { text: 'key04', size: 55 }];

    public dataCloud02 = "Of course that’s your contention. You’re a first year grad student. You just got finished readin’ some Marxian historian, Pete Garrison probably. You’re gonna be convinced of that ’til next month when you get to James Lemon and then you’re gonna be talkin’ about how the economies of Virginia and Pennsylvania were entrepreneurial and capitalist way back in 1740. That’s gonna last until next year. You’re gonna be in here regurgitating Gordon Wood, talkin’ about, you know, the Pre-Revolutionary utopia and the capital-forming effects of military mobilization… ‘Wood drastically underestimates the impact of social distinctions predicated upon wealth, especially inherited wealth.’ You got that from Vickers, Work in Essex County, page 98, right? Yeah, I read that, too. Were you gonna plagiarize the whole thing for us? Do you have any thoughts of your own on this matter? Or do you, is that your thing? You come into a bar. You read some obscure passage and then pretend, you pawn it off as your own, as your own idea just to impress some girls and embarrass my friend? See, the sad thing about a guy like you is in 50 years, you’re gonna start doin’ some thinkin’ on your own and you’re gonna come up with the fact that there are two certainties in life. One: don’t do that. And two: you dropped a hundred and fifty grand on a fuckin’ education you coulda got for a dollar fifty in late charges at the public library.";

    public dataCloud03 = new Map([
      [ "Russia", 60 ],
      [ "Belgium", 45 ],
      [ "Turkey", 48 ],
      [ "Germany", 55 ],
      [ "Hungary", 20 ],
      [ "Belarus", 20 ],
      [ "France", 60 ],
      [ "Greece", 45 ],
      [ "Italy", 48 ],
      [ "England", 55 ],
      [ "Austria", 20 ],
      [ "Switzerland", 20 ],
      [ "Spain", 60 ],
      [ "keyword01", 20 ],
      [ "keyword02", 20 ],
      [ "Ukraine", 60 ],
      [ "Bulgaria", 45 ],
      [ "Serbia", 48 ],
      [ "Czech Republic", 55 ],
      [ "Denmark", 20 ],
      [ "Finland", 20 ],
      [ "Azerbaijan", 45 ],
      [ "Kazakhstan", 48 ],
      [ "Portugal", 55 ],
      [ "Slovakia", 20 ],
      [ "Norway", 20 ],
      [ "Poland", 60 ],
      [ "Ireland", 20 ],
      [ "Croatia", 20 ],
      [ "Wales", 60 ],
      [ "Sweeden", 45 ],
      [ "Romania", 48 ],
      [ "Netherlands", 55 ],
      [ "North Ireland", 20 ],
      [ "Scotland", 45 ],
      [ "Georgia", 48 ],
      [ "Bosnia and Herzegovina", 55 ],
      [ "Armenia", 20 ],
      [ "Moldova", 20 ],
      [ "Albania", 60 ],
      [ "Lithuania", 20 ],
      [ "Macedonia", 20 ],
      [ "Slovenia", 60 ],
      [ "Latvia", 45 ],
      [ "Kosovo", 48 ],
      [ "Estonia", 55 ],
      [ "Cyprus", 20 ],
      [ "Montenegro", 45 ],
      [ "Luxembourg", 48 ],
      [ "Malta", 55 ],
      [ "Iceland", 20 ],
      [ "Andorra", 20 ],
      [ "Faroe Island", 60 ],
      [ "Liechtenstein", 20 ],
      [ "Gibraltar", 20 ],
      [ "San Marino", 60 ],
      [ "Nigeria", 45 ],
      [ "Ethiopia", 48 ],
      [ "Egypt", 55 ],
      [ "Democratic Republic of the Congo", 20 ],
      [ "South Africa", 45 ],
      [ "Tanzania", 48 ],
      [ "Kenya", 55 ],
      [ "Sudan", 20 ],
      [ "Algeria", 20 ],
      [ "Uganda", 60 ],
      [ "Morocco", 20 ],
      [ "Mozambique", 20 ],
      [ "Ghana", 60 ],
      [ "Angola", 45 ],
      [ "Ivory Coast", 48 ],
      [ "Madagascar", 55 ],
      [ "Cameroon", 20 ],
      [ "Niger", 45 ],
      [ "Burkina Faso", 48 ],
      [ "Mali", 55 ],
      [ "Malawi", 20 ],
      [ "Zambia", 20 ],
      [ "Somalia", 60 ],
      [ "Senegal", 20 ],
      [ "Chad", 20 ],
      [ "Zimbabwe", 60 ],
      [ "South Sudan", 45 ],
      [ "Rwanda", 48 ],
      [ "Tunisia", 55 ],
      [ "Guinea", 20 ],
      [ "Benin", 45 ],
      [ "Burundi", 48 ],
      [ "Togo", 55 ],
      [ "Eritrea", 20 ],
      [ "Sierra Leone", 20 ],
      [ "Libya", 60 ],
      [ "Republic of the Congo", 20 ],
      [ "Central African Republic", 20 ],
      [ "Liberia", 60 ],
      [ "Mauritania", 45 ],
      [ "Namibia", 48 ],
      [ "Botswana", 55 ],
      [ "Gambia", 20 ],
      [ "Equatorial Guinea", 45 ],
      [ "Lesotho", 48 ],
      [ "Gabon", 55 ],
      [ "Guinea-Bissau", 20 ],
      [ "Mauritius", 20 ],
      [ "Swaziland", 60 ],
      [ "Djibouti", 20 ],
      [ "Comoros", 20 ],
      [ "Cape Verde", 60 ],
      [ "Sao Tome and Principe", 45 ],
      [ "Seychelles", 48 ],
      [ "Mayotte", 45 ],
      [ "Saint Helena", 48 ],
    ]);

    public allowedWords = "poop,i,me,my,myself,we,us,our,ours,ourselves,you,your,yours,yourself,yourselves,he,him,his,himself,she,her,hers,herself,it,its,itself,they,them,their,theirs,themselves,what,which,who,whom,whose,this,that,these,those,am,is,are,was,were,be,been,being,have,has,had,having,do,does,did,doing,will,would,should,can,could,ought,i'm,you're,he's,she's,it's,we're,they're,i've,you've,we've,they've,i'd,you'd,he'd,she'd,we'd,they'd,i'll,you'll,he'll,she'll,we'll,they'll,isn't,aren't,wasn't,weren't,hasn't,haven't,hadn't,doesn't,don't,didn't,won't,wouldn't,shan't,shouldn't,can't,cannot,couldn't,mustn't,let's,that's,who's,what's,here's,there's,when's,where's,why's,how's,a,an,the,and,but,if,or,because,as,until,while,of,at,by,for,with,about,against,between,into,through,during,before,after,above,below,to,from,up,upon,down,in,out,on,off,over,under,again,further,then,once,here,there,when,where,why,how,all,any,both,each,few,more,most,other,some,such,no,nor,not,only,own,same,so,than,too,very,say,says,said,shall";

    constructor(
        protected _elmRef : ElementRef, 
        @Inject(PLATFORM_ID) protected _platformId : Object
    ) { 
    };

    ngOnInit() {

    };

    ngOnDestroy() {
    };
};
```
## API - Directive - Word Cloud
Add the element to your HTML:
```
<!-- Word cloud 01 -->
<div mcWordCloud
    [data]="dataCloud01"
    style="float: left; display: flex; width: 100%; height: 350px; overflow: hidden; flex-direction: column; align-items: center; justify-content: center; position: relative; background-color: greenyellow">
</div>

<!-- Word cloud 02 -->
<div mcWordCloud
    [data]="dataCloud02"
    [allowedWords]="allowedWords"
    style="float: left; display: flex; width: 100%; height: 350px; overflow: hidden; flex-direction: column; align-items: center; justify-content: center; position: relative; background-color: greenyellow">
</div>

<!-- Word cloud 03 -->
<div mcWordCloud
    [data]="dataCloud03"
    [allowedWords]="allowedWords"
    style="float: left; display: flex; width: 100%; height: 350px; overflow: hidden; flex-direction: column; align-items: center; justify-content: center; position: relative; background-color: greenyellow">
</div>
```
## API - Directive - MyCoreWordCloudDirective
### Inputs
|  Name                  | Type      | Description     |
| ---------------------- |---------- | --------------- |
| `data`    | `any` | Data to be used for the creation of the word cloud |
| `allowedWords`    | `'string':'string[]'` | Words that would be used as filter to generate the word cloud |
| `maxFontSize`    | `number` | Maximum of the font size for each text of the word cloud |

### Data Description 
The data for the creation of the word cloud has to be an string or an array of type object like `{ text: 'string', size: 'number'}` or an object which every key represent the text and the value represent the total amount of frequencies for the word, example: `{'text1' : 20, 'text2': 50, 'text3': 45, .... }`.
When the data type is string, it would be automatically generated the array of objects of every word and its total amount of frequencies in the string informed as input.
When thd data type is a Map, the key must be an string type and the value must be a number type.