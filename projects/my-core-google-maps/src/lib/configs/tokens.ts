import { 
    InjectionToken
} from "@angular/core"; 

// Models 
import { 
    GoogleMapConfigs
} from "../models";

export const GM_CONFIGS = new InjectionToken<GoogleMapConfigs>("./configs/tokens"); 