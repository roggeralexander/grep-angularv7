import { 
  Inject, 
  Injectable, 
  NgZone,
  PLATFORM_ID 
} from '@angular/core';

import { 
  isPlatformServer
} from "@angular/common";

import { 
  BehaviorSubject, 
  Observable, 
  of 
} from "rxjs";

// Environment vars 
import { 
  GM_CONFIGS
} from "../configs/tokens"; 

// Models 
import { 
  Coords, 
  GoogleMapListener,
  GoogleMapConfigs
} from "../models";

@Injectable()
export class GoogleMapsService {  
  // Propriedade para referenciar a leitura do api 
  public apiReady : BehaviorSubject<boolean> = new BehaviorSubject(null);

  // Propriedade para referenciar ao google Maps como behavior subject 
  protected _api : any = null;
  
  constructor(
    @Inject(GM_CONFIGS) protected _configs : GoogleMapConfigs, 
    protected _ngZone : NgZone,
    @Inject(PLATFORM_ID) protected _platformId : Object
  ) { 
    if(!isPlatformServer(this._platformId)) {
      // Solicitando a leitura do script 
      this._loadScript();
    }
  };

  /**
   * Método para solicitar a leitura do script
   */
  protected _loadScript = () : void => {
    this._ngZone.run(() => {
      const googleMapsUrl = this._configs.scriptUrl.replace(
        new RegExp(
          "\\$apiKey", 
          "gi"
        ), 
        this._configs.apiKey
      );
  
      const script = document.createElement("script");
      script.type = "text/javascript";
      script.src = googleMapsUrl;
      script.async = true; 
      script.defer = true;
  
      document.body.appendChild(script);
  
      // Callback quando o script for carregado
      script.onload = () => {
        const googleModule2 = window["google"];
  
        // Seteando o objeto do google maps
        this._api = (googleModule2 === undefined || !googleModule2 || googleModule2["maps"] === undefined || !googleModule2["maps"]) ? 
          null : 
          googleModule2["maps"];        
  
        // Indicando que o api foi lido com sucesso e encerrando o behavior subject
        this.apiReady.next(
          this._api ? 
            true : 
            false
        );
      };
  
      // Callback quando acontecer algum erro na leitura do script
      script.onerror = (err : any) =>  {
        console.log("Error loading google maps script", err);
        this.apiReady.next(false);
      };
  
      // Callback quando acontecer o abort do carregamento do script
      script.onabort = () => {
        this.apiReady.next(false);
      };  
    });
  };

  /**
   * Método para a geração de um mapa e o seu posicionamento
   * @param mapElm
   * @param coords 
   * @param zoom
   * @param draggable
   */
  public createMap = (mapElm : HTMLDivElement, coords: Coords, zoom : number = 3, draggable : boolean = false) : any => new this._api.Map(
    mapElm, 
    {
      center: coords, // Coordenadas para centralizar o map
      zoom: zoom, // Zoom a ser aplicado no map
      mapTypeControl : false,  // Indicar que o controle de tipo de mapas não deve ser mostrado
      streetViewControl : false,  // Indicar que o controle de ruas não deve ser mostrado
      mapTypeId : this._api.MapTypeId.ROADMAP // Indicar o id do tipo de map que é requerido 
    }
  );

  /**
   * Método para gerar um mark no mapa, caso ele existir a partir das coordenadas informadas 
   * Se informado um array de listeners, os listeners deverão ser declarados 
   * Se informado o icon, ele deverá ser seteado
   * @param map 
   * @param coords
   * @param icon
   * @param label
   */
  public createMarker = (map : any, coords: Coords, icon? : string, label? : string) : any => {
    if(map === undefined || !map || coords === undefined || !coords || Object.keys(coords).length !== 2) {
      return of(null); 
    }

    // Gerando o marker 
    const marker = new this._api.Marker();

    // Seteando a posição 
    marker.setPosition(
      coords
    );

    // Seteando o map 
    marker.setMap(
      map
    );

    // Seteando o icon do marker
    if(icon !== undefined && icon && icon.trim().length > 0) {
      marker.setIcon(
        icon
      );
    }

    // Seteando o label do marker 
    if(label !== undefined && label && label.trim.length > 0) {
      marker.setLabel(
        label
      );
    }

    return marker;
  };

  /**
   * Método para a geração do info window, especiricando qual o content a ser mostrado 
   * @param content
   */
  public createInfoWindow = (content : string) : any => content === undefined || !content || !content.trim().length ? 
    null : 
    new this._api.InfoWindow(
      {
        content: content
      }
  );

  /**
   * Método para setear os listeners de um marker
   * @param target 
   * @param listeners
   */
  public setListeners = (target : any, listeners : GoogleMapListener[]) : any => {
    if(target === undefined || !target || listeners === undefined || !listeners || !listeners.length) {
      return null;
    }

    listeners.forEach((obj : GoogleMapListener) => {
      target.addListener(
        obj.event, 
        obj.cb
      )
    });

    return target;
  };

  /**
   * Método para solicitar que um marker seja removido 
   * @param marker
   */
  public removeMarker = (marker : any) : void => {
    if(marker === undefined || !marker) {
      return ; 
    }

    setTimeout(() => {
      marker.setMap(
        null
      );
  
      marker = null;
    });
  };  

  /**
   * Método para atualizar o posicionamento de um marker 
   * @param marker 
   * @param coords
   */
  public updateMarkerPosition = (marker : any, coords : Coords) : void => {
    if(marker === undefined || !marker || coords === undefined || !coords) {
      return ; 
    }

    setTimeout(() => {
      marker.setPosition(
        coords
      );
    });
  };

  /**
   * Método para solicitar que o conteúdo do info window seja atualizado 
   * @param infoWindow 
   * @param content
   */
  public updateInfoWindowContent = (infoWindow : any, content : string) : void => {
    if(infoWindow === undefined || !infoWindow || content === undefined || !content || !content.trim().length) {
      return ; 
    }

    setTimeout(() => {
      infoWindow.setContent(
        content
      );
    });
  };

  /**
   * Método para obter a direção, aplicando reverse geocode, a partir das coordenadas informadas 
   * @param coords
   */
  public getAddress = (coords? : Coords) : Observable<any> => new Observable(observer => {
    if(coords === undefined || !coords || Object.keys(coords).length !== 2) {
      observer.next(null);
      observer.complete();
      return ; 
    }
      
    const geoCoder = new this._api.Geocoder;
  
    if(geoCoder === undefined || !geoCoder) {
      observer.next(null);
      observer.complete();
      return ;
    }
  
    // Aplicando reverse geocode
    geoCoder.geocode({ 'location' : coords }, (results : any, status : any) => {
      if(status !== "OK" || results === undefined || !results || results[0] === undefined || !results[0]) {
        observer.next(null);
        observer.complete();
        return ;
      }    

      console.log(results);

      const address : string = results[0]["formatted_address"]; 

      console.log(`Address obtido do geocoder: ${ address }`);

      observer.next(
        address
      );

      observer.complete();
    });
  }); 
};