/**
 * Interface para representar a coordenada
 */
export interface Coords {
    lat: number; 
    lng: number;
};

/**
 * Interface para representar os listeners a serem aplicados ao marker
 */
export interface GoogleMapListener {
    event: string, 
    cb: Function
};

/**
 * Interface para representar a data a ser vinculada ao marker do google maps
 */
export class MarkerProperties {
    coords: Coords;
    icon?: string;
};

/**
 * Interface para representar as configurações do google maps
 */
export interface GoogleMapConfigs {
    apiKey : string; 
    scriptUrl: string;
    markerIcon?: string; 
    coords?: Coords;
};