import { 
  ModuleWithProviders, 
  NgModule 
} from '@angular/core';

import { 
  CommonModule
} from "@angular/common"; 

// Tokens 
import { 
  GM_CONFIGS
} from "./configs/tokens"; 

// Models
import { 
  GoogleMapConfigs 
} from './models';

// Services 
import { 
  GoogleMapsService
} from "./services/google-maps.service"; 

@NgModule({
  imports: [
    CommonModule
  ]
})
export class MyCoreGoogleMapsModule { 
  static forRoot(configs?: GoogleMapConfigs) : ModuleWithProviders {
    return {
      ngModule: MyCoreGoogleMapsModule, 
      providers: [
        {
          provide: GM_CONFIGS, 
          useValue: configs
        }, 
        GoogleMapsService
      ] 
    }
  };
};
