/*
 * Public API Surface of my-core-google-maps
 */

// Models 
export { 
    Coords, 
    GoogleMapConfigs, 
    GoogleMapListener, 
    MarkerProperties
} from "./lib/models"; 

// Services 
export * from "./lib/services/google-maps.service"; 

// Módulos
export * from './lib/my-core-google-maps.module';
