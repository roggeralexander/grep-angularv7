import { 
  NgModule 
} from '@angular/core';

// Submodules 
import { 
  CaptureModule
} from "./capture/capture.module"; 

import { 
  PlayerModule
} from "./player/player.module";

@NgModule({
  exports: [
    CaptureModule, 
    PlayerModule
  ]
})
export class MyCoreAudioModule { }
