import { 
  ChangeDetectionStrategy, 
  ChangeDetectorRef, 
  Component,
  ElementRef, 
  EventEmitter, 
  Inject,  
  OnDestroy, 
  OnInit, 
  Output, 
  PLATFORM_ID, 
  Renderer2, 
  ViewChild
} from '@angular/core';

import { 
  isPlatformServer
} from "@angular/common"; 

import {
  from, 
  fromEvent, 
  Observable,  
  of, 
  Subscription, 
  throwError, 
  timer
} from "rxjs"; 

import { 
  catchError, 
  delay, 
  switchMap
} from "rxjs/operators"; 

// Angular CDK Modules 
import { 
  ViewportRuler
} from "@angular/cdk/scrolling"; 

// Third party modules 
import moment from "moment"; 

import Recorder from "recorder-js";

import uuid from "uuid"; 

// Toolbar component 
import { 
  ToolbarComponent
} from "../toolbar/toolbar.component"; 

// Constants 
import { 
  DEFAULT_CONFIGS
} from "../../constants"; 

// Models 
import { 
  CaptureOutput
} from "@my-core/utils"; 

// Utilities 
import { 
  blobToFile,
  getUMStream, 
  saveFileFromBlob
} from "@my-core/utils";

@Component({
  selector: 'mc-audio-capture',
  templateUrl: './audio.component.html',
  styleUrls: ['./audio.component.scss'], 
  changeDetection: ChangeDetectionStrategy.Default
})
export class AudioComponent implements OnInit, OnDestroy {
  // Instância do arquivo gerado quando tirada uma imagem 
  public file : File = null; 

  // Propriedade para conferir se o capture está permitido ou não 
  public allowed : boolean = false;

  // Propriedade para referenciar o tempo de gravação da mídia 
  public timeRecorded : string = "00:00";

  // Propriedade para referenciar se está sendo gravado ou não 
  public isRecording : boolean = false;

  // Id do component 
  protected _id : string = "mc-audio-capture-" + uuid.v1().replace(
    new RegExp("-", "gi"), 
    ""
  );

  // Propriedades exclusivas do audio
  protected _audioContext : any = null;
  protected _audioHtml : HTMLAudioElement = null; 
  protected _audioRecorder : Recorder = null;

  // Propriedade quando o media stream recorder gerar um novo arquivo 
  protected _blob : Blob = null; 

  // Propriedade para referenciar o timer 
  protected _timer : any = null; 
  
  // Subscriptions 
  protected _subs : Subscription[] = [];

  // View child - audio html element ref 
  @ViewChild(
    "audio", 
    { read: ElementRef }
  ) protected _audioRef : ElementRef = null; 

  // View child - Toolbar component 
  @ViewChild(
    ToolbarComponent
  ) protected _toolbar : ToolbarComponent = null; 

  // Outputs 
  @Output() 
  public ready : EventEmitter<void> = new EventEmitter();

  @Output() 
  public output : EventEmitter<CaptureOutput> = new EventEmitter();

  constructor(
    protected _changeDetector : ChangeDetectorRef, 
    protected _elmRef : ElementRef, 
    @Inject(PLATFORM_ID) protected _platformId : Object, 
    protected _renderer : Renderer2, 
    protected _ruler : ViewportRuler
  ) { };

  ngOnInit() {
    if(isPlatformServer(this._platformId)) {
      return ; 
    }

    // Subscription para inicializar o component 
    this._subs.push(
      timer(100).pipe(
        switchMap(() => this._initialize()), 
        catchError((err : Error) => {
          console.log("Audio web capture error", err); 
          return of(undefined);
        })
      ).subscribe(() => {
        console.log("Audio capture inicializado");
      })
    );
  };

  ngOnDestroy() {
    this._subs.forEach((sub : Subscription) => {
      sub.unsubscribe();
    });

    this.ready.complete();
    this.output.complete();
  };  

  /**
   * Método para inicializar o component
   */
  protected _initialize = () : Observable<void> => getUMStream(
    this._platformId, 
    {
      audio: true, 
      video: false
    }
  ).pipe(
    switchMap((stream : MediaStream) => {
      if(stream === undefined || !stream) {
        return throwError(
          new Error(
            "Web cam is not allowed to be used"
          )
        );
      }

      this._audioContext = new (window["AudioContext"] || window["webkitAudioContext"])();

      if(!this._audioContext) {
        return throwError(
          new Error("Web cam audio context is not allowed")
        );
      }

      this._audioRecorder = new Recorder(
        this._audioContext
      );

      return from(
        this._audioRecorder.init(
          stream
        )
      );
    }), 
    delay(25), 
    switchMap(() => {
      this._renderer.setAttribute(
        this._elmRef.nativeElement, 
        "id", 
        this._id
      );

      this.allowed = true;
              
      this._changeDetector.detectChanges(); 

      return this._setElements(); 
    }), 
    switchMap(() => this._declareToolbarListeners())
  );

  /**
   * Método para setar os elementos
   */
  protected _setElements = () : Observable<void> => {
    if(this._audioRef === undefined || !this._audioRef) {
      return throwError(
        new Error(
          "Audio output reference não existe!"
        )
      );
    }

    this._audioHtml = <HTMLAudioElement>this._audioRef.nativeElement; 

    // Subscription para o áudio, quando conseguir gravar 
    this._subs.push(
      fromEvent(
        this._audioHtml, 
        "canplay"
      ).subscribe(() => {
        console.log("Áudio pode ser usado, pois já está conseguindo gravar");
      })
    );

    return timer(25).pipe(
      switchMap(() => {
        // Hbbilitando os botões necessários do toolbar
        this._toolbar.allowBtnCancel.next(false); 
        this._toolbar.allowBtnDownload.next(false); 
        this._toolbar.allowBtnSave.next(false);
        this._toolbar.allowBtnStop.next(false); 
        this._toolbar.allowBtnRecord.next(true);

        this._changeDetector.markForCheck(); 
        this.ready.emit(); 

        return of(
          undefined
        );
      })
    );
  };

  /**
   * Método para registrar os listeners dos botões do toolbar
   */
  protected _declareToolbarListeners = () : Observable<void> => {
    // Subscription para realizar a recording de um áudio 
    this._subs.push(
      this._toolbar.record.pipe(
        switchMap(() => this._onRecord())
      ).subscribe(() => {
        console.log("Gravando o áudio do audio capture component");
      })
    );

    // Subscription para parar a reprodução 
    this._subs.push(
      this._toolbar.stop.pipe(
        switchMap(() => this._onStop())
      ).subscribe(() => {
        console.log("Parando o recording do audio capture component");
      })
    );

    // Subscription para cancelar a reprodução
    this._subs.push(
      this._toolbar.cancel.pipe(
        switchMap(() => this._onCancel())
      ).subscribe(() => {
        console.log("Cancelando a gravação atual do audio capture component");
      })
    );

    // Subscription para salvar o arquivo 
    this._subs.push(
      this._toolbar.save.pipe(
        switchMap(() => this._onSave())
      ).subscribe(() => {
        console.log("Salvando o áudio do audio capture component")
      })
    );

    // Subscription para realizar o download do arquivo 
    this._subs.push(
      this._toolbar.download.pipe(
        switchMap(() => this._onDownload())
      ).subscribe(() => {
        console.log("Realizando o download do arquivo do audio capture component"); 
      })
    );

    return timer(25).pipe(
      switchMap(() => of(
        undefined
      ))
    );
  };

  /**
   * Método para cancelar a gravação do áudio
   */
  protected _onCancel = () : Observable<void> => {
    this._showOutput(false);
    this._blob = null; 
    this.isRecording = false; 

    // Habilitar os respectivos botões 
    this._toolbar.allowBtnCancel.next(false); 
    this._toolbar.allowBtnDownload.next(false); 
    this._toolbar.allowBtnRecord.next(true); 
    this._toolbar.allowBtnSave.next(false); 
    this._toolbar.allowBtnStop.next(false);

    return of(
      undefined
    );
  };

  /**
   * Método para solicitar o download do arquivo de áudio gerado
   */
  protected _onDownload = () : Observable<void> => {
    if(this._blob === undefined || !this._blob) {
      return throwError(
        new Error(
          "Blob para audio capture component não existe!"
        )
      ); 
    }

    saveFileFromBlob(
      this._blob, 
      DEFAULT_CONFIGS["extension"]
    );

    return of(
      undefined
    );
  };

  /**
   * Método para realizar o recording do áudio
   */
  protected _onRecord = () : Observable<void> => from(
    this._audioRecorder.start()
  ).pipe(
    switchMap(() => {
      this._showOutput(false); 

      this._blob = null; 
      this.isRecording = true; 

      // Habilitar os respectivos botões 
      this._toolbar.allowBtnCancel.next(false); 
      this._toolbar.allowBtnDownload.next(false); 
      this._toolbar.allowBtnRecord.next(false); 
      this._toolbar.allowBtnSave.next(false); 
      this._toolbar.allowBtnStop.next(true);

      this._startTimer();

      return of(
        undefined
      );
    })
  );

  /**
   * Método para solicitar que o áudio seja salvo
   */
  protected _onSave = () : Observable<void> => {
    if(this._blob === undefined || !this._blob) {
      return throwError(
        new Error(
          "Blob do audio capture component não existe!"
        )
      ); 
    }

    const fileName =uuid.v4().replace(
      new RegExp("-", "gi"), 
      ""
    );

    return blobToFile(
      this._blob, 
      fileName, 
      DEFAULT_CONFIGS['extension']
    ).pipe(
      switchMap((file : File) => {
        console.log("Arquivo gerado do áudio capture component com sucesso", file);

        const obj : CaptureOutput = {
          file: this.file, 
          type: "audio"
        };

        this.output.emit(
          obj
        );

        return of(
          undefined
        );
      })
    );
  };

  /**
   * Método para parar a gravação do áudio
   */
  protected _onStop = () : Observable<void> => from(
    this._audioRecorder.stop()
  ).pipe(
    switchMap(({ blob, buffer }) => {
      this._showOutput(
        true
      );
      
      this._blob = blob;
      this.isRecording = false;

      this._changeDetector.markForCheck();

      this._stopTimer();
      this._audioHtml.src = URL.createObjectURL(
        blob
      );

      // Botões do toolbar 
      this._toolbar.allowBtnCancel.next(true); 
      this._toolbar.allowBtnDownload.next(true); 
      this._toolbar.allowBtnRecord.next(false); 
      this._toolbar.allowBtnSave.next(true); 
      this._toolbar.allowBtnStop.next(false);

      return of(
        undefined
      );
    })
  );

  /**
   * Método para inicializar o tempo de reprodução
   */
  protected _startTimer = () : void => {
    this.timeRecorded = "00:00";
      let start = moment();

      this._timer = setInterval(() => {
        let now = moment();
        this.timeRecorded = moment(now.diff(start)).format("mm:ss");
        this._changeDetector.detectChanges();
    }, 1000);
  };

  /**
   * Método para parar o tempo de reprodução
   */
  protected _stopTimer = () : void => {
    if(this._timer === undefined || !this._timer) {
      return ; 
    }

    clearInterval(this._timer);
    this._timer = null;
    this.timeRecorded = "00:00";
    this._changeDetector.detectChanges();
  };

  /**
   * Método para mostrar o output 
   * @param flag
   */
  protected _showOutput  = (flag : boolean) : void => {
    if(this._audioHtml === undefined || !this._audioHtml) {
      return ; 
    }

    this._renderer.setStyle(
      this._audioHtml, 
      "display", 
      flag ?  
        "block" : 
        "none"
    );

    this._changeDetector.markForCheck();
  };
};
