import { 
    NgModule
} from "@angular/core"; 

import { 
    CommonModule
} from "@angular/common";

// Angular CDK Modules 
import { 
    ScrollDispatchModule
} from "@angular/cdk/scrolling"; 

// Components
import { 
    AudioComponent 
} from './components/audio/audio.component';

import { 
    ToolbarComponent 
} from './components/toolbar/toolbar.component'; 

@NgModule({
    imports: [
        CommonModule, 
        ScrollDispatchModule
    ], 
    declarations: [
        AudioComponent, 
        ToolbarComponent
    ], 
    exports: [
        AudioComponent
    ], 
    entryComponents: [
        AudioComponent
    ]
})
export class CaptureModule { };