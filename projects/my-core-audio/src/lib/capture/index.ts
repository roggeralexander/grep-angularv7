// Components 
export { 
    AudioComponent as AudioCaptureComponent
} from "./components/audio/audio.component";

// Módulo
export {
    CaptureModule as AudioCaptureModule
} from "./capture.module";