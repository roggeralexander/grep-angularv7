// Components 
export {
    PlayerComponent as AudioPlayerComponent
} from "./components/player/player.component"; 

// Modules 
export {
    PlayerModule as AudioPlayerModule
} from "./player.module"; 