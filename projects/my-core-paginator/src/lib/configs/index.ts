import {
    InjectionToken
} from "@angular/core";

// Models
import { 
    PaginatorPlaceholders, 
    PaginatorPlaceholdersLangs
} from "../models";

// Tokens 
export const PLACEHOLDERS = new InjectionToken<PaginatorPlaceholders>("./©onfigs/tokens");
export const PLACEHOLDERS_LANGS = new InjectionToken<PaginatorPlaceholdersLangs>("./configs/tokens");