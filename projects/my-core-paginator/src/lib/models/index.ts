import { 
    Page
} from "ids-ng7-utils";

/**
 * Interface para representar o model de uma instacia de controle de página
 */
export interface PageCtrl extends Page {
    // Label type
    labelType: "label" | "tooltip";

    // Flag para mostrar ou esconder a página
    show?: boolean;

    // Icon a ser vinculado à página
    icon?: string;
};

/**
 * Interface para representar os labels que o paginator pode ter
 */
export interface PaginatorLabels {
    btnPrevious?: string; 
    btnNext?: string; 
    btnFirst?: string; 
    btnLast?: string; 
    perPage?: string; 
    of?: string;
    goTo?: string;
};

/**
 * Interface que representa o tipo de dados para o output do paginator
 */
export interface PaginatorOutput {
    pageNumber: number; 
    pageSize: number; 
    totalItems: number;
};