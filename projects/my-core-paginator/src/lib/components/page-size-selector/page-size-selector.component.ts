import { 
  AfterViewInit, 
  ChangeDetectionStrategy, 
  ChangeDetectorRef, 
  Component,
  ElementRef, 
  EventEmitter, 
  Inject,  
  Input, 
  OnDestroy, 
  Output, 
  PLATFORM_ID, 
  ViewChild 
} from '@angular/core';

import { 
  isPlatformServer 
} from '@angular/common';

import {
  fromEvent, 
  Subscription
} from "rxjs";

@Component({
  selector: 'mc-page-size-selector',
  templateUrl: './page-size-selector.component.html',
  styleUrls: ['./page-size-selector.component.scss'], 
  changeDetection: ChangeDetectionStrategy.Default
})
export class PageSizeSelectorComponent implements AfterViewInit, OnDestroy {
  @Input()
  get pageSizeOptions() : number[] {
    return this._pageSizeOptions;
  };
  set pageSizeOptions(values : number[]) {
    if(values === undefined || !values || !values.length || this._pageSizeOptions === values) {
      return;
    }

    this._pageSizeOptions = values; 
    this._changeDetector.markForCheck();
  };
  protected _pageSizeOptions : number[] = [];

  @Input() 
  get pageSize() : number {
    return this._pageSize;
  };
  set pageSize(value : number) {
    if(value === undefined || value === null || this._pageSize === value) {
      return ; 
    }

    this._pageSize = value; 
    this._changeDetector.markForCheck();
  };
  protected _pageSize : number = null; 

  // Selector child reference 
  @ViewChild(
    "select", 
    { 
      read: ElementRef
    }
  ) protected _selectRef : ElementRef = null;

  // Subjects & Subscriptions 
  protected _subs : Subscription[] = [];

  // Output para indicar qual a opção selecionada 
  @Output() 
  public pageSizeChange : EventEmitter<number> = new EventEmitter();

  constructor(
    protected _changeDetector : ChangeDetectorRef, 
    @Inject(PLATFORM_ID) protected _platformId : Object
  ) { 
    if(isPlatformServer(this._platformId)) {
      return ; 
    }
  };

  ngAfterViewInit() {
    if(isPlatformServer(this._platformId) || !this._selectRef || this._selectRef === undefined) {
      return ; 
    }

    this._subs.push(
      fromEvent(
        this._selectRef.nativeElement, 
        "change"
      ).subscribe(
        this._handlePageSizeChange
      )
    );
  };

  ngOnDestroy() {
    this._subs.forEach((sub : Subscription) => sub.unsubscribe());
    this._subs = null; 

    this.pageSizeChange.complete(); 
    this.pageSizeChange = null;
  };

  /**
   * Callback quando acontecer a escolha de uma opção de página 
   * @param $event
   */
  protected _handlePageSizeChange = ($event : any) : void => {
    this.pageSizeChange.emit(
      parseInt(
        $event["target"]["value"]
      )
    );
  };
};
