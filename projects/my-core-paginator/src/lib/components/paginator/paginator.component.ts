import { 
  AfterViewInit, 
  ChangeDetectionStrategy, 
  ChangeDetectorRef, 
  Component, 
  ElementRef, 
  EventEmitter,
  Inject, 
  Input, 
  OnChanges, 
  OnDestroy, 
  Output, 
  PLATFORM_ID, 
  Renderer2, 
  SimpleChanges, 
  ViewChild
} from '@angular/core';

import { 
  isPlatformServer
} from "@angular/common";

import { 
  fromEvent, 
  Observable, 
  of, 
  Subscription,
  Subject
} from "rxjs";

import { 
  catchError, 
  switchMap
} from "rxjs/operators";

// Third party modules 
import uuid from "uuid";

// Constantes 
import { 
  PAGE_SIZE, 
  PAGE_SIZE_OPTIONS
} from "../../constants"; 

// Models 
import { 
  PageCtrl, 
  PaginatorOutput 
} from '../../models';

// Services 
import { 
  PaginatorIntService
} from "../../services/paginator-int.service";

// Utilities 
import { 
  createPages, 
  Page 
} from "ids-ng7-utils";

@Component({
  selector: 'mc-paginator',
  templateUrl: './paginator.component.html',
  styleUrls: ['./paginator.component.scss'], 
  changeDetection: ChangeDetectionStrategy.Default
})
export class PaginatorComponent implements OnChanges, AfterViewInit, OnDestroy {
  // Input para representar o tamanho de items 
  @Input() 
  get totalItems() : number {
    return this._totalItems;
  };
  set totalItems(value : number) {
    if(isPlatformServer(this._platformId) || value === undefined || value === null || value < 0 || this._totalItems === value) {
      return ; 
    }

    this._totalItems = value;
    this._changeDetector.markForCheck();
  };
  protected _totalItems : number = 0;

  // Input para indicar o índice atual da página 
  @Input() 
  get pageNumber() : number {
    return this._pageNumber;
  };
  set pageNumber(value : number) {
    if(isPlatformServer(this._platformId) || value === undefined || value === null || value < 0 || this._pageNumber === value) {
      return ; 
    }

    this._pageNumber = value;
    this._changeDetector.markForCheck();
  };
  protected _pageNumber : number = 1;

  // Input para o tamanho da página 
  @Input() 
  get pageSize() : number {
    return this._pageSize;
  };
  set pageSize(value : number) {
    if(isPlatformServer(this._platformId) || value === undefined || value === null || value < 0 || this._pageSize === value) {
      return ; 
    }

    this._pageSize = value;
    this._updateDisplayedPageSizeOptions();
  };
  protected _pageSize : number = PAGE_SIZE;

  // Input 
  @Input() 
  get pageSizeOptions()  : number[] {
    return this._pageSizeOptions;
  };
  set pageSizeOptions(arr : number[]) {
    if(isPlatformServer(this._platformId) || arr === undefined || !arr || !arr.length) {
      return ;
    }

    this._pageSizeOptions = arr;
    this._updateDisplayedPageSizeOptions();
  };
  protected _pageSizeOptions : number[] = PAGE_SIZE_OPTIONS;

  // Input para representar o idioma do sistema 
  @Input() 
  get lang() : string {
    return this._lang;
  };
  set lang(lang : string) {
    if(lang === undefined || !lang || !lang.trim().length || this._lang === lang) {
      return ;
    }

    this._lang = lang;
    this._changeDetector.markForCheck();
  };
  protected _lang : string = null; 

  // Input para indicar se só serão gerados os botões que correspondem à navegação 
  @Input() 
  get showNumberBtns() : boolean {
    return this._showNumberBtns;
  };
  set showNumberBtns(flag : boolean) {
    if(flag === undefined || !flag || this._showNumberBtns === flag) {
      return ; 
    }

    this._showNumberBtns = flag; 
    this._changeDetector.markForCheck();
  };
  protected _showNumberBtns : boolean = true;

  // Input para indicar se os botões first last deverão ser indexados 
  @Input() 
  get showFirstLastBtns() : boolean {
    return this._showFirstLastBtns;
  };
  set showFirstLastBtns(flag : boolean) {
    if(flag === undefined || !flag || this._showFirstLastBtns === flag) {
      return ; 
    }

    this._showFirstLastBtns = flag; 
    this._changeDetector.markForCheck();
  };
  protected _showFirstLastBtns : boolean = true;

  // Input para especificar que será aceito o input para escrever o número respectivo 
  @Input() 
  get withInputGoTo() : boolean {
    return this._withInputGoTo;
  };
  set withInputGoTo(flag : boolean) {
    if(flag === undefined || flag === null || this._withInputGoTo === flag) {
      return ; 
    }

    this._withInputGoTo = flag;
  };
  protected _withInputGoTo : boolean = true;

  // Propriedade para referenciar às opções de paginação que serão mostradas 
  public displayedPageSizeOptions : number[] = [];

  // Propriedade para representar os controles das páginas 
  public pagesCtrls : PageCtrl[] = [];

  // Propriedade para representar o range label 
  public rangeLabel : string = null; 

  // View child do input go to 
  @ViewChild(
    "inputGoTo", 
    {
      read: ElementRef
    }
  ) protected _inputRef : ElementRef = null; 

  // Input 
  protected _inputHtml : HTMLInputElement = null; 

  // Propriedade para referenciar ao id do component 
  protected readonly _id : string = `mc-paginator-` + uuid.v4().replace(
    new RegExp("-", "gi"), 
    ""
  );

  // Subjects & Subscriptions 
  protected _worker : Subject<void> = new Subject();
  protected _subs : Subscription[] = [];

  // Output para emissão das informações de mudança no paginator 
  @Output() 
  public output : EventEmitter<PaginatorOutput> = new EventEmitter();

  constructor(
    protected _changeDetector : ChangeDetectorRef, 
    protected _elmRef : ElementRef, 
    protected _labels : PaginatorIntService, 
    @Inject(PLATFORM_ID) protected _platformId : Object, 
    protected _renderer : Renderer2
  ) { 
    if(isPlatformServer(this._platformId)) {
      return ; 
    }

    // Subscription para o worker 
    this._subs.push(
      this._worker.pipe(
        switchMap(() => this._updateCtrls()), 
        catchError((err : Error) => {
          console.log("Error inicializando o component: ", err.message);
          
          return of(
            undefined
          );
        })
      ).subscribe(
        () => {
          this._changeDetector.markForCheck(); 
        }
      )
    );

    setTimeout(() => {
      this._worker.next();
    });
  };

  ngOnChanges(changes : SimpleChanges) {
    if(isPlatformServer(this._platformId)) {
      return ; 
    }

    setTimeout(() => {
      this._worker.next();
    });
  };

  ngAfterViewInit() {
    if(isPlatformServer(this._platformId)) {
      return ;
    }

    this._renderer.setAttribute(
      this._elmRef.nativeElement, 
      "id", 
      this._id
    );

    if(this._inputRef !== undefined && this._inputRef) {
      this._inputHtml = <HTMLInputElement>this._inputRef.nativeElement;

      // Subscription quando existir uma mudança de valores da página solicitada 
      this._subs.push(
        fromEvent(
          this._inputHtml, 
          "change"
        ).subscribe(
          this._handleInputGoToChange
        )
      );
    }
  };

  ngOnDestroy() {
    this._subs.forEach((sub : Subscription) => sub.unsubscribe());
    this._subs = null;

    this.output.complete(); 
    this.output = null;

    this._worker.complete(); 
    this._worker = null; 
  };

  /**
   * Método para atualizar os controles do componente
   */
  protected _updateCtrls = () : Observable<void> => createPages(
    this._pageNumber, 
    {
      totalPages: this.getTotalPages(), 
      maxBtns: 7, 
      ellipses: "..."
    }
  ).pipe(
    switchMap((pages : Page[]) => this._createPageCtrls(
      pages
    )), 
    switchMap(() => this._setRangeLabel())
  );

  /**
   * Método para gerar os controles de páginas 
   * @param pages
   */
  protected _createPageCtrls = (pages : Page[]) : Observable<void> => {
    let pagesCtrls : PageCtrl[] = [];

    if(this._showNumberBtns) {
      pages.forEach((page : Page) => {
        let pageCtrl : any = Object.assign(
          {}, 
          page
        );
  
        pageCtrl["labelType"] = "label"; 
        pageCtrl["show"] = true;
  
        pagesCtrls.push(
          pageCtrl
        );
      });
    }

    // Adicionando o botão previous 
    if(this._pageNumber > 1) {
      pagesCtrls.unshift({
        labelType: "label", 
        label: "<", 
        value: this.pageNumber - 1, 
        show: this.hasPage("previous")
      });
  
      if(this._showFirstLastBtns) {
        // Adicionando o botão first page 
        pagesCtrls.unshift({
          labelType: "label", 
          label: "<<", 
          value: 1,
          show: this.hasPage("previous")
        });
      }
    }

    // Adicionando o botão next 
    if(this._pageNumber < this.getTotalPages()) {
      pagesCtrls.push({
        labelType: "label", 
        label: ">", 
        value: this.pageNumber + 1, 
        show: this.hasPage("next")
      });
  
      if(this._showFirstLastBtns) {
        // Adicionando o botão first page 
        pagesCtrls.push({
          labelType: "label", 
          label: ">>", 
          value: this.getTotalPages(), 
          show: this.hasPage("next")  
        });
      }
    }

    this.pagesCtrls = pagesCtrls;

    // Atualizando os valodators do input go to 
    if(this._withInputGoTo && this._inputHtml !== undefined && this._inputHtml) {
      this._inputHtml.min = "1";
      this._inputHtml.max = this.getTotalPages().toString(); 
    }

    return of(
      undefined
    );
  };

  /**
   * Método para setear o range label
   */
  protected _setRangeLabel = () : Observable<void> => {
    this.rangeLabel = this._labels.getRangeLabel(
      this._pageNumber, 
      this._pageSize, 
      this._totalItems
    );

    return of(
      undefined
    );
  };

  /**
   * Método para atualizar as opções de paginação
   */
  protected _updateDisplayedPageSizeOptions = () : void => {
    if(!this._pageSize) {
      this._pageSize = this._pageSizeOptions.length !== 0 ? 
        this._pageSizeOptions[0] : 
        PAGE_SIZE;
    }

    this.displayedPageSizeOptions = Array.from(
      this._pageSizeOptions
    );

    if(this.displayedPageSizeOptions.indexOf(this._pageSize) === -1) {
      this.displayedPageSizeOptions.push(
        this._pageSize
      );
    }

    // Ordenando a numeração 
    this.displayedPageSizeOptions.sort((a : number, b : number) => a - b);

    this._changeDetector.markForCheck();
  };

  /**
   * Método para obter o label a partir do seu nome 
   * @param type
   */
  public getLabel = (type : "btnPrevious" | "btnNext" | "btnFirst" | "btnLast" | "perPage" | "goTo") : string => this._labels.getLabel(
    type
  );

  /**
   * Método para obter o total de páginas
   */
  public getTotalPages = () : number => Math.ceil(
    this._totalItems / this._pageSize
  );

  /**
   * Método para saber se há uma página anterior ou seguinte 
   * @param type 
   */
  public hasPage = (type : "previous" | "next") : boolean => {
    let flag : boolean = false; 

    const totalPages : number = this.getTotalPages();

    switch(type) {
      case "previous": 
        flag = this._pageNumber > 1 && totalPages > 1;
        break; 
      default: 
        flag = (totalPages > 1) && (this._pageNumber < totalPages);
    }

    return flag;
  };

  /**
   * Método para especificar se o botão de seleção deve ser escondido ou não 
   */
  public showPageSize = () : boolean => this.displayedPageSizeOptions.length > 1 && this.displayedPageSizeOptions.some(
    (value : number) => value < this._totalItems
  );

  /**
   * Callback quando escolhida uma nova opção de tamanho de página
   * @param pageSize
   */
  public handlePageSizeChange = (pageSize : any) : void => {
    if(pageSize === this._pageSize) {
      return ; 
    }

    const previousStart = (this._pageNumber - 1) * this._pageSize + 1;
    this._pageSize = pageSize; 
    this._pageNumber = Math.ceil(previousStart / this._pageSize);

    if(this._withInputGoTo && this._inputHtml !== undefined && this._inputHtml) {
      this._inputHtml.value = "";
    }

    setTimeout(() => {
      this._emitOutput();

      this._worker.next();
    });
  };

  /**
   * Método de callback quando clicada uma determiada página 
   * @param page
   */
  public handlePageClicked = (page : number) : void => {
    if(this._pageNumber === page) {
      return ; 
    }

    this._pageNumber = page; 

    if(this._withInputGoTo && this._inputHtml !== undefined && this._inputHtml) {
      this._inputHtml.value = "";
    }

    setTimeout(() => {
      this._emitOutput();

      this._worker.next();
    });
  };

  /**
   * Callback quando existir uma mudança de valor do input go to
   * @param $event
   */
  protected _handleInputGoToChange = ($event : any) : void => {
    const value : string = $event["target"]["value"];

    if(value === undefined || !value || !value.trim().length) {
      return ; 
    }

    const page : number = parseInt(
      value
    );

    let pageToSelect : number = 1;

    if(page <= 0) {
      pageToSelect = 1;
    } else if(page >= this.getTotalPages()) {
      pageToSelect = this.getTotalPages();
    } else { 
      pageToSelect = page;
    }

    this.handlePageClicked(
      pageToSelect
    );
  };

  /**
   * Método para emissão do output
   */
  protected _emitOutput = () : void => {
    this.output.emit({
      pageNumber: this._pageNumber, 
      pageSize: this._pageSize, 
      totalItems: this._totalItems
    });
  };
};
