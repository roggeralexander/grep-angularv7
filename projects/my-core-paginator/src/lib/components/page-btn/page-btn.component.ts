import { 
  AfterViewInit,
  ChangeDetectionStrategy, 
  ChangeDetectorRef, 
  Component, 
  ElementRef, 
  EventEmitter, 
  Inject,
  Input,  
  OnDestroy, 
  Output, 
  PLATFORM_ID 
} from '@angular/core';

import { 
  isPlatformServer
} from "@angular/common";

import { 
  fromEvent, 
  Subscription
} from "rxjs";

// Models 
import { 
  PageCtrl
} from "../../models";

@Component({
  selector: 'mc-page-btn',
  templateUrl: './page-btn.component.html',
  styleUrls: ['./page-btn.component.scss'],
  changeDetection: ChangeDetectionStrategy.Default
})
export class PageBtnComponent implements AfterViewInit, OnDestroy {
  // Input para a data do botão da página
  @Input()
  get data() : PageCtrl {
    return this._data;
  };
  set data(data : PageCtrl) {
    if(isPlatformServer(this._platformId) || data === undefined || !data || !Object.keys(data).length) {
      return ; 
    }

    this._data = data;
    this._changeDetector.markForCheck();
  };
  protected _data : PageCtrl = null;

  @Input() 
  get isCurrent() : boolean {
    return this._isCurrent;
  };
  set isCurrent(flag : boolean) {
    if(isPlatformServer(this._platformId) || flag === undefined || !flag) {
      return ; 
    }

    this._isCurrent = flag;
    this._changeDetector.markForCheck();
  };
  protected _isCurrent : boolean = false;

  @Input() 
  get isFirst() : boolean {
    return this._isFirst;
  };
  set isFirst(flag : boolean) {
    if(isPlatformServer(this._platformId) || flag === undefined || flag === null || this._isFirst === flag) {
      return ;
    }

    this._isFirst = flag; 
    this._changeDetector.markForCheck();
  };
  protected _isFirst : boolean = false;

  @Input() 
  get isLast() : boolean {
    return this._isFirst;
  };
  set isLast(flag : boolean) {
    if(isPlatformServer(this._platformId) || flag === undefined || flag === null || this._isLast === flag) {
      return ;
    }

    this._isLast = flag; 
    this._changeDetector.markForCheck();
  };
  protected _isLast : boolean = false;

  // Output para especificar que uma página foi escolhida 
  @Output()
  public clicked : EventEmitter<number> = new EventEmitter();

  // Subscriptions 
  protected _subs : Subscription[] = [];

  constructor(
    protected _changeDetector : ChangeDetectorRef, 
    protected _elmRef : ElementRef, 
    @Inject(PLATFORM_ID) protected _platformId : Object
  ) { 
    if(isPlatformServer(this._platformId)) {
      return ; 
    }
  };

  ngAfterViewInit() {
    if(isPlatformServer(this._platformId) || this._elmRef === undefined || !this._elmRef) {
      return ; 
    }

    // Subscription
    this._subs.push(
      fromEvent(
        this._elmRef.nativeElement, 
        "click"
      ).subscribe(
        this._handleClick
      )
    );
  };  
  
  ngOnDestroy() {
    this._subs.forEach((sub : Subscription) => sub.unsubscribe());
    this._subs = null;

    this.clicked.complete();
    this.clicked = null;
  };

  /**
   * Método para quando o botão da paǵinação for clicada
   */
  protected _handleClick = () : void => {
    if(this._isCurrent) {
      return ; 
    }

    this.clicked.emit(
      this._data["value"]
    );
  };

  /**
   * Método para obter as classes para o botão
   */
  public getClasses = () : string => {
    return [
      "btn-page", 
      this._isCurrent ? "active" : "", 
      this._isFirst ? "is-first" : "", 
      this._isLast ? "is-last" : ""
    ].join(" ");
  };
};
