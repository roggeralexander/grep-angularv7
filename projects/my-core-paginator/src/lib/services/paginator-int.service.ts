import { 
  Inject, 
  Injectable, 
  PLATFORM_ID 
} from '@angular/core';

import { 
  isPlatformServer
} from "@angular/common";

// Constantes 
import { 
  DEFAULT_LABELS
} from "../constants";

// Models 
import { 
  PaginatorLabels
} from "../models";

@Injectable()
export class PaginatorIntService {
  // Propriedade para referenciar aos labels 
  protected _labels : PaginatorLabels = Object.assign(
    {}, 
    DEFAULT_LABELS
  ); 

  constructor(
    @Inject(PLATFORM_ID) protected _platformId : Object
  ) { 
    if(isPlatformServer(this._platformId)) {
      return ; 
    }
  };

  /**
   * Método para setear os labels customizados informados pelo usuário 
   * @param labels
   */
  public setLabels = (labels : PaginatorLabels) : void => {
    if(labels === undefined || !labels || !Object.keys(labels).length) {
      return ; 
    }

    this._labels = Object.assign(
      {}, 
      DEFAULT_LABELS, 
      labels
    );
  };

  /**
   * Método para obter um determinado label
   * @param type
   */
  public getLabel = (type : "btnPrevious" | "btnNext" | "btnFirst" | "btnLast" | "perPage" | "goTo") : string => {
    if(type === undefined || !type) {
      return null; 
    }

    return this._labels[type];
  };

  /**
   * Método para obter o range label especificando a página inicial e página final 
   * @param pageNumber
   * @param pageSize
   * @param totalItems
   */
  public getRangeLabel = (pageNumber : number, pageSize : number, totalItems : number) : string => {
    const start : number = (pageNumber - 1) * pageSize + 1; 
    const end : number = pageNumber * pageSize;

    return `${ start } - ${ end } ${ this._labels["of"] } ${ totalItems }`;
  };
};
