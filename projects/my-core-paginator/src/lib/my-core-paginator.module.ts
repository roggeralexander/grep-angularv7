import { 
  NgModule 
} from '@angular/core';

import {
  CommonModule
} from "@angular/common";

// Components
import { 
  PageBtnComponent 
} from './components/page-btn/page-btn.component';

import { 
  PageSizeSelectorComponent 
} from './components/page-size-selector/page-size-selector.component';

import { 
  PaginatorComponent 
} from './components/paginator/paginator.component'; 

// Services 
import { 
  PaginatorIntService
} from "./services/paginator-int.service";

@NgModule({
  imports: [
    CommonModule
  ], 
  declarations: [
    PageBtnComponent,
    PaginatorComponent,
    PageSizeSelectorComponent,
  ], 
  exports: [
    PaginatorComponent
  ], 
  providers: [
    PaginatorIntService
  ],
  entryComponents: [
    PageBtnComponent,
    PageSizeSelectorComponent,
    PaginatorComponent    
  ]
})
export class MyCorePaginatorModule { };
