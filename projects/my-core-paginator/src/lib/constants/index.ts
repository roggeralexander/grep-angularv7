import { 
    PaginatorLabels
} from "../models";

/**
 * Constante para especificar o valor do tamanho de cada página por defult
 */
export const PAGE_SIZE : number = 50;

/**
 * Constante para determinar o total de items por página
 */
export const PAGE_SIZE_OPTIONS : number[] = [
    10, 
    25,
    50,
    100
];

/**
 * Constante default dos placeholders quando o key principal for o botão ou label
 */
export const DEFAULT_LABELS : PaginatorLabels = {
    btnPrevious: "Previous", 
    btnNext: "Next", 
    btnFirst: "First page", 
    btnLast: "Last page", 
    perPage: "Items per page", 
    of: "of", 
    goTo: "Navigate to page"
};