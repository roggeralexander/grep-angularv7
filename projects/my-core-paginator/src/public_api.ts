/*
 * Public API Surface of my-core-paginator
 */

// Componentes 
export { 
    PaginatorComponent as MyCorePaginatorComponent
} from "./lib/components/paginator/paginator.component";

// Models 
export { 
    PaginatorLabels as MyCorePaginatorLabels,
    PaginatorOutput as MyCorePaginatorOutput 
} from "./lib/models/index";

// Services 
export { 
    PaginatorIntService as MyCorePaginatorLabelsService
} from "./lib/services/paginator-int.service";

// Módulo
export * from './lib/my-core-paginator.module';
