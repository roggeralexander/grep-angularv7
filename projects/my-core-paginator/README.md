## Paginator para Angular 7

### Forma de uso: 
Adicione o MyCorePaginatorModule importando no módulo no qual você deseje usar como import (dependência). 
```
import { 
    NgModule 
} from '@angular/core';

import {
    MyCorePaginatorModule, 
    MyCorePaginatorOutput
} from "ids-ng7-paginator";
 
@NgModule({
    imports: [
        ...
        MyCorePaginatorModule
    ],
    declarations: [
        ...
    ],
    exports: [
        ...
    ],
    providers: [
        ...
    ]
})
export class AppModule {
  // Propriedade para referenciar o page number 
  public pageNumber : number = 3;

  // Propriedade para especificar o page size 
  public pageSize : number = 250;

  /**
   * Callback para quando uma página mudar
   */
  public onPageChange = ($event : MyCorePaginatorOutput) : void => {
    console.log("Objeto mudou quando escolhida a página:", $event);
    this.pageNumber = $event["pageNumber"];
    this.pageSize = $event["pageSize"];
  };
}
```
### Exemplo de uso - Declaração de variáveis no component
```
@Component({
  selector: 'abc',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'] 
})
export class AppComponent {

};
```
### Exemplo de uso - Implementação no template
```
<div style="float: left; display: block; width: 90%; height: 350px; position:relative; margin-left: 5%; background-color:greenyellow;">
  <mc-paginator 
    [totalItems]="12000"
    [pageNumber]="pageNumber"
    [pageSize]="pageSize"
    (output)="onPageChange($event)">
  </mc-paginator>
</div>
```
## API 
### Componente: MyCorePaginatorComponent
#### Inputs
|  Nome                  | Tipo      | Descrição     |
| ---------------------- |---------- | --------------- |
| `totalItems`    | `'number'` | Total de items para geração da paginação |
| `pageNumber`    | `'number'` | Página atual |
| `pageSize`    | `'number'` | Tamanho da página |
| `pageSizeOptions`    | `'number[]'` | Opções de tamanho de página |
| `lang`    | `'string'` | Idioma do sistema |
| `showNumberBtns`    | `'boolean'` | Para mostrar ou manter escondidos os botões numéricos |
| `showFirstLastBtns`    | `'boolean'` | Para mostrar os botões de navegação para ir à primeira ou última página |
| `withInputGoTo`    | `'boolean'` | Para adicionar um input para inserir a página à qual o usuário deseja navegar |

#### Outputs
|  Nome                  | Tipo      | Descrição     |
| ---------------------- |---------- | --------------- |
| `ouput`    | `EventEmitter<MyCorePaginatorOutput>` | Emitter com as informações de mudança que aconteceu no paginator |

### Model: MyCorePaginatorOutput
Model que representa ao output que é emitido pelo component de paginação
|  Nome                  | Tipo      | Descrição     |
| ---------------------- |---------- | --------------- |
| `pageNumber`    | `'number'` | Nro atual da página de navegação no paginator |
| `pagaSize`    | `'number'` | Tamanho de cada página |
| `totalItems`    | `'number'` | Total de items com os quais o paginator está trabalhando |

### Service MyCorePaginatorLabelsService
Service para setear os labels a serem mostrados no paginator
#### Métodos
|  Nome                  | Parâmetros      | Tipo de Retorno      | Description     |
| ---------------------- | ---------- | --------------- | --------------- |
| `setLabels`    | `labels:MyCorePaginatorLabels` | `void` | Método para setear os labels que serão usados pelo paginator component |

## Outros Models que são de configuração do módulo
### Model: MyCorePaginatorLabels 
Model para estabelecer quais os labels ou placeholders que o paginator deverá renderizar.
|  Nome                  | Tipo      | Descrição     |
| ---------------------- |---------- | --------------- |
| `btnPrevious`    | `'string'` | Label correspondente ao botão previous |
| `btnNext`    | `'string'` | Label correspondente ao botão next |
| `btnFirst`    | `'string'` | Label correspondente ao botão first |
| `btnLast`    | `'string'` | Label correspondente ao botão last |
| `perRange`    | `'string'` | Label correspondente ao placeholder perRange |
| `of`    | `'string'` | Label relacionado ao range das páginas (ex: $start 'of' $end) |
| `goTo`    | `'string'` | Label correspondente ao input de navegação |

### Exemplo de representação de um objeto de tipo MyCorePaginatorLabels
```
let obj : PaginatorPlaceholders = {
    btnPrevious: "Previous", 
    btnNext: "Next", 
    btnFirst: "First page", 
    btnLast: "Last page", 
    perPage: "Items per page", 
    of: "of", 
    goTo: "Navigate to page"
};
```