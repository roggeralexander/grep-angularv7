export interface Position {
    top?: boolean; 
    bottom?: boolean;
    left?: boolean;
    right?: boolean;
    center?: boolean;
    middle?: boolean;
};