import { 
  AfterViewInit,
  ChangeDetectionStrategy, 
  ChangeDetectorRef, 
  Component, 
  ElementRef, 
  EventEmitter,
  Inject, 
  Input, 
  OnDestroy, 
  Output, 
  PLATFORM_ID, 
  Renderer2, 
  ViewChild
} from '@angular/core';

import { 
  isPlatformServer
} from "@angular/common";

import { 
  ViewportRuler
} from "@angular/cdk/scrolling"; 

import { 
  from,
  Observable, 
  of, 
  Subject,
  throwError, 
  timer,
} from "rxjs"; 

import { 
  catchError, 
  switchMap
} from "rxjs/operators";

// Third party libraries 
import domtoimage from "dom-to-image";
import uuid from "uuid";

// Parent class 
import { 
  BaseWatermark
} from "../../classes/base-watermark";  

// Utilities 
import { 
  blobToFile,
  CaptureOutput,
  hexToRgba, 
  ImgLoader
} from "@my-core/utils";

@Component({
  selector: 'mc-watermark-to-img',
  templateUrl: './to-img.component.html',
  styleUrls: ['./to-img.component.scss'], 
  providers: [{
    provide: BaseWatermark, 
    useExisting: ToImgComponent
  }], 
  changeDetection: ChangeDetectionStrategy.Default
})
export class ToImgComponent extends BaseWatermark implements AfterViewInit, OnDestroy{
  // Input para referenciar o arquivo ou src 
  @Input() 
  get src() : string | File {
    return this._src;
  };
  set src(src : string | File) {
    if(isPlatformServer(this._platformId) || src === undefined || !src || this._src === src) {
      return ; 
    }

    this._src = src;

    setTimeout(() => {
      this._worker.next();
    });

    console.log("Arquivo passado", this._src);
  };
  protected _src : string | File = null;

  // Div contenedor da imagem 
  @ViewChild(
    "canvas", 
    { read: ElementRef }
  ) protected _imgDivRef : ElementRef = null;

  // Propriedade para referenciar o html da img div
  protected _imgDivHtml : HTMLDivElement = null; 

  @ViewChild(
    "parentContent", 
    { read : ElementRef }
  ) protected _parentContentRef : ElementRef = null; 

  // Propriedade para referenciar o html do parent content
  protected _parentContentHtml : HTMLDivElement = null; 

  // ImgLoader 
  protected _imgLoader : ImgLoader = null; 

  // Workers 
  protected _worker : Subject<void> = new Subject();

  // Blob correlacionado ao arquivo gerado 
  protected _blob : Blob = null; 

  // Outputs 
  @Output() 
  public output : EventEmitter<CaptureOutput> = new EventEmitter();

  constructor(
    changeDetector : ChangeDetectorRef, 
    elmRef : ElementRef, 
    @Inject(PLATFORM_ID) platformId : Object, 
    renderer : Renderer2, 
    ruler : ViewportRuler
  ) { 
    super(
      changeDetector, 
      elmRef, 
      platformId, 
      renderer, 
      ruler
    );

    // Subscription do worker 
    this._worker.pipe(
      switchMap(() => this._initialize())
    ).subscribe(
      () => console.log("Watermark to img component gerado com sucesso")
    );
  };

  ngAfterViewInit() {
    this._onAfterViewInit();

    if(isPlatformServer(this._platformId) || this._imgDivRef === undefined || !this._imgDivRef || this._parentContentRef === undefined || !this._parentContentRef) {
      return ;      
    }

    this._imgDivHtml = <HTMLDivElement>this._imgDivRef.nativeElement;
    this._parentContentHtml = <HTMLDivElement>this._parentContentRef.nativeElement;
  };

  ngOnDestroy() {
    this._onDestroy();

    if(this._imgLoader) {
      this._imgLoader.destroy(); 
      this._imgLoader = null;
    }

    this._blob = null; 

    this._worker.complete();
    this.output.complete();
  };

  /**
   * Método para inicializar o component
   */
  protected _initialize() : Observable<void> {
    if(this._imgLoader) {
      this._imgLoader.destroy(); 
      this._imgLoader = null;
    }

    this._imgLoader = new ImgLoader(
      this._src instanceof File ? 
        URL.createObjectURL(this._src) : 
        this._src
    );

    return this._imgLoader.loaded.pipe(
      switchMap(() => this._update())
    );
  };

  /**
   * Método para atualizar o component
   */
  protected _update() : Observable<void> {
    // Obtendo as dimensões do content 
    if(this._imgDivHtml === undefined || !this._imgDivHtml || this._parentContentHtml === undefined || !this._parentContentHtml || this._imgLoader === undefined || !this._imgLoader || this._imgLoader.img === undefined || !this._imgLoader.img || this._content === undefined || !this._content || this._content.html === undefined || !this._content.html) {
      return throwError(
        new Error(
          "Img Loader e/ou content não existe(m)"
        )
      );
    }

    return timer(25).pipe(
      switchMap(() => this._updateImgContainer()), 
      switchMap(() => this._updateContent()), 
      switchMap(() => this._save()), 
      catchError((err : Error) => {
        console.log("Error aconteceu", err);

        this._blob = null; 

        return of(
          undefined
        );
      })
    );
  };

  /**
   * Método para atualização das propriedades da div que contém a imagem
   */
  protected _updateImgContainer = () : Observable<void> => {
    // Seteando os estilos de acordo à imagem 
    let objStyles : any = {
      width: `${ this._imgLoader.img.width }px`, 
      height: `${ this._imgLoader.img.height }px`, 
      background: `url(${ this._imgLoader.img.src })`, 
    }; 

    this._applyStyle(
      this._imgDivHtml, 
      objStyles
    );

    this._changeDetector.markForCheck();

    return of(
      undefined
    );
  };

  /**
   * Método para atualizar o container
   */
  protected _updateContent = () : Observable<void> => {
    const clientRect = this._content.html.getBoundingClientRect();
    const w : number = clientRect.width;
    const h : number = clientRect.height;
    const rect : number = w + 2 * h;

    let parentContentStyle : any = {
      width: `${ rect }px`, 
      height: `${ rect }px`, 
      background: hexToRgba(
        this._bgColor, 
        this._opacity
      ), 
      "transform": `rotate(${ this._degress }deg)`
    };

    // De acordo às posições
    const posStyle : any = this._getPosStyle(
      rect
    ); 

    if(posStyle) {
      for(let [key, value] of Object.entries(posStyle)) {
        parentContentStyle[key] = value;
      }
    }

    this._applyStyle(
      this._parentContentHtml, 
      parentContentStyle
    );

    let contentStyle : any = {
      left: `${ (rect - w)/2 }px`, 
      top: `${ (rect - h) }px`
    };

    this._applyStyle(
      this._content.html, 
      contentStyle
    );

    this._changeDetector.markForCheck();

    return of(
      undefined
    );
  };

  /**
   * Método para solicitar que o conteúdo html seja salvo
   */
  protected _save = () : Observable<void> => from(
    domtoimage.toBlob(
      this._elmRef.nativeElement
    )
  ).pipe(
    switchMap((blob : Blob) => {
      this._blob = blob;
      
      return blobToFile(
        this._blob, 
        uuid.v4().replace(
          new RegExp("-", "gi"), 
          ""
        ), 
        "png"
      );
    }), 
    switchMap((file : File) => {
      this.output.emit({
        type: "image", 
        file: file
      });

      return of(
        undefined
      );
    })
  );
};
