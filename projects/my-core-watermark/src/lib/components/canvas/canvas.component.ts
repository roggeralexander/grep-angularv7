import { 
  AfterViewInit,
  ChangeDetectionStrategy, 
  ChangeDetectorRef, 
  Component, 
  ElementRef, 
  Inject, 
  OnDestroy, 
  OnInit, 
  PLATFORM_ID, 
  Renderer2
} from '@angular/core';

import { 
  ViewportRuler
} from "@angular/cdk/scrolling"; 

import { 
  Observable, 
  of, 
  throwError, 
  timer
} from "rxjs"; 

import { 
  switchMap
} from "rxjs/operators";

// Parent class 
import { 
  BaseWatermark
} from "../../classes/base-watermark";  

// Utilities 
import { 
  hexToRgba
} from "@my-core/utils";

@Component({
  selector: 'mc-watermark-canvas',
  templateUrl: './canvas.component.html',
  styleUrls: ['./canvas.component.scss'], 
  providers: [{
    provide: BaseWatermark, 
    useExisting: CanvasComponent
  }],
  changeDetection: ChangeDetectionStrategy.Default
})
export class CanvasComponent extends BaseWatermark implements OnInit, AfterViewInit, OnDestroy {
  constructor(
    changeDetector : ChangeDetectorRef, 
    elmRef : ElementRef, 
    @Inject(PLATFORM_ID) platformId : Object, 
    renderer : Renderer2, 
    ruler : ViewportRuler
  ) { 
    super(
      changeDetector, 
      elmRef, 
      platformId, 
      renderer, 
      ruler
    );
  };

  ngOnInit() {
    this._onInit();
  };

  ngAfterViewInit() {
    this._onAfterViewInit();
  };

  ngOnDestroy() {
    this._onDestroy();
  };

  /**
   * Método para inicializar o component
   */
  protected _initialize() : Observable<void> {
    // Obtendo as dimensões do content 
    if(this._content === undefined || !this._content || this._content.html === undefined || !this._content.html) {
      return throwError(
        new Error(
          "Content não lido"
        )
      );
    }

    const w : number = this._content.html.offsetWidth;
    const h : number = this._content.html.offsetHeight;
    const rect : number = 2 * h + w;

    // Atualizando as dimensões correspondentes ao container - elm ref 
    let elmRefStyle : any = {
      width: `${ rect }px`, 
      height: `${ rect }px`, 
      background: hexToRgba(
        this._bgColor, 
        this._opacity
      )
    };

    let contentStyle : any = {
      "margin-top": `${ h + w }px`,
      "margin-left": `${ h }px`
    };

    // Position style 
    const posStyle : any = this._getPosStyle(
      rect
    ); 

    if(posStyle) {
      for(let [key, value] of Object.entries(posStyle)) {
        elmRefStyle[key] = value;
      }
    }

    // Aplicando estilos ao element ref 
    this._applyStyle(
      this._elmRef.nativeElement, 
      elmRefStyle
    );

    // Aplicando styles ao conteiner 
    this._applyStyle(
      this._content.html, 
      contentStyle
    );

    return timer(25).pipe(
      switchMap(() => this._update())
    );
  };

  /**
   * Método para determinar a escala e a rotação de acordo ao view port rule
   */
  protected _update() : Observable<void> {
    // Obtendo as dimensões do content 
    if(this._content === undefined || !this._content || this._content.html === undefined || !this._content.html) {
      return throwError(
        new Error(
          "Content não existe"
        )
      );
    }

    // Obtendo o width do view port ruler
    const width : number = this._ruler.getViewportRect().width;

    let scale : number = this._scale ? 
      this._scale : 
      1;

    if(this._scale === undefined || this._scale === null) {
      if(width < this._maxWidthForScale) {
        scale = parseFloat((width / this._maxWidthForScale).toFixed(2));
      }
    }

    this._renderer.setStyle(
      this._elmRef.nativeElement, 
      "transform", 
      `scale(${ scale }) rotate(${ this._degress }deg)`
    );

    return of(
      undefined
    );
  };
};
