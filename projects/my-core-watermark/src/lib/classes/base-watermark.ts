import { 
    ChangeDetectorRef,
    ContentChild, 
    ElementRef,
    Input,
    Renderer2
} from "@angular/core";

import { 
    isPlatformServer
} from "@angular/common";

import { 
    ViewportRuler
} from "@angular/cdk/scrolling"; 

import { 
    Observable, 
    of, 
    Subscription, 
    timer
} from "rxjs"; 

import { 
    catchError, 
    switchMap
} from "rxjs/operators";

// Directive 
import { 
    ContentDirective
} from "../directives/content.directive"; 

// Third party modules 
import uuid from "uuid";

// Models 
import { 
    Position
} from "../models";
  
// Utilities 
import { 
    isColorOk
} from "@my-core/utils";  

export abstract class BaseWatermark {
    // Input para especificar o background color
    @Input() 
    get bgColor() : string {
        return this._bgColor;
    };
    set bgColor(value : string) {
        if(value === undefined || !value || !value.trim().length || this._bgColor === value || !isColorOk(value)) {
            return ;
        }

        this._bgColor = value;
    };
    protected _bgColor : string = "#000000";

    // Input para especificar o opacity
    @Input() 
    get opacity() : number {
        return this._opacity;
    };
    set opacity(value : number) {
        if(value === undefined || value === null || value <= 0 || value > 1 || this._opacity === value) {
            return ;
        }

        this._opacity = value;
    };
    protected _opacity : number = 0.5;

    // Input para especificar a posição do component
    @Input() 
    get position() : Position {
        return this._position;
    };
    set position(position : Position) {
        if(position === undefined || !position || Object.keys(position).length !== 2 || this._position === position) {
            return ;
        }

        this._position = position;
    };
    protected _position : Position = {
        top: true, 
        right: true
    };

    // Input para especificar o máximo de width para aplicar scale
    @Input() 
    get maxWidthForScale() : number {
        return this._maxWidthForScale;
    };
    set maxWidthForScale(value : number) {
        if(value === undefined || value === null || value < 320 || this._maxWidthForScale === value) {
            return ;
        }

        this._maxWidthForScale = value;
    };
    protected _maxWidthForScale : number = 1024;

    // Input para especificar a escala de forma manual
    @Input()
    get scale() : number {
        return this._scale;    
    };
    set scale(scale : number) { 
        if(scale === undefined || scale === null || (scale < 0 && scale > 1) || this._scale === scale) {
        return ;
        }

        this._scale = scale;
    };
    protected _scale : number = null;

    // Input para especificar os degrees de rotação
    @Input() 
    get degrees() : number {
        return this._degress;
    };
    set degress(value : number) {
        if(value === undefined || value === null || this._degress === value) {
            return ;
        }

        this._degress = value;
    };
    protected _degress : number = 45;
    
    // Id do compnent 
    protected _id : string = `mc-watermark-` + uuid.v4().replace(
        new RegExp("-", "gi"), 
        ""
    );

    // Content Child 
    @ContentChild(
        ContentDirective
    ) protected _content : ContentDirective = null;
    
    // Subscriptions 
    protected _subs : Subscription[] = [];

    constructor(
        protected _changeDetector : ChangeDetectorRef, 
        protected _elmRef : ElementRef, 
        protected _platformId : Object, 
        protected _renderer : Renderer2, 
        protected _ruler : ViewportRuler
    ) {
        if(!isPlatformServer(this._platformId)) {
            return  ;
        }
    };

    /**
     * Método para aplicar estilos a um determinado element a partir do objeto estilos
     * @param html 
     * @param style
     */
    protected _applyStyle = (html : HTMLElement, style : any) : void => {
        if(html === undefined || !html || style === undefined || !style || !Object.keys(style).length) {
            return ;
        }

        for(let [key, value] of Object.entries(style)) {
            this._renderer.setStyle(
                html, 
                key, 
                value
            );
        }
    };

    /**
     * Método para quando acontecer o ngOnInit
     */
    protected _onInit = () : void => {
        if(isPlatformServer(this._platformId)) {
            return ; 
        }

        // Subscription para inicialziar o component 
        this._subs.push(
            timer(100).pipe(
                switchMap(() => this._initialize()), 
                catchError((err : Error) => {
                    console.log("Error gerando o watermark do canvas", err);
                    return of(
                        undefined
                    );
                })
            ).subscribe(
                () => {
                    this._changeDetector.markForCheck();
                    console.log("Base Watermark component inicializado");
                }
            )
        );

        // Subscription para o ruler 
        this._subs.push(
            this._ruler.change().pipe(
                switchMap(() => this._update()),
                catchError((err : Error) => {
                    console.log("Error ao atualizar o watermark do canvas", err);
                    return of(
                        undefined
                    );
                })
            ).subscribe(
                () => {
                    this._changeDetector.markForCheck();
                    console.log("Rotado e escalado desde o ruler");
                }
            )
        );
    };

    /**
     * Método para quando acontecer o after view init
     */
    protected _onAfterViewInit = () : void => {
        if(isPlatformServer(this._platformId)) {
            return ; 
        }
      
        this._renderer.setAttribute(
            this._elmRef.nativeElement, 
            "id", 
            this._id
        );
    };

    /**
     * Método a ser invocado quando acontecer o ngOnDestroy
     */
    protected _onDestroy = () : void => {
        this._subs.forEach((sub : Subscription) => {
            sub.unsubscribe();
        });
    };

    /**
     * Método para obter o posicionamento, em object style, a partir do rect informado 
     * @param rect
     */
    protected _getPosStyle = (rect : number) : any => {
        // Position style 
        let posStyle = null; 

        if(this._position.top && this._position.right) {
            posStyle = {
                top: `-${ rect / 2 }px`, 
                right: `-${ rect / 2 }px`
            };
        } else if(this._position.top && this._position.left) {
            posStyle = {
                top: `-${ rect / 2 }px`, 
                left: `-${ rect / 2 }px`
            };
        } else if(this._position.bottom && this._position.right) {
            posStyle = {
                bottom: `-${ rect / 2 }px`, 
                right: `-${ rect / 2 }px`
            };
        } else if(this._position.bottom && this._position.left) {
            posStyle = {
                bottom: `-${ rect / 2 }px`, 
                left: `-${ rect / 2 }px`
            };
        }

        return posStyle;
    };

    // Métodos a serem implementados pelas classes concretas 
    protected abstract _initialize() : Observable<void>;
    protected abstract _update() : Observable<void>;
};