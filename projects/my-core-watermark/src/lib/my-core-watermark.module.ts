import { 
  NgModule 
} from '@angular/core';

import {
  CommonModule
} from "@angular/common";

// Angular CDK Modules 
import { 
  ScrollDispatchModule
} from "@angular/cdk/scrolling";

// Component
import { 
  CanvasComponent 
} from './components/canvas/canvas.component';

import { 
  ToImgComponent 
} from './components/to-img/to-img.component';

// Directives
import { 
  ContentDirective 
} from './directives/content.directive';

@NgModule({
  imports: [
    CommonModule, 
    ScrollDispatchModule
  ],
  declarations: [
    CanvasComponent,
    ToImgComponent,
    ContentDirective
  ], 
  exports: [
    CanvasComponent, 
    ToImgComponent,
    ContentDirective
  ], 
  entryComponents: [
    CanvasComponent,
    ToImgComponent
  ]
})
export class MyCoreWatermarkModule { }
