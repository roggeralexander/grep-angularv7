import { 
  AfterViewInit,
  Directive, 
  ElementRef, 
  Inject, 
  PLATFORM_ID 
} from '@angular/core';

import { 
  isPlatformServer
} from "@angular/common";

@Directive({
  selector: '[mcWatermarkContent]'
})
export class ContentDirective implements AfterViewInit {
  public html : HTMLElement = null;

  constructor(
    protected _elmRef : ElementRef, 
    @Inject(PLATFORM_ID) protected _platformId : Object
  ) { };

  ngAfterViewInit() {
    if(isPlatformServer(this._platformId)) {
      return ; 
    }

    this.html = this._elmRef.nativeElement;
  };
};
