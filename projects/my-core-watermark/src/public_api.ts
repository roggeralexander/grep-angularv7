/*
 * Public API Surface of my-core-watermark
 */

// Components 
export { 
    CanvasComponent as WatermarkCanvasComponent
} from "./lib/components/canvas/canvas.component";

// Modules
export {
    MyCoreWatermarkModule as WatermarkModule
} from './lib/my-core-watermark.module';
