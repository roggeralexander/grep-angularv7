import { 
    ChangeDetectorRef,
    ElementRef, 
    QueryList, 
    Renderer2
} from "@angular/core";

import { 
    AnimationBuilder
} from "@angular/animations";

import { 
    ViewportRuler
} from "@angular/cdk/scrolling";
  
import {
    BehaviorSubject, 
    merge, 
    Observable, 
    of, 
    Subscription,
    Subject, 
    throwError
} from "rxjs";

import { 
    catchError, 
    switchMap
} from "rxjs/operators";

// Third party library 
import uuid from "uuid";

import elementResizeEvent from "element-resize-event";

// Classes 
import { 
    Animation, 
    ArrowNavs
} from "@my-core/utils";

// Controllers 
import { 
    SliderButton
} from "../controllers/slider-button";

import { 
    SliderItem
} from "../controllers/slider-item";

import { 
    SliderSelector
} from "../controllers/slider-selector";

// Hammer Gestures 
import { 
    HammerGestures
} from "../utilities/hammer";

// Models 
import {
    SliderConfigs
} from "../../models";

/**
 * Classe para implementação da classe base de slider
 */
export abstract class BaseSlider {
    // Subjects & Subscriptions 
    protected _subs : Subscription[] = [];

    // Propriedade para especificar o id 
    protected readonly _id : string = `sl-` + uuid.v4().replace(
        new RegExp("-", "gi"), 
        ""
    );

    // Propriedade para obter o total de elementos 
    protected get _totalItems() : number {
        return this._items !== undefined && this._items && this._items.length > 0 ? 
            this._items.length :
            0;
    };

    // Instância do arrow navs 
    protected _arrowNavs : ArrowNavs = null;

    // Instância do animation para i current item
    protected _animation : Animation = null;

    // Propriedade para referenciar à instância do animation do previous item 
    protected _previousAnimation : Animation = null;

    // Propriedade para definir ao previous item 
    protected _previousItem : SliderItem = null;

    // Propriedade para referenciar ao item atual 
    protected _currentItem : SliderItem = null;

    // Propriedade para indicar se a animação está sendo aplicado 
    protected _isInAnimation : boolean = false;

    // Propriedade para especificar o parent node do container ref 
    protected _parent : HTMLElement = null;

    // Propriedade para especificar o page width 
    protected get _parentWidth() : number {
        if(this._parent === undefined || !this._parent) {
            return 0;
        }

        return this._parent.offsetWidth;
    };

    // Propriedade para especificar o type 
    protected _type : string = null;

    // Subject para solicitar que seja aplicada a animação do slider 
    protected _playSlider : Subject<boolean> = new Subject();

    // Propriedade para referenciar o timeout do slider 
    protected _sliderTimeout : any = null; 

    // Propriedade para especificar o progress do interval 
    protected _intervalProgress : any = null;

    // Propriedade para especificar quando o interval começou 
    protected _intervalStartedAt : number = null;

    // Propriedade para saber se o slider está com o interval ativo ou não 
    protected _isIntervalActive : boolean = false;

    // Propriedade para especificar que a animação começou com um determinado index
    protected _startedWith : number = null;

    // Propriedade para especificar que a animação foi completada com um determinado index 
    protected _completedWith : number = null;

    // Propriedade para especificar se os índices devem ser considerados quando a animação é aplicado
    protected _allowStartedCompletedWith : boolean = true;

    // Propriedade para representar gerenciar os touch events 
    protected _gestures : HammerGestures = null;

    // Output para emitir a página atual antes de aplicar a animação
    public started : Subject<number> = new Subject();
    
    // Output para emitir a página atual após de aplicar a animação
    public completed : Subject<number> = new Subject();
    
    // Output para indicar se o slider está reproducindo ou não
    public isPlaying : BehaviorSubject<boolean> = new BehaviorSubject(false);

    // Output para especificar o progress para a próxima interação
    public progress : BehaviorSubject<number> = new BehaviorSubject(0);

    // Output para indicar que o slider está pronto para ser usado 
    public isReady : BehaviorSubject<boolean> = new BehaviorSubject(false);

    constructor(
        protected _options : SliderConfigs, 
        protected _animationBuilder : AnimationBuilder,
        protected _changeDetector : ChangeDetectorRef,
        protected _containerRef : ElementRef,
        protected _items : QueryList<SliderItem>,
        protected _platformId : Object, 
        protected _renderer : Renderer2, 
        protected _ruler : ViewportRuler,
        protected _btns : QueryList<SliderButton> = null,
        protected _selectors: QueryList<SliderSelector> = null 
    ) { 
        // Subscription para inicializar o slider
        setTimeout(() => {
            this._subs.push(
                of(null).pipe(
                    switchMap(() => this._initialize()), 
                    switchMap(() => this._setItemsSequence()), 
                    switchMap(() => this._resizeItems()), 
                    switchMap(() => this._initializeAnimation()),
                    switchMap(() => this._initializeArrowNavs()), 
                    switchMap(() => this._initializeGestures()), 
                ).subscribe(() => {
                    // Indicando que o slider está pronto para uso
                    this.isReady.next(
                        true
                    );

                    this._playSlider.next(
                        true
                    );
                })
            );
        });

        // Subscription para o play slider 
        this._subs.push(
            this._playSlider.subscribe((flag : boolean) => {
                if(this._gestures !== undefined && this._gestures) {
                    this._gestures.allow(
                        flag
                    );
                }

                this._initSliderTimeout(
                    flag
                );
            })
        );
    };

    /**
     * Método para registrar o resize do element parent
     */
    protected _detectResizeParent = () : Observable<void> => new Observable(observer => {
        elementResizeEvent(
            this._parent, 
            () => {
                observer.next(); 
                observer.complete();
            }
        );
    });

    /**
     * Método para solicitar que o slider seja redimensionado quando detectar que o parent element foi redimensionaod 
     * ou quando houver redimensionamento do view port
     */
    protected _resizeSlider = () : Observable<any> => merge(
        this._detectResizeParent(), 
        this._ruler.change()
    );

    /**
     * Método para setear os elementos do component
     */
    protected _initialize = () : Observable<void> => {
        if(this._containerRef !== undefined && this._containerRef) {
            this._renderer.setAttribute(
                this._containerRef.nativeElement, 
                "id", 
                this._id
            );

            this._parent = this._containerRef.nativeElement.parentNode;

            // Subscription para redimensionar o slider
            this._subs.push(
                this._resizeSlider().pipe(
                    switchMap(() => {
                        if(this._currentItem === undefined || !this._currentItem) {
                            return throwError(
                                new Error(
                                    "Current item não definido"
                                )
                            );
                        }

                        this._playSlider.next(
                            false
                        );
    
                        return of(
                            null
                        );
                    }),
                    switchMap(() => this._resizeItems()), 
                    switchMap(() => this._resetAnimation()), 
                    catchError((err : Error) => {
                        console.log("Error no slider: ", err.message); 

                        return of(
                            null
                        );
                    })
                ).subscribe((flag : boolean) => {
                    if(flag === null) {
                        return ; 
                    }

                    this._playSlider.next(
                        true
                    );
                })
            );
        }

        // Subscriptions dos botões 
        if(this._btns !== undefined && this._btns && this._btns.length > 0) {
            this._btns.forEach((btn : SliderButton) => {
                // Subscription para aplicar a ação solicitada 
                this._subs.push(
                    btn.applyAction.subscribe((action : string) => {
                        this._applyBtnAction(action);
                    })
                );
            });
        }

        // Subscriptions para os selectors 
        if(this._selectors !== undefined && this._selectors && this._selectors.length > 0) {
            this._selectors.forEach((selector : SliderSelector) => {
                // Subscription para saber qual o elemento deve ser escolhido a partir do selector 
                this._subs.push(
                    selector.choose.subscribe((index : number) => {
                        this._scrollToItem(index);
                    })
                );
            });
        }

        return of(
            undefined
        );
    };

    /**
     * Método para inicializar os arrows navs
     */
    protected _initializeArrowNavs = () : Observable<void> => {
        if(this._parent === undefined || !this._parent || !this._options["keyboard"]) {
            return throwError(
                new Error(
                    "Parent element não existe"
                )
            )
        }

        this._arrowNavs = new ArrowNavs(
            this._parent, 
            this._platformId
        );

        // Subscription para o arrow navs 
        if(this._arrowNavs) {
            this._subs.push(
                this._arrowNavs.scrollTo.subscribe((scrollTo : "previous" | "next") => {
                    this._scrollToNode(scrollTo);
                })
            );
        }

        return of(
            undefined
        );
    };

    /**
     * Método para estabelecer a sequência dos items
     */
    protected _setItemsSequence = () : Observable<void> => {
        this._items.forEach((item : SliderItem, index : number) => {
            item.index = index;

            const previousItem : SliderItem = this._getItemByIndex(
                this._calculatePageToNav(
                    index, 
                    "previous"
                )
            );

            const nextItem : SliderItem = this._getItemByIndex(
                this._calculatePageToNav(
                    index
                )
            );

            // Seteando os intems para a lista encadeada 
            item.setItem(
                previousItem, 
                "previous"
            );

            item.setItem(
                nextItem
            );
        });

        // Seteando o elemento inicial 
        this._currentItem = this._getItemByIndex(
            this._options["startWith"] > 0 && this._options["startWith"] < this._items.length ?
                this._options["startWith"] :
                0
        );

        // Atualizando os selectors
        this._updateSelectors();

        if(this._type === "infinite") {
            this._cloneFirstLastItem();
        }

        return of(
            undefined
        );
    };

    /**
     * Método para inicializar os gestures
     */
    protected _initializeGestures = () : Observable<void> => {
        if(!this._options["touch"]) {
            return of(
                undefined
            );
        }
        
        this._gestures = new HammerGestures(
            this._animationBuilder, 
            this._containerRef, 
            this._platformId, 
            this._options["animationTime"], 
            this._type
        );

        // Seteando o current item para o slider hammer 
        this._gestures.currentItem = this._currentItem;

        // Subscription para saber a direção de navegação a aplicar
        this._subs.push(
            this._gestures.scrollTo.subscribe((action : "previous" | "next") => {
                this._scrollToNode(
                    action
                );
            })
        );

        // Subscription para saber se o hammer está em operação. 
        // Caso estiver, parar o interval 
        this._subs.push(
            this._gestures.isPlaying.subscribe(() => {
                // Só quando informado o tempo de intervalo 
                if(this._options["intervalTime"] <= 0) {
                    return ;
                }

                this._initSliderTimeout(
                    false
                );
            })
        );

        // Subscription para saber se a operação do hammer foi cancelada 
        // Caso certo, ativar o interval
        this._subs.push(
            this._gestures.cancelled.subscribe(() => {
                // Só quando informado o tempo de intervalo 
                if(this._options["intervalTime"] <= 0) {
                    return ;
                }

                this._initSliderTimeout(
                    true
                );
            })
        );
            
        return of(
            undefined
        );
    };       

    /**
     * Método para atualizar o currentIndex dos selectors
     */
    protected _updateSelectors = () : void => {
        // Caso existam os selectors 
        if(this._selectors !== undefined && this._selectors && this._selectors.length > 0) {
            this._selectors.forEach((selector : SliderSelector, index : number) => {
                // Estabelecendo o índice
                selector.index = index;

                // Indicando qual o selector atual ou current
                selector.isCurrent = this._currentItem.index === index;

                this._changeDetector.markForCheck();
            });
        }
    };

    /**
     * Método para ser aplicado unicamente quando o type for infinite
     */
    protected _cloneFirstLastItem = () : void => {
        if(this._type !== "infinite") {
            return ;
        }

        const firstItem : SliderItem = this._getItemByIndex(0);
        const lastItem : SliderItem = this._getItemByIndex(
            this._totalItems - 1
        );

        if(firstItem !== undefined && firstItem && lastItem !== undefined && lastItem) {
            let copyFI = <HTMLElement>firstItem.elm.cloneNode(true);
            let copyLI = <HTMLElement>lastItem.elm.cloneNode(true);
    
            // Estabelecendo um atributo para o first Item e last Item
            copyFI.setAttribute(
                "element", 
                `${ this._id }-first`
            );

            copyLI.setAttribute(
                "element", 
                `${ this._id }-last`
            );

            copyFI.style.display = "inline-flex";
            copyLI.style.display = "inline-flex";

            // Adicionar ao container de sliders o primeiro elemento 
            this._renderer.insertBefore(
                this._containerRef.nativeElement, 
                copyLI,
                firstItem.elm
            );

            // Adicionar ao conteiners o último elemento 
            this._renderer.appendChild(
                this._containerRef.nativeElement, 
                copyFI
            );            
        }
    };

    /**
     * Método para solicitar o redimensionamento dos items
     */
    protected _resizeItems = () : Observable<void> => {
        let totalWidth : number = 0;
        let containerStyles : any = {};
        let itemStyles : any = {};

        switch(this._type) {
            case "fade":   
                totalWidth = this._parentWidth;
                    
                containerStyles = {
                    width: `${ totalWidth }px`, 
                    height: "100%",
                    padding: "0px", 
                    margin: "0px", 
                    "list-style": "none !important"                        
                };

                itemStyles = {
                    width: `${ this._parentWidth }px`, 
                    height: "100%",
                    display: "block",
                    position: "absolute", 
                    top: "0px", 
                    left: "0px"
                };

                break;
            case "cube": 
                totalWidth = this._parentWidth;
                        
                containerStyles = {
                    width: `${ totalWidth }px`, 
                    height: "100%",
                    padding: "0px", 
                    margin: "0px", 
                    "list-style": "none !important"                        
                };

                itemStyles = {
                    width: `${ this._parentWidth }px`, 
                    height: "100%",
                    display: "block",
                    position: "absolute", 
                    top: "0px", 
                    left: "0px"
                };

                break;
            
            case "finite": 
                totalWidth = this._parentWidth * this._totalItems;

                containerStyles = {
                    width: `${ totalWidth }px`, 
                    display: "flex", 
                    padding: "0px", 
                    margin: "0px", 
                    "list-style": "none !important"
                };

                itemStyles = {
                    width: `${ this._parentWidth }px`, 
                    display: "inline-flex !important"
                };

                break;
            case "infinite": 
                totalWidth = this._parentWidth * (this._totalItems + 2);

                containerStyles = {
                    width: `${ totalWidth }px`, 
                    display: "flex", 
                    padding: "0px", 
                    margin: "0px", 
                    "list-style": "none !important"
                };

                itemStyles = {
                    width: `${ this._parentWidth }px`, 
                    display: "inline-flex !important"
                };

                break;
        }

        // Aplicando styles para o container
        this._setStyles(
            containerStyles
        );

        this._changeDetector.markForCheck();

        // Aplicando styles para os items 
        this._items.forEach((item : SliderItem, index : number) => {
            item.posY = 0;

            if(["fade", "cube"].indexOf(this._type) !== -1) {
                itemStyles["z-index"] = this._currentItem.index === index ? 
                    1 : 
                    0;

                itemStyles["opacity"] =  this._currentItem.index === index ? 
                    1 : 
                    0;

                itemStyles["display"] = this._currentItem.index === index ? 
                    "block" : 
                    "none";
            }

            item.setStyles(
                itemStyles
            );

            switch(this._type) {
                case "fade":
                    item.posX = 0;
                    break;
                case "cube":
                    item.posX = 0;
                    break;
                case "finite": 
                    item.posX = this._parentWidth * index;
                    break;
                case "infinite": 
                    item.posX = this._parentWidth * (index + 1);
                    break;
            }
        });

        // Caso for infinite, aplicar os estilos aos elementos clonados 
        if(this._type === "infinite") {
            this._applyOnClonedElms(
                itemStyles
            );
        }

        this._changeDetector.markForCheck();

        return of(
            undefined
        );
    };

    /**
     * Método para solicitar que seja aplicado o estilo nos elementos clonados
     * @param styleObj
     */
    protected _applyOnClonedElms = (styleObj : any) : void => {
        if(styleObj !== undefined && styleObj !== null && Object.keys(styleObj).length > 0) {
            let copyFI = <HTMLElement>document.querySelector(`[element=${ this._id }-first]`);
            let copyLI = <HTMLElement>document.querySelector(`[element=${ this._id }-last]`);
        
            if(copyFI !== undefined && copyFI && copyLI !== undefined && copyLI) {
                for(let [key, value] of Object.entries(styleObj)) {
                    copyFI["style"][key] = value;
                    copyLI["style"][key] = value;
                }
            }
        }
    };

    /**
     * Método para obter uma instância de item directive pelo seu índice 
     * @param i
     */
    protected _getItemByIndex = (i : number = 0) : SliderItem => this._items.find((obj : SliderItem, index : number) => index === i);

    /**
     * Método para calcular a página de acordo à ação
     * @param page 
     * @param action
     */
    protected _calculatePageToNav = (page : number, action : "previous" | "next" = "next") : number => {
        let toPage : number = null;
        
        if(action === "next") {
            toPage = page + 1 <= this._totalItems - 1 ?
            page + 1 : 
                0;
        } else if(action === "previous") {
            toPage = page - 1 >= 0 ?
                page - 1 : 
                this._totalItems - 1;
        }

        return toPage;
    };

    /**
     * Método para solicitar que seja aplicado estilos ao HTML Element
     * @param obj
     */
    protected _setStyles = (obj : any | null) : void => {
        if(this._containerRef !== undefined && this._containerRef && obj!== null && obj && Object.keys(obj).length > 0) {
            for(let [key, value] of Object.entries(obj)) {
                this._renderer.setStyle(
                    this._containerRef.nativeElement, 
                    key, 
                    value
                );
            }
        }
    };

    /**
     * Método para aplicar uma ação desde o button 
     * @param action
     */
    protected _applyBtnAction = (action : string) : void => {
        if(action === "previous" || action === "next") {
            this._scrollToNode(action);
        } else if(action === "play" || action === "pause") {

        }
    };

    /**
     * Método para inicializar o slider timeout
     * @param flag
     */
    protected _initSliderTimeout = (flag : boolean) : void => {
        // Só quando informado o tempo de intervalo 
        if(this._options["intervalTime"] <= 0) {
            return ;
        }

        if(flag && !this._sliderTimeout) {
            this._isIntervalActive = true;

            // Timeout para o slider 
            this._sliderTimeout = setTimeout(
                () => this._scrollToNode("next"), 
                this._options["intervalTime"]
            );

            // Estabelecendo o momento que o time out começou 
            this._intervalStartedAt = Date.now();

            // Inicializando o interval 
            this._intervalProgress = setInterval(
                () => this._upgradeTimeProgress(), 
                25
            );

        } else if(!flag && this._sliderTimeout) {
            this._isIntervalActive = false;

            clearTimeout(
                this._sliderTimeout
            );
            
            this._sliderTimeout = null;

            if(this._intervalProgress) {
                clearInterval(
                    this._intervalProgress
                );

                this._intervalProgress = null;

                this._intervalStartedAt = null;
            }

            this.progress.next(
                0
            );
        }
    };

    /**
     * Método para gerenciar o período que falta para ativar a próxima interação
     */
    protected _upgradeTimeProgress = () : void => {
        const elapsedTime : number = Date.now() - this._intervalStartedAt;

        this.progress.next(
            (elapsedTime * 100) / this._options["intervalTime"]
        );
    };

    /**
     * Método para destroir a instância
     */
    public destroy = () : void => {
        if(this._sliderTimeout) {
            clearTimeout(
                this._sliderTimeout
            );

            this._sliderTimeout = null;
        }

        if(this._intervalProgress) {
            clearInterval(
                this._intervalProgress
            );

            this._intervalProgress = null;
        }

        this._subs.forEach((sub : Subscription) => {
            sub.unsubscribe();
        });

        this.started && !this.started.closed ? 
            this.started.complete() : 
            null;

        this.completed && !this.completed.closed ? 
            this.completed.complete() : 
            null;

        this.isPlaying && !this.isPlaying.closed ? 
            this.isPlaying.complete() : 
            null;

        this.progress && !this.progress.closed ? 
            this.progress.complete() : 
            null;

        this._playSlider && !this._playSlider.closed ?
            this._playSlider.complete() : 
            null;

        if(this._previousAnimation) {
            this._previousAnimation.destroy(); 
            this._previousAnimation = null;
        }

        if(this._animation) {
            this._animation.destroy(); 
            this._animation = null;
        }

        if(this._gestures) {
            this._gestures.destroy(); 
            this._gestures = null;
        }

        this._currentItem ?
            this._currentItem = null : 
            null;

        this._previousItem ? 
            this._previousItem = null : 
            null;

        if(this._arrowNavs) {
            this._arrowNavs.destroy(); 
            this._arrowNavs = null;
        }
    };

    // Métodos abstratos a serem implementados pelas instâncias concretas
    protected abstract _applyAnimation(currentPage : number, toPage : number, styleObj?: any) : void;
    protected abstract _initializeAnimation() : Observable<void>;
    protected abstract _resetAnimation() : Observable<void>;
    protected abstract _scrollToNode(action : "previous" | "next" | "current") : void;
    protected abstract _scrollToItem(index : number) : void;
};
