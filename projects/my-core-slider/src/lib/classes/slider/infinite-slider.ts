import { 
    ChangeDetectorRef,
    ElementRef, 
    QueryList, 
    Renderer2
} from "@angular/core";

import { 
    AnimationBuilder
} from "@angular/animations";

import { 
    ViewportRuler
} from "@angular/cdk/scrolling";

import { 
    Observable, 
    of,
    Subject
} from "rxjs";

// Parent class 
import { 
    BaseSlider
} from "./base-slider";

// Classes 
import { 
    Animation
} from "@my-core/utils";

// Directives 
import { 
    SliderButton
} from "../controllers/slider-button";
  
import { 
    SliderItem
} from "../controllers/slider-item"; 
  
import { 
    SliderSelector
} from "../controllers/slider-selector";

// Mdoels 
import { 
    SliderConfigs
} from "../../models";

export class InfiniteSlider extends BaseSlider {
    // Propriedade para atualizar o posicionamento do slide 
    protected _slidePos : Subject<number> = new Subject();
    
    constructor(
        options : SliderConfigs, 
        animationBuilder : AnimationBuilder,
        changeDetector : ChangeDetectorRef,
        containerRef : ElementRef,
        items : QueryList<SliderItem>,
        platformId : Object, 
        renderer : Renderer2, 
        ruler : ViewportRuler,
        btns : QueryList<SliderButton> = null,
        selectors: QueryList<SliderSelector> = null
    ) {
        super(
            options, 
            animationBuilder, 
            changeDetector, 
            containerRef, 
            items, 
            platformId,
            renderer, 
            ruler, 
            btns, 
            selectors
        );

        this._type = "infinite";

        // Subscription para quando acontecer mudança de posicionamento do primeiro ou último item
        setTimeout(() => {
            this._subs.push(
                this._slidePos.subscribe((posX : number) => {
                    if(this._animation !== undefined && this._animation) {
                        const styleObj = {
                            transform: `translate3d(${ -posX }px, 0px, 0px)`
                        };
            
                        setTimeout(() => {
                            this._animation.animate(
                                styleObj, 
                                0
                            );
                        }, 25);
                    }
                })
            );
        }, 100);
    };

    /**
     * Método para inicializar animation
     */
    protected _initializeAnimation() : Observable<void> {
        this._animation = new Animation(
            this._animationBuilder,
            this._platformId,
            this._containerRef
        );

        this._allowStartedCompletedWith = false;

        // Inicializando o posicionamento
        const styleObj = {
            transform: `translate3d(${ -this._currentItem.posX }px, 0px, 0px)`
        };

        this._animation.animate(
            styleObj,
            0
        );

        // Subscripton para quando a animação começar
        this._subs.push(
            this._animation.started.subscribe(() => {
                this._isInAnimation = true;

                if(this._allowStartedCompletedWith) {
                    this.started.next(
                        this._startedWith
                    );
                }

                this.isPlaying.next(
                    this._isInAnimation
                );
            })
        );

        // Subscription para quando a animação concluir
        this._subs.push(
            this._animation.completed.subscribe(() => {
                this._isInAnimation = false;

                if(this._allowStartedCompletedWith) {
                    // Atualizando o item atual
                    this._currentItem = this._getItemByIndex(
                        this._completedWith
                    );

                    // Atualizando o slider hammer index
                    if(this._gestures !== undefined && this._gestures) {
                        this._gestures.currentItem = this._currentItem;
                    }

                    // Atualizando os selectors
                    this._updateSelectors();

                    this.completed.next(
                        this._completedWith === undefined || this._completedWith === null ?
                            0 :
                            this._completedWith
                    );

                    if((this._startedWith === 0 && this._completedWith === this._totalItems - 1) || (this._startedWith === this._totalItems - 1 && this._completedWith === 0)) {
                        this._allowStartedCompletedWith = false;
                        this._slidePos.next(
                            this._currentItem.posX
                        );

                        this._playSlider.next(
                            false
                        );

                        return ;
                    }
                } else {
                    this._allowStartedCompletedWith = true;
                }

                this.isPlaying.next(
                    this._isInAnimation
                );

                this._playSlider.next(
                    true
                );
            })
        );

        return of(
            undefined
        );
    };

    /**
     * Método para resetear a animação quando existir resize
     */
    protected _resetAnimation() : Observable<void> {
        this._allowStartedCompletedWith = false;

        const styleObj = {
            transform: `translate3d(${ -this._currentItem.posX }px, 0px, 0px)`
        };

        if(this._animation) {
            this._animation.animate(
                styleObj,
                0
            );
        }

        return of(
            undefined
        );
    };

    /**
     * Método para aplicar a animação
     * @param currentPage 
     * @param toPage 
     * @param styleObj 
     */
    protected _applyAnimation(currentPage : number, toPage : number, styleObj?: any) : void {
        if(this._animation) {
            this._startedWith = currentPage;

            this._animation.animate(
                styleObj, 
                this._options["animationTime"]
            );

            this._completedWith = toPage;
        }
    };

    /**
     * Método para aplicar o scroll para um determinado nó
     * @param action 
     */
    protected _scrollToNode(action : "previous" | "next" | "current" = "current") : void {
        if(!this._isInAnimation) {
            // Parar o interval 
            this._playSlider.next(
                false
            );

            const item : SliderItem = action === "current" ? 
                this._currentItem : 
                this._currentItem.getItem(
                    action
                );

            if(item !== undefined && item && item.posX !== undefined && item.posX !== null) {
                let offsetX = item.posX;

                if(this._currentItem.index === 0 && item.index === this._totalItems - 1) {
                    offsetX = 0;
                } else if(this._currentItem.index === this._totalItems -1 && item.index === 0) {
                    offsetX = this._parentWidth * (this._totalItems + 1);
                }

                const styleObj = {
                    transform: `translate3d(${ -offsetX }px, 0px, 0px)`
                };

                this._applyAnimation(
                    this._currentItem.index, 
                    item.index, 
                    styleObj
                );
            }
        }
    };

    /**
     * Método para aplicar o scroll para um determinado index
     * @param index 
     */
    protected _scrollToItem(index : number) : void {
        if(!this._isInAnimation) {
            // Parar o interval 
            this._playSlider.next(
                false
            );

            const item : SliderItem = this._getItemByIndex(
                index
            );
            
            if(item !== undefined && item && item.posX !== undefined && item.posX !== null) {
                let offsetX = item.posX;

                if(this._currentItem.index === 0 && item.index === this._totalItems - 1) {
                    offsetX = 0;
                } else if(this._currentItem.index === this._totalItems -1 && item.index === 0) {
                    offsetX = this._parentWidth * (this._totalItems + 1);
                }

                const styleObj = {
                    transform: `translate3d(${ -offsetX }px, 0px, 0px)`
                };

                this._applyAnimation(
                    this._currentItem.index, 
                    item.index, 
                    styleObj
                );                
            }
        }
    };
};
