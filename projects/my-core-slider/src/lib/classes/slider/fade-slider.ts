import { 
    ChangeDetectorRef,
    ElementRef, 
    QueryList, 
    Renderer2
} from "@angular/core";

import { 
    AnimationBuilder
} from "@angular/animations";

import { 
    ViewportRuler
} from "@angular/cdk/scrolling";
  
import { 
    Observable,
    of
} from "rxjs";

// Parent class 
import { 
    BaseSlider
} from "./base-slider";

// Classes 
import { 
    Animation
} from "@my-core/utils";

// Directives 
import { 
    SliderButton
} from "../controllers/slider-button";
  
import { 
    SliderItem
} from "../controllers/slider-item"; 
  
import { 
    SliderSelector
} from "../controllers/slider-selector";

// Models 
import { 
    SliderConfigs
} from "../../models";

/**
 * Classe para implementar o fade slider
 */
export class FadeSlider extends BaseSlider {
    constructor(
        options: SliderConfigs, 
        animationBuilder : AnimationBuilder,
        changeDetector : ChangeDetectorRef,
        containerRef : ElementRef,
        items : QueryList<SliderItem>,
        platformId : Object, 
        renderer : Renderer2, 
        ruler : ViewportRuler,
        btns : QueryList<SliderButton> = null,
        selectors: QueryList<SliderSelector> = null
    ) {
        super(
            options, 
            animationBuilder, 
            changeDetector, 
            containerRef, 
            items, 
            platformId, 
            renderer,
            ruler, 
            btns, 
            selectors
        );

        this._type = "fade";
    };

    /**
     * Método para inicializar animation
     */
    protected _initializeAnimation() : Observable<void> {
        this._previousAnimation = new Animation(
            this._animationBuilder,
            this._platformId,
            null
        );

        this._animation = new Animation(
            this._animationBuilder,
            this._platformId,
            null
        );

        // Subscription para quando a animação do current item começar
        this._subs.push(
            this._animation.started.subscribe(() => {
                this._isInAnimation = true;

                this.isPlaying.next(
                    this._isInAnimation
                );

                this.started.next(
                    this._previousItem.index
                );
            })
        );

        // Subscription para quando a animação concluir
        this._subs.push(
            this._animation.completed.subscribe(() => {
                this._isInAnimation = false;

                // Aplicando styles adicionais ao previous item
                this._previousItem.setStyles({
                    display: "none"
                });

                // Atualizando os selectors
                this._updateSelectors();

                // Atualizando o slider hammer index
                if(this._gestures !== undefined && this._gestures) {
                    this._gestures.currentItem = this._currentItem;
                }

                this.completed.next(
                    this._currentItem.index
                );

                this._playSlider.next(
                    true
                );
            })
        );

        return of(
            undefined
        );
    };

    /**
     * Método para resetear a animação quando existir resize
     */
    protected _resetAnimation() : Observable<void> { 
        return of(
            undefined
        );
    };

    /**
     * Método para aplicar a animação
     * @param currentPage 
     * @param toPage 
     * @param styleObj 
     */
    protected _applyAnimation(currentPage : number, toPage : number, styleObj?: any) : void {
        if(this._animation && this._previousAnimation) {
            this._previousItem = this._getItemByIndex(
                currentPage
            );

            this._currentItem = this._getItemByIndex(
                toPage
            );

            // Setear os targets dos animations 
            this._previousAnimation.setTarget(
                this._previousItem.elm
            );

            this._animation.setTarget(
                this._currentItem.elm
            );

            const stylesForPrevious = {
                opacity: 0
            };

            const stylesForCurrent = {
                opacity: 1
            };

            this._previousAnimation.animate(
                stylesForPrevious, 
                this._options["animationTime"]
            );

            this._animation.animate(
                stylesForCurrent, 
                this._options["animationTime"]
            );
        }
    };

    /**
     * Método para aplicar o scroll para um determinado nó
     * @param action 
     */
    protected _scrollToNode(action : "previous" | "next") : void {
        if(!this._isInAnimation) {
            // Parar o interval 
            this._playSlider.next(
                false
            );

            const item : SliderItem = this._currentItem.getItem(
                action
            );

            if(item === undefined || !item) {
                return ;
            }

            this._currentItem.setStyles({
                "z-index": 0
            });

            item.setStyles({
                display: "block", 
                "z-index": 1
            });

            this._applyAnimation(
                this._currentItem.index, 
                item.index
            );
        }
    };

    /**
     * Método para aplicar o scroll para um determinado index
     * @param index 
     */
    protected _scrollToItem(index : number) : void {
        if(!this._isInAnimation && this._currentItem.index !== index) {
            this._playSlider.next(
                false
            );

            const item : SliderItem = this._getItemByIndex(
                index
            );

            if(item === undefined || !item) {
                return ;
            }

            this._currentItem.setStyles({
                "z-index": 0
            });

            item.setStyles({
                display: "block", 
                "z-index": 1
            });

            this._applyAnimation(
                this._currentItem.index, 
                item.index
            );
        }
    };
};
