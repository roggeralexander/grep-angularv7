/*
 * Public API Surface of my-core-img
 */

// Sub módulos 
export * from "./lib/capture/index"; 
export * from "./lib/viewer/index";

// Módulo
export * from './lib/my-core-img.module';
