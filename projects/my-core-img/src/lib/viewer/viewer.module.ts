import { 
    NgModule
} from "@angular/core";     

import { 
    CommonModule
} from "@angular/common"; 

// Components
import { 
    ImgComponent 
} from './components/img/img.component'; 

// Directives 
import { 
    LazyImgDirective
} from "./directives/lazy-img.directive";

@NgModule({
    imports: [
        CommonModule
    ], 
    declarations: [
        ImgComponent, 
        LazyImgDirective
    ],
    exports: [
        ImgComponent, 
        LazyImgDirective
    ], 
    entryComponents: [
        ImgComponent
    ]
})
export class ViewerModule { };