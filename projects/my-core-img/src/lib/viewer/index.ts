// Components 
export {
    ImgComponent as ImgViewerComponent
} from "./components/img/img.component";

// Directives 
export * from "./directives/lazy-img.directive"; 

// Modules 
export {
    ViewerModule as ImgViewerModule
} from "./viewer.module";