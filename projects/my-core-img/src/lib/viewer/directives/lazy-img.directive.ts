import {
    Directive,
    EventEmitter,
    Inject,  
    Input, 
    OnDestroy,
    OnInit, 
    Output,  
    PLATFORM_ID
} from "@angular/core"; 

import { 
    isPlatformServer
} from "@angular/common"; 

import { 
    Observable, 
    of, 
    Subscription, 
    timer, 
    throwError
} from "rxjs"; 

import { 
    catchError, 
    switchMap
} from "rxjs/operators"; 

// Image Loader 
import { 
    ImgLoader
} from "@my-core/utils";

@Directive({
    selector: "[lazyImg]", 
    exportAs: "lazyImg"
})
export class LazyImgDirective implements OnInit, OnDestroy {
    @Input() 
    get src() : string {
        return this._src;
    };
    set src(src : string) {
        if(isPlatformServer(this._platformId) || src === undefined || !src || !src.trim().length || this._src === src) {
            return ;
        }

        this._src = src; 
    };
    protected _src : string = null;

    @Input() 
    get alt() : string {
      return this._alt; 
    };
    set alt(alt : string) {
      if(alt === undefined || !alt || !alt.trim().length || this._alt === alt) {
        return ;
      }
  
      this._alt = alt;
    };
    protected _alt : string = null; 
  
    // Propriedade para referenciar à instância do img loader 
    protected _imgLoader : ImgLoader = null;

    // Subjects & Subscriptions 
    protected _subs : Subscription[] = [];

    // Output para quando a imagem estiver sendo carregada
    @Output()
    public loading : EventEmitter<boolean> = new EventEmitter();

    // Output para quando a imagem estiver carregada
    @Output()
    public loaded : EventEmitter<string> = new EventEmitter();

    // Output para emissão do html img gerado após leitura da imagem
    @Output() 
    public img : EventEmitter<HTMLImageElement> = new EventEmitter();

    constructor(
        @Inject(PLATFORM_ID) protected _platformId : Object
    ) { };

    ngOnInit() {
        if(isPlatformServer(this._platformId)) {
            return ; 
        }

        // Subcription para inicializar o component 
        this._subs.push(
            timer(100).pipe(
                switchMap(() => this._initialize()), 
                catchError((err : Error) => {
                    console.log("Lazy loading image error", err); 
                    return of(undefined); 
                })
            ).subscribe(() => {})
        );
    };

    ngOnDestroy() {
        this._subs.forEach((sub : Subscription) => {
            sub.unsubscribe();
        });

        this._imgLoader ? 
            this._imgLoader.destroy() : 
            null; 
    };

    /**
     * Método para inicializar a directive
     */
    protected _initialize = () : Observable<void> => {
        if(this._src === undefined || !this._src || !this._src.trim().length) {
            return throwError(
                new Error(
                    "Image path não informado"
                )
            );
        }

        this._imgLoader = new ImgLoader(
            this._src, 
            this._alt
        );

        this._imgLoader = new ImgLoader(
            this._src
        );

        // Subscription para o loading 
        this._subs.push(
            this._imgLoader.loading.subscribe((flag : boolean) => {
                if(flag === null) {
                    return ; 
                }

                this.loading.next(
                    flag
                );
            })
        );

        // Subscription para saber se foi realizada a leitura 
        this._subs.push(
            this._imgLoader.loaded.subscribe(() => {
                this.loaded.next();

                this.img.next(
                    this._imgLoader.img
                );
            })
        );

        return of(
            undefined
        );        
    };
};