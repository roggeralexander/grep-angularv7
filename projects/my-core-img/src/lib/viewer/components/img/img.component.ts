import { 
  AfterViewInit, 
  ChangeDetectionStrategy, 
  ChangeDetectorRef,
  Component, 
  ElementRef, 
  Inject, 
  Input, 
  OnDestroy, 
  OnInit, 
  PLATFORM_ID, 
  Renderer2, 
  ViewChild 
} from '@angular/core';

import { 
  isPlatformServer
} from "@angular/common"; 

import { 
  Observable, 
  of, 
  Subject,
  Subscription, 
  throwError,
  timer
} from "rxjs"; 

import { 
  catchError,
  switchMap, 
  tap
} from "rxjs/operators"; 

// Loader class
import { 
  ImgLoader
} from "@my-core/utils"; 

// Third party library 
import uuid from "uuid";

@Component({
  selector: 'mc-img-viewer',
  templateUrl: './img.component.html',
  styleUrls: ['./img.component.scss'], 
  changeDetection: ChangeDetectionStrategy.Default
})
export class ImgComponent implements OnInit, AfterViewInit, OnDestroy {
  @Input() 
  get src() : string {
    return this._src; 
  };
  set src(src : string) {
    if(isPlatformServer(this._platformId) || src === undefined || !src || !src.trim().length || this._src === src) {
      return ;
    }

    this._src = src; 

    setTimeout(() => {
      this._worker.next();
    });
  };
  protected _src : string = null; 

  @Input() 
  get alt() : string {
    return this._alt; 
  };
  set alt(alt : string) {
    if(alt === undefined || !alt || !alt.trim().length || this._alt === alt) {
      return ;
    }

    this._alt = alt;
  };
  protected _alt : string = null; 

  @Input() 
  get renderImg() : boolean {
    return this._renderImg;
  };
  set renderImg(flag : boolean) {
    if(flag === undefined || flag === null || this._renderImg === flag) {
      return ; 
    }

    this._renderImg = flag; 
  };
  protected _renderImg : boolean = false; 

  // Propriedade para referenciar o id do component 
  protected _id : string = "mc-img-" + uuid.v4().replace(
    new RegExp("-", "gi"), 
    ""
  );

  // Propriedades para monitorar o load da imagem 
  public loading : boolean = false; 
  public loaded : boolean = false; 

  // View Child onde a imagem será postada ou mostrada como style
  @ViewChild(
    "containerImg", 
    { read: ElementRef }
  ) protected _containerElmRef : ElementRef = null;

  // Propriedade para referenciar à instância do loader 
  protected _imgLoader : ImgLoader = null; 

  // Subjects & Subscriptions 
  protected _worker : Subject<void> = new Subject();
  protected _subs : Subscription[] = [];

  constructor(
    protected _changeDetector : ChangeDetectorRef, 
    protected _elmRef : ElementRef, 
    @Inject(PLATFORM_ID) protected _platformId : Object, 
    protected _renderer : Renderer2
  ) { 
    if(isPlatformServer(this._platformId)) {
      return ;
    }

    this._worker.pipe(
      tap(() => console.log("Observable timer concluído")), 
      switchMap(() => this._initialize()), 
      catchError((err : Error) => {
        console.log("Error na leitura da imagem", err);
        return of(undefined);
      })
    ).subscribe(() => {
      console.log("Img component atualizado");
    });
  };

  ngOnInit() {
    // Subscription para inicializar o component 
    if(isPlatformServer(this._platformId)) {
       return ; 
    }
  };

  ngAfterViewInit() {
    if(isPlatformServer(this._platformId)) {
      return ; 
    }

    // Atribuindo o id ao element ref
    this._renderer.setAttribute(
      this._elmRef.nativeElement, 
      "id", 
      this._id
    );
  };

  ngOnDestroy() {
    this._subs.forEach((sub : Subscription) => {
      sub.unsubscribe();
    });

    this._worker.complete();

    if(this._imgLoader) {
      this._imgLoader.destroy(); 
      this._imgLoader = null;
    }
  };

  /**
   * Método para inicializar o component 
   */
  protected _initialize = () : Observable<void> => {
    if(this._src === undefined || !this._src || !this._src.trim().length) {
      return throwError(
        new Error(
          "Image path não informado"
        )
      );
    }

    this._subs.forEach((sub : Subscription) => sub.unsubscribe());

    if(this._imgLoader) {
      this._imgLoader.destroy(); 
      this._imgLoader = null; 
    }

    this._imgLoader = new ImgLoader(
      this._src, 
      this._alt
    );

    // Subscription para o loading 
    this._subs.push(
      this._imgLoader.loading.subscribe((flag : boolean) => {
        if(flag === null) {
          return ; 
        }

        this.loading = flag; 
        this._changeDetector.markForCheck();
      })
    );

    // Subscription para saber se a imagem foi carregada 
    this._subs.push(
      this._imgLoader.loaded.pipe(
        switchMap(() => {
          this.loaded = true; 
          this._changeDetector.markForCheck(); 
          return timer(25); 
        }), 
        switchMap(() => this._addImgToContainer())
      ).subscribe(() => { 
        this._changeDetector.markForCheck();
      })
    );

    return of(undefined); 
  };

  /**
   * Método para solicitar que a imagem seja vinculada ao container da imagem
   */
  protected _addImgToContainer = () : Observable<void> => {
    const containerHtml : HTMLDivElement = <HTMLDivElement>this._containerElmRef.nativeElement;

    if(this._renderImg) {
      // Caso o container possuir filhos, removê-los
      while(containerHtml.firstChild) {
        containerHtml.removeChild(
          containerHtml.firstChild
        );
      }

      this._renderer.appendChild(
        containerHtml, 
        this._imgLoader.img
      );
    } else {
      this._renderer.setStyle(
        this._elmRef.nativeElement, 
        "height", 
        "100%"
      );

      this._renderer.addClass(
        containerHtml, 
        "full-bg-img"
      );
      
      this._renderer.setStyle(
        containerHtml, 
        "background-image", 
        `url(${ this._src })`
      );
    }

    return of(undefined);
  };
};
