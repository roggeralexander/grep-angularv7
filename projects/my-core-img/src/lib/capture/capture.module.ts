import { 
    NgModule
} from "@angular/core"; 

import { 
    CommonModule
} from "@angular/common";

// Angular CDK Module 
import { 
    ScrollDispatchModule
} from "@angular/cdk/scrolling"; 

// Components
import { 
    ImgComponent 
} from './components/img/img.component';

import { 
    ToolbarComponent 
} from './components/toolbar/toolbar.component'; 

@NgModule({
    imports: [
        CommonModule, 
        ScrollDispatchModule
    ], 
    declarations: [
        ImgComponent,
        ToolbarComponent
    ], 
    exports: [
        ImgComponent
    ], 
    entryComponents: [
        ImgComponent, 
        ToolbarComponent
    ]
})  
export class CaptureModule { };