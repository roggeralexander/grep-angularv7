// Components 
export {
    ImgComponent as ImgCaptureComponent
} from "./components/img/img.component";

// Módulo
export {
    CaptureModule as ImgCaptureModule
} from "./capture.module"; 