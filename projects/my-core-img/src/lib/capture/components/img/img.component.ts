import { 
  ChangeDetectionStrategy, 
  ChangeDetectorRef, 
  Component,
  ElementRef, 
  EventEmitter, 
  Inject,  
  OnDestroy, 
  OnInit, 
  Output, 
  PLATFORM_ID, 
  Renderer2, 
  ViewChild
} from '@angular/core';

import { 
  isPlatformServer
} from "@angular/common"; 

import {
  fromEvent, 
  Observable,  
  of, 
  Subscription, 
  throwError, 
  timer
} from "rxjs"; 

import { 
  catchError, 
  switchMap
} from "rxjs/operators"; 

// Angular CDK Modules 
import { 
  ViewportRuler
} from "@angular/cdk/scrolling"; 

// Third party module 
import uuid from "uuid"; 

// Constants
import { 
  DEFAULT_CONFIGS 
} from '../../constants';

// Models 
import { 
  CaptureOutput
} from "@my-core/utils"; 

// Toolbar component 
import { 
  ToolbarComponent
} from "../toolbar/toolbar.component"; 

// Utilities 
import { 
  blobToFile,
  canvasToBlob, 
  getUMStream
} from "@my-core/utils";

@Component({
  selector: 'mc-img-capture',
  templateUrl: './img.component.html',
  styleUrls: ['./img.component.scss'], 
  changeDetection: ChangeDetectionStrategy.Default
})
export class ImgComponent implements OnInit, OnDestroy {
  // Instância do arquivo gerado quando tirada uma imagem 
  public file : File = null; 

  // Propriedade para conferir se o capture está permitido ou não 
  public allowed : boolean = false;

  // Id do component 
  protected _id : string = "mc-img-capture-" + uuid.v1().replace(
    new RegExp("-", "gi"), 
    ""
  );

  // Video, Canvas & Context 
  protected _videoHtml : HTMLVideoElement = null; 
  protected _canvas : HTMLCanvasElement = null; 
  protected _context : CanvasRenderingContext2D = null; 

  // Subscriptions 
  protected _subs : Subscription[] = [];

  // View child - Video Input
  @ViewChild(
    "video", 
    { read : ElementRef }
  ) protected _videoRef : ElementRef = null; 

  // View child - Toolbar component 
  @ViewChild(
    ToolbarComponent
  ) protected _toolbar : ToolbarComponent = null; 

  // Outputs 
  @Output() 
  public ready : EventEmitter<void> = new EventEmitter(); 

  @Output() 
  public output : EventEmitter<CaptureOutput> = new EventEmitter(); 

  constructor(
    protected _changeDetector : ChangeDetectorRef, 
    protected _elmRef : ElementRef, 
    @Inject(PLATFORM_ID) protected _platformId : Object, 
    protected _renderer : Renderer2, 
    protected _ruler : ViewportRuler
  ) { };

  ngOnInit() {
    if(isPlatformServer(this._platformId)) {
      return ; 
    }

    // Subscription para o ruler 
    this._subs.push(
      this._ruler.change().subscribe(() => {
        this._resetCanvasSize();
      })
    );

    // Subscription para inicializar o component 
    this._subs.push(
      timer(100).pipe(
        switchMap(() => this._initialize()), 
        catchError((err : Error) => {
          console.log("Image web capture error", err); 
          return of(undefined);
        })
      ).subscribe(() => {
        console.log("Image capture inicializado");
      })
    );
  };

  ngOnDestroy() {
    this._subs.forEach((sub : Subscription) => {
      sub.unsubscribe();
    });

    this.ready.complete(); 
    this.output.complete(); 

    this.ready = null; 
    this.output = null; 
  };

  /**
   * Método para solicitar a inicialização do component
   */
  protected _initialize = () : Observable<void> => getUMStream(
    this._platformId, 
    {
      audio: false, 
      video: true
    }
  ).pipe(
    switchMap((stream : MediaStream) => {
      if(stream === null || !stream) {
        return throwError(
          new Error(
            "Web cam is not allowed to be used"
          )
        );
      }

      this._renderer.setAttribute(
        this._elmRef.nativeElement, 
        "id", 
        this._id
      );

      this.allowed = true;
              
      this._changeDetector.detectChanges(); 

      return of(stream);
    }), 
    switchMap((stream : MediaStream) => this._setElements(stream)), 
    switchMap(() => this._declareToolbarListeners())
  );

  /**
   * Método para setear as propriedades do vídeo input
   * @param stream
   */
  protected _setElements = (stream : MediaStream) : Observable<void> => {
    if(this._videoRef === undefined || !this._videoRef) {
      return throwError(
        new Error(
          "Video input reference para img capture component não existe"
        )
      );
    }

    // Inicializando video input e o canvas 
    this._videoHtml = <HTMLVideoElement>this._videoRef.nativeElement;
    this._canvas = <HTMLCanvasElement>document.createElement("canvas");
    this._context = this._canvas.getContext("2d");

    // Subscription para o vídeo - quando conseguir gravar 
    this._subs.push(
      fromEvent(
        this._videoHtml, 
        "canplay"
      ).subscribe(() => {
        // Reseteando o tamanho do canvas
        this._resetCanvasSize();
      })
    );

    // Vinculando o streaming do video 
    this._videoHtml.srcObject = stream; 

    return timer(25).pipe(
      switchMap(() => {
        this._toolbar.allowBtnCancel.next(false); 
        this._toolbar.allowBtnEdit.next(false); 
        this._toolbar.allowBtnSave.next(false); 
        this._toolbar.allowBtnTake.next(true);

        this._videoHtml.play();
        this._changeDetector.markForCheck();

        this.ready.emit();

        return of(
          undefined
        );
      })
    );
  };

  /**
   * Método para registrar os listeners dos botões do toolbar
   */
  protected _declareToolbarListeners = () : Observable<void> => {
    // Subscription para solicitar que uma foto seja tomada 
    this._subs.push(
      this._toolbar.take.pipe(
        switchMap(() => this._onTake())
      ).subscribe(() => {
        console.log("Solicitando que a foto seja tirada");
      })
    );

    // Subscription para solicitar que uma foto seja cancelada 
    this._subs.push(
      this._toolbar.cancel.pipe(
        switchMap(() => this._onCancel())
      ).subscribe(() => {
        console.log("Solicitando o cancelamento da foto"); 
      })
    );

    // Subscription para solicitar salvar a foto 
    this._subs.push(
      this._toolbar.save.pipe(
        switchMap(() => this._onSave())
      ).subscribe(() => {
        console.log("Solicitando salvar a foto")
      })
    );

    // Subscription para edição da foto 
    this._subs.push(
      this._toolbar.edit.pipe(
        switchMap(() => this._onEdit())
      ).subscribe(() => {
        console.log("Solicitando editar a foto");
      })
    );

    return timer(25).pipe(
      switchMap(() => of(
        undefined
      ))
    );
  };

  /**
   * Método para resetear o tamanho do canvas, quando ele existir
   */
  protected _resetCanvasSize = () : void => {
    if(this._canvas === undefined || !this._canvas) {
      return ; 
    }

    const videoProperties = this._videoHtml.getBoundingClientRect();

    this._canvas.width = videoProperties.width; 
    this._canvas.height = videoProperties.height;
  };

  /**
   * Método para limpar o canvas
   */
  protected _clearCanvas = () : Observable<void> => new Observable(observer => {
    this.file = null; 

    this._context.clearRect(
      0, 
      0, 
      this._canvas.width, 
      this._canvas.height
    );

    setTimeout(() => {
      this._toolbar.allowBtnCancel.next(false); 
      this._toolbar.allowBtnEdit.next(false); 
      this._toolbar.allowBtnSave.next(false); 
      this._toolbar.allowBtnTake.next(true);

      observer.next(); 
      observer.complete();
    });
  });

  /**
   * Método para tirar uma fotografia
   */
  protected _onTake = () : Observable<void> => new Observable(observer => {
    this._clearCanvas().pipe(
      switchMap(() => {
        this._context.drawImage(
          this._videoHtml, 
          0, 
          0, 
          this._canvas.width, 
          this._canvas.height
        );
  
        this._videoHtml.pause(); 
  
        return canvasToBlob(
          this._canvas, 
          DEFAULT_CONFIGS["mimeType"], 
          DEFAULT_CONFIGS["quality"]
        );
      }), 
      switchMap((blob : Blob) => {
        const fileName : string = uuid.v4().replace(
          new RegExp("-", "gi"), 
          ""
        );
  
        return blobToFile(
          blob, 
          fileName, 
          DEFAULT_CONFIGS["extension"]
        );
      })
    ).subscribe(
      (file : File) => {
        console.log("Arquivo gerado do canvas", file);
  
        this.file = file;
  
        this._toolbar.allowBtnCancel.next(true); 
        this._toolbar.allowBtnEdit.next(true); 
        this._toolbar.allowBtnSave.next(true); 
        this._toolbar.allowBtnTake.next(false);
  
        this._changeDetector.detectChanges(); 
 
        setTimeout(() => {
          observer.next(); 
          observer.complete(); 
        });
      }
    );    
  });

  /**
   * Método para cancelar a operação
   */
  protected _onCancel = () : Observable<void> => new Observable(observer => {
    this._clearCanvas().pipe(
      switchMap(() => {
        this._videoHtml.play(); 
        return of(undefined); 
      })
    ).subscribe(() => {
      observer.next(); 
      observer.complete();
    });
  });

  /**
   * Método para salvar a imagem gerada após tirar a foto
   */
  protected _onSave = () : Observable<void> => new Observable(observer => {
    const obj : CaptureOutput = {
      file: this.file, 
      type: "image"
    };

    console.log("Capture output", obj);

    this.output.next(
      obj
    );

    setTimeout(() => {
      observer.next(); 
      observer.complete();
    });
  });

  /**
   * Método para solicitar a edição da imagem 
   */
  protected _onEdit = () : Observable<void> => of(undefined);
};
