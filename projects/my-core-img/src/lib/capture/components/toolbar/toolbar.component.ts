import { 
  ChangeDetectionStrategy, 
  ChangeDetectorRef, 
  Component,
  Inject, 
  OnDestroy, 
  OnInit, 
  PLATFORM_ID
} from '@angular/core';

import { 
  isPlatformServer
} from "@angular/common"; 

import { 
  Subscription, 
  Subject
} from "rxjs"; 

@Component({
  selector: 'mc-img-toolbar-capture',
  templateUrl: './toolbar.component.html',
  styleUrls: ['./toolbar.component.scss'], 
  changeDetection: ChangeDetectionStrategy.Default
})
export class ToolbarComponent implements OnInit, OnDestroy {
  // Subject para cancelar uma operação 
  public readonly cancel : Subject<void> = new Subject();

  // Subject para solicitar que seja tirada a fotografia 
  public readonly take : Subject<void> = new Subject(); 

  // Subject para solicitar que seja realizada a edição do vídeo
  public readonly edit : Subject<void> = new Subject();

  // Subject para solicitar que o arquivo seja salvo 
  public readonly save : Subject<void> = new Subject();

  // Subject para habilitar o botão take picture 
  public readonly allowBtnTake : Subject<boolean> = new Subject();

  // Subject para habilitar o botão edit 
  public readonly allowBtnEdit : Subject<boolean> = new Subject(); 

  // Subject para habilitar o botão cancel 
  public readonly allowBtnCancel : Subject<boolean> = new Subject(); 

  // Subject para habilitar o botão save 
  public readonly allowBtnSave : Subject<boolean> = new Subject(); 

  // Propriedade para habilitar o botão cancelar 
  public btnCancelAllowed : boolean = false; 

  // Propriedade para habilitar o botão save 
  public btnSaveAllowed : boolean = false; 

  // Propriedade para habilitar o botão edit 
  public btnEditAllowed : boolean = false; 

  // Propriedade para habilitar o botão take 
  public btnTakeAllowed : boolean = false; 

  // Subscriptions 
  protected _subs : Subscription[] = [];

  constructor(
    protected _changeDetector : ChangeDetectorRef, 
    @Inject(PLATFORM_ID) protected _platformId : Object
  ) {};

  ngOnInit() {
    if(isPlatformServer(this._platformId)) {
      return ; 
    }

    // Subscription para permitir o botão save 
    this._subs.push(
      this.allowBtnSave.subscribe((flag : boolean) => {
        console.log("Habilitar o botão save", flag);

        this.btnSaveAllowed = flag; 
        this._changeDetector.markForCheck();
      })
    );

    // Subscription para permitir o botão edit 
    this._subs.push(
      this.allowBtnEdit.subscribe((flag : boolean) => {
        console.log("Habilitar o botão edit", flag);

        this.btnEditAllowed = flag; 
        this._changeDetector.markForCheck();
      })
    );

    // Subscription para permitir o botão take 
    this._subs.push(
      this.allowBtnTake.subscribe((flag : boolean) => {
        console.log("Habilitar o botão take", flag);

        this.btnTakeAllowed = flag; 
        this._changeDetector.markForCheck();
      })
    );

    // Subscription para permitir o botão cancel 
    this._subs.push(
      this.allowBtnCancel.subscribe((flag : boolean) => {
        console.log("Habilitar o botão cancel", flag);

        this.btnCancelAllowed = flag; 
        this._changeDetector.markForCheck();
      })
    );
  };

  ngOnDestroy() {
    this._subs.forEach((sub : Subscription) => {
      sub.unsubscribe();
    });

    this.cancel.complete(); 
    this.edit.complete(); 
    this.save.complete(); 
    this.take.complete(); 
    this.allowBtnCancel.complete(); 
    this.allowBtnEdit.complete(); 
    this.allowBtnSave.complete(); 
    this.allowBtnTake.complete();
  };

  /**
   * Método para aplicar uma operação 
   * @param op
   */
  public applyOperation = (op : string) : void => {
    if(isPlatformServer(this._platformId) || op === undefined || op === null || this[op] === undefined || !this[op]) {
      return ; 
    }

    setTimeout(() => {
      this[op].next();
    });
  };
};
