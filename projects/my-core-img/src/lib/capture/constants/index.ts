import { 
    CameraConfig
} from "@my-core/utils";

/**
 * Constante com as configurações definidas por default
 */
export const DEFAULT_CONFIGS : CameraConfig = {
    mimeType: "image/jpeg", 
    quality: 0.95, 
    extension: "jpeg"
};