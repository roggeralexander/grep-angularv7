import { 
  NgModule 
} from '@angular/core';

// Sub módulos 
import {
  CaptureModule
} from "./capture/capture.module";    

import { 
  ViewerModule
} from "./viewer/viewer.module"; 

@NgModule({
  exports: [
    CaptureModule, 
    ViewerModule
  ] 
})
export class MyCoreImgModule { }
