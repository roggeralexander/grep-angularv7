/*
 * Public API Surface of my-core-utils
 */
// Ngx pipes 
export * from "ngx-pipes";

// Utilitários do módulo 
export * from "./lib/actions/index"; 
export * from "./lib/array/index"; 
export * from "./lib/color/index";
export * from "./lib/date-time/index"; 
export * from "./lib/file/index";
export * from "./lib/forms/index";
export * from "./lib/http/index"; 
export * from "./lib/loaders/index";
export * from "./lib/pagination/index"; 
export * from "./lib/patterns/index";
export * from "./lib/services/index";
export * from "./lib/string/index"; 
export * from "./lib/web-cam/index";

// Módulo
export * from './lib/my-core-utils.module';
