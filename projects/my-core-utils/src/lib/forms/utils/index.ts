import { 
    AbstractControl,
    FormArray, 
    FormControl, 
    FormGroup
} from "@angular/forms";

/**
 * Método para codificar o json em x-www-form-urlencoded
 * @param srcjson 
 */
export const xwwwfurlenc = (srcjson : any) : any => {
    if(typeof srcjson !== "object") if(typeof console !== "undefined"){ console.log("\"srcjson\" is not a JSON object"); return null; }
        let u = encodeURIComponent;
        let urljson = "";
        let keys = Object.keys(srcjson);
        for(let i=0; i <keys.length; i++){
            urljson += u(keys[i]) + "=" + u(srcjson[keys[i]]);
            if(i < (keys.length-1))urljson+="&";
        }
	return urljson;
};

/**
 * Método para retornar os dados de x-www-form-urlencoded em JSON
 * @param urljson 
 */
export const dexwwwfurlenc = (urljson : any) => {
	let dstjson = {};
	let ret;
	let reg = /(?:^|&)(\w+)=(\w+)/g;
	while((ret = reg.exec(urljson)) !== null){
		dstjson[ret[1]] = ret[2];
	}
	return dstjson;
};

/**
 * Method to enable or disable a form control
 * @param formCtrl 
 * @param flag 
 */
export const enableFormCtrl = (formCtrl : FormControl, flag : boolean) : void => {
    if(formCtrl === undefined || !formCtrl) {
      return ;
    }

    flag ? 
      formCtrl.enable({
        onlySelf: true
      }) : 
      formCtrl.disable({
        onlySelf: true
      });
};

/**
 * Método para adicionar data a um determinado form array
 * @param formGroup 
 * @param fieldName 
 * @param data
 */
export const addDataToFormArray = (formGroup : FormGroup, fieldName : string, data : any[]) : void => {
    if(formGroup === undefined || !formGroup) {
        return ; 
    }

    const ctrlArray : FormArray = <FormArray>formGroup.get(
        fieldName
    );

    if(ctrlArray === undefined || !ctrlArray) {
        return ;
    }

    while(ctrlArray.length !== 0) {
        ctrlArray.removeAt(0);
    }

    if(data === undefined || !data) {
        return;
    }

    data.forEach(item => {
        ctrlArray.push(
            new FormControl(
                item, 
            )
        );
    });
};

/**
 * Método para obter o nome de um controle
 * @param control 
 */
export const getCtrlName = (control : AbstractControl) : string | null => {
    if(control === undefined || !control) {
        return null;
    }
    
    let parent = control.parent;

    if(parent === undefined || !parent) {
        return null;
    }

    let ctrlName : string = null;

    Object.keys(parent.controls).forEach((key : string) => {
        let childCtrl = parent.get(
            key
        );

        if(childCtrl !== control) {
            return ;
        }

        ctrlName = key;
    })

    return ctrlName;
};

/**
 * Método para retornar um controle de um formulário
 * @param form 
 * @param name 
 */
export const getCtrl = (form : FormGroup, name : string) : AbstractControl => {
    if(form === undefined || !form || name === undefined || !name) {
        return null;
    }

    let ctrl : AbstractControl = form.get(name);

    if(ctrl === undefined || !ctrl) {
        return null;
    }

    return ctrl;
};

/**
 * Método para obter o total de elementos vazíos ou nulls
 * @param form 
 */
export const getTotalElmsEmpty = (form : FormGroup) : number => {

    if(form === undefined || !form) {
        return 0;
    }

    const obj = Object.assign(
        {}, 
        form.value
    );

    return Object.values(obj).filter(item => item === null || item === "" || item === []).length;
};