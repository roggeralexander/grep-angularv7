/**
 * Constante para indicar o erro quando dois form controls têm conteúdo igual
 */
export const mustMatchError : any = {
    "pt-br": "$ctrlName e $matchingCtrlName não coincidem", 
    "en": "$ctrlName and $matchingCtrlName aren't equals", 
    "es": "$ctrlName y $matchingCtrlName no coincidem"
};

/**
 * Constante para indicar o erro quando dois form controls não têm conteúdo igual
 */
export const notMustMatchError : any = {
    "pt-br": "$ctrlName e $matchingCtrlName coincidem", 
    "en": "$ctrlName and $matchingCtrlName are equals", 
    "es": "$ctrlName y $matchingCtrlName coincidem"
};
