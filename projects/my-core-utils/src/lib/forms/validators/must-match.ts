import { 
    FormGroup, 
    ValidatorFn, 
    ValidationErrors
} from "@angular/forms";

// Constants 
import { 
    mustMatchError, 
    notMustMatchError
} from "../constants";

// Utils 
import { 
    replaceWithParamsSync
} from "../../string/utils";

/**
 * Validator para dois controls cujos valores devem coincidir
 * @param ctrlName 
 * @param matchingCtrlName 
 * @param lang 
 * @param flag (para indicar se deve ser mustMatch ou não mustMatch)
 */
export const mustMatch = (ctrlName : string, matchingCtrlName : string, lang: string = "en", flag : boolean = true) : ValidatorFn => (form : FormGroup) : ValidationErrors | null => {
    const ctrl = form.controls[ctrlName];
    const matchingCtrl = form.controls[matchingCtrlName];

    if(ctrl === undefined || !ctrl || matchingCtrl === undefined || !matchingCtrl) {
        return null;
    }

    if(matchingCtrl.errors && (matchingCtrl.errors["mustMatch"] === undefined || matchingCtrl.errors["mustMatch"] === null)) {
        // Retorna se outro validator encontrou algum outro erro no confirm ctrl
        return ;
    }

    let objError = null;

    if(flag && ctrl.value !== matchingCtrl.value) {
        objError = {
            mustMatch: true, 
            message: replaceWithParamsSync(
                mustMatchError[lang], 
                {
                    ctrlName: ctrlName, 
                    matchingCtrlName: matchingCtrlName
                }
            )
        };
    } else if(!flag && ctrl.value === matchingCtrl.value) {
        objError = {
            notMustMatch: true, 
            message: replaceWithParamsSync(
                notMustMatchError[lang], 
                {
                    ctrlName: ctrlName, 
                    matchingCtrlName: matchingCtrlName
                }
            )
        };
    }

    matchingCtrl.setErrors(
        objError
    );
};
