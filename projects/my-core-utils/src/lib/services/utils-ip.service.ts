import { 
    Injectable
} from "@angular/core";

import { 
    HttpClient, 
    HttpRequest, 
    HttpResponse
} from "@angular/common/http";

import { 
    Observable, 
    of
} from "rxjs";

import { 
    catchError,
    skipWhile,
    switchMap
} from "rxjs/operators";

// Constants 
import { 
    IP_LOCATION_ENDPOINTS
} from "./constants";

// Utils 
import { 
    createHttpHeaders
} from "../http/utils";

@Injectable()
export class UtilsIpService {
    constructor(
        protected _http : HttpClient 
    ) {};

    /**
     * Método para solicitar a execução do request para obtenção do location
     */
    public getIp = () : Observable<string | null> => {
        const req = new HttpRequest(
            "GET", 
            IP_LOCATION_ENDPOINTS.get("ip"), 
            {
                headers: createHttpHeaders()
            }
        );

        return this._http.request(req).pipe(
            skipWhile(($event : any) => !($event instanceof HttpResponse && $event["status"] === 200)), 
            switchMap(($event : any) => of(
                    $event["body"]["ip"]
                )
            ),
            catchError((err : Error) => {
                console.log("Error ao obter o ip do driver", err);

                return of(
                    null
                );
            })
        );
    };
};