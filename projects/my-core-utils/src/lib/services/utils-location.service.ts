import { 
    Injectable
} from "@angular/core";

import { 
    HttpClient, 
    HttpRequest, 
    HttpResponse
} from "@angular/common/http";

import { 
    Observable, 
    of
} from "rxjs";

import { 
    catchError,
    skipWhile,
    switchMap
} from "rxjs/operators";

// Constants 
import { 
    IP_LOCATION_ENDPOINTS
} from "./constants";

// Models 
import { 
    Location
} from "./models/location";

// Utils 
import { 
    createHttpHeaders
} from "../http/utils";

@Injectable()
export class UtilsLocationService {
    constructor(
        protected _http : HttpClient 
    ) {};

    /**
     * Método para solicitar a execução do request para obtenção do location
     */
    public getLocation = () : Observable<Location | null> => {
        const req = new HttpRequest(
            "GET", 
            IP_LOCATION_ENDPOINTS.get("location"), 
            {
                headers: createHttpHeaders()
            }
        );

        return this._http.request(req).pipe(
            skipWhile(($event : any) => !($event instanceof HttpResponse && $event["status"] === 200)), 
            switchMap(($event : any) => of(
                    $event["body"]
                )
            ),
            catchError((err : Error) => {
                console.log("Error ao obter o location do driver", err);

                return of(
                    null
                );
            })
        );
    };
};