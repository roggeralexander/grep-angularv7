/**
 * Constante para representar os endpoins para obter o ip ou location do driver
 */
export const IP_LOCATION_ENDPOINTS : Map<string, string> = new Map([
    [ "ip", "https://api.ipify.org?format=json" ], 
    [ "location", "https://ipapi.co/$ip/json" ]
]);