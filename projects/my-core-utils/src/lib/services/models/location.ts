/**
 * Classe para representar a estrutura do ip location
 */
export class Location {
    ip?: string;
    city?: string;
    region?: string;
    region_code?: string;
    country?: string; 
    country_name?: string;
    continent_code?: string;
    in_eu?: string | boolean;
    postal?: string | number;
    latitude?: string | number;
    longitude?: string | number;
    timezone?: string;
    utc_offset?: string | number;
    country_calling_code?: string;
    currency?: string;
    languages?: string | string[];
    asn?: string;
    org?: string;
};