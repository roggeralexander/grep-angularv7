import { 
    EventEmitter,
    Inject, 
    Injectable,
    PLATFORM_ID
} from "@angular/core";

import { 
    isPlatformServer
} from "@angular/common";

import { 
    fromEvent, 
    merge, 
    Observable, 
    of
} from "rxjs";

import { 
    mapTo
} from "rxjs/operators"; 

@Injectable()
export class UtilsCheckConnService {
    // Emitter para saber se está online ou não 
    public change : EventEmitter<boolean> = new EventEmitter();

    // Propriedade para saber se está ou não online 
    protected _isOnline : boolean = false;

    constructor(
        @Inject(PLATFORM_ID) protected _platformId : Object
    ) { 
        if(isPlatformServer(this._platformId)) {
            return ; 
        }

        setTimeout(() => {
            this._check().subscribe((isOk : boolean) => {
                this._isOnline = isOk; 

                this.change.emit(
                    this._isOnline
                );
            });
        });
    };

    /**
     * Método para saber se a conexão está ou não ativa
     */
    public getIsOnline = () : boolean => this._isOnline;

    /**
     * Método para checar o status da conexão
     * O map é usado para transformar o valor retornado pelo from event em um valor booleano
     * O create para iniciar o valor retornado pelo merge
     */
    protected _check = () : Observable<boolean> => merge(
        fromEvent(
            window, 
            "online"
        ).pipe(
            mapTo(true)
        ), 
        fromEvent(
            window, 
            "offline"
        ).pipe(
            mapTo(false)
        ), 
        of(
            navigator.onLine === true
        )
    );
};