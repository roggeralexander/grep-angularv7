import { 
    Pipe, 
    PipeTransform
} from "@angular/core"; 

import { 
    Observable, 
    of
} from "rxjs"; 

// Models 
import { 
    Page, 
    PageOptions
} from "../models"; 

// Utilitários 
import { 
    createPages
} from "../utils"; 

@Pipe({
    name: "mcGetPages"
})
export class GetPagesPipe implements PipeTransform {
    transform = (pageIndex : number, options: PageOptions) : Observable<Page[] | null> => createPages(
        pageIndex, 
        options
    );
};