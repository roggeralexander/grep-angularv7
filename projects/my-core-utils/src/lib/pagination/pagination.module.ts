import { 
    NgModule
} from "@angular/core"; 

import { 
    CommonModule
} from "@angular/common"; 

// Pipes
import { 
    GetPagesPipe
} from "./pipes/get-pages.pipe"; 

const ARRAY_PIPES = [
    GetPagesPipe
];

@NgModule({
    imports: [
        CommonModule
    ], 
    declarations: ARRAY_PIPES, 
    exports: ARRAY_PIPES
})
export class PaginationModule { };