/**
 * Interface para representar uma instância da Page
 */
export class Page {
    label: string = null; 
    value: number = null; 
};  

/**
 * Interface para representar as opções de pagination
 */
export class PageOptions {
    totalPages : number = 1; 
    maxBtns : number = 7;
    ellipses : string = "..."; 
};