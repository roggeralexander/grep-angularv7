import { 
    Page, 
    PageOptions
} from "../models";    

import { 
    Observable, 
    of
} from "rxjs";

/**
 * Método para gerar os labels dos paginators
 * @param pageIndex 
 * @param totalPages 
 * @param maxBtns
 * @param ellipses
 */
export const createPages = (pageIndex : number, options : PageOptions) : Observable<Page[] | null> => {
    if(options["totalPages"] === undefined || options["totalPages"] === null || options["totalPages"] < 1 || options["maxBtns"] === undefined || options["maxBtns"] === null || options["maxBtns"] < 7) {
        return of(null);
    }

    const paginationRange : number = options["maxBtns"];
    const rangePages : Page[] = [];
    
    const halfWay : number = Math.ceil(
        paginationRange / 
        2
    );

    const isStart = pageIndex <= halfWay; 
    const isEnd = options["totalPages"] - halfWay < pageIndex;
    const isMiddle = !isStart && !isEnd;

    const ellipsesNeeded = paginationRange < options["totalPages"];
    let i = 1; 

    while(i <= options["totalPages"] && i <= paginationRange) {
        const pageNumber = calculatePageNumber(
            i, 
            pageIndex, 
            options["maxBtns"], 
            options["totalPages"]
        );

        const openingEllipsesNeeded = (i === 2  && (isMiddle || isEnd));

        const closingEllipsesNeeded = (i === paginationRange - 1 && (isMiddle || isStart));

        rangePages.push({
            label: (ellipsesNeeded && (openingEllipsesNeeded || closingEllipsesNeeded)) ?
                options["ellipses"] : 
                `${ pageNumber }`, 
            value: pageNumber
        });

        i++;
    }

    return of(
        rangePages
    );
};

/**
 * Método para calcular o número de página
 * @param i 
 * @param currentPage 
 * @param paginationRange 
 * @param totalPages 
 */
export const calculatePageNumber = (i : number, currentPage : number, paginationRange : number, totalPages : number) : number => {
    const halfWay = Math.ceil(paginationRange / 2);
    
    if (i === paginationRange) {
        return totalPages;
    } else if (i === 1) {
        return i;
    } else if (paginationRange < totalPages) {
        if (totalPages - halfWay < currentPage) {
            return totalPages - paginationRange + i;
        } else if (halfWay < currentPage) {
            return currentPage - halfWay + i;
        } else {
            return i;
        }
    } else {
        return i;
    }
};