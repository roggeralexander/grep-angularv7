import { 
    isPlatformServer
} from "@angular/common";   

import { 
    fromEvent, 
    Observable,
    of, 
    Subject, 
    Subscription
} from "rxjs";

import { 
    switchMap
} from "rxjs/operators";

// Constantes
import { 
    ESCAPE
} from "@angular/cdk/keycodes";

/**
 * Classe para permitir as interações do escape button
 */
export class EscapePress {
    // Propriedade para saber se está acima ou fora do target
    protected _isHover : boolean = false; 

    // Output
    public escape : Subject<void> = new Subject();

    // Subscriptions 
    protected _subs : Subscription[] = [];

    constructor(
        protected _elm : HTMLElement, 
        protected _platformId : Object
    ) {
        if(!isPlatformServer(this._platformId)) {
            setTimeout(() => {
                // Subscription para inicializar a instância 
                this._subs.push(
                    of(null).pipe(
                        switchMap(() => this._initialize())
                    ).subscribe(() => { 
                        console.log("Escape press initialized");
                    })
                );                
            });
        }
    };

    /**
     * Método para inicializar a instância
     */
    protected _initialize = () : Observable<void> => {
        this._subs.push(
            fromEvent(
                this._elm, 
                "mouseenter"
            ).subscribe(
                this._cbOnMouseEnter
            )
        );
    
        this._subs.push(
            fromEvent(
                this._elm, 
                "mouseleave"
            ).subscribe(
                this._cbOnMouseLeave
            )
        );
    
        this._subs.push(
            fromEvent(
                document, 
                "keydown"
            ).subscribe(
                this._cbOnKeyDown
            )
        );

        return of(
            undefined
        );
    };

    /**
     * Método para destroir a instância
     */
    public destroy = () : void => {
        this._subs.forEach((sub : Subscription) => {
            sub.unsubscribe();
        });

        this.escape && !this.escape.closed ? 
            this.escape.complete() : 
            null;
    };

    /**
     * Callback quando 'mouseenter' acontecer
     */
    protected _cbOnMouseEnter = ($event : MouseEvent) : void => {
        this._isHover = true;
    };

    /**
     * Callback quando 'mouseleave' acontecer
     */
    protected _cbOnMouseLeave = ($event : MouseEvent) : void => {
        this._isHover = false;
    };

    /**
     * Callback quando 'keydown' acontecer
     */
    protected _cbOnKeyDown = ($event : KeyboardEvent) : void => {
        const keyCode : number = $event.keyCode || $event.which;

        if(this._isHover) {
            if(keyCode === ESCAPE) {
                this.escape.next();
            }
        }    
    };
};