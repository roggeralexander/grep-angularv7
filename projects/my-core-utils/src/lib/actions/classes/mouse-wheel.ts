import { 
    isPlatformServer
} from "@angular/common";

import { 
    fromEvent, 
    Observable, 
    of, 
    merge, 
    Subject, 
    Subscription
} from "rxjs"; 

/**
 * Classe para aplicação 
 */
export class MouseWheel {
    // Propriedade para saber se está acima ou fora do target
    protected _isHover : boolean = false; 

    // Output 
    public scrollTo : Subject<"previous" | "next"> = new Subject();

    // Subscriptions
    protected _subs : Subscription[] = [];

    constructor(
        protected _elm : HTMLElement, 
        protected _platformId : Object, 
        protected _dir : "horizontal" | "vertical" = "horizontal"
    ) { 
        if(isPlatformServer(this._platformId)) {
            return ;
        }

        setTimeout(() => {
            // Subscription para inicializar a instância 
            this._subs.push(
                this._initialize().subscribe(
                    () => console.log("Mouse wheel inicializado")
                )
            );
        });
    };

    /**
     * Método para destroir a instância
     */
    public destroy = () : void => {
        this._subs.forEach((sub : Subscription) => sub.unsubscribe());
        this._subs = null; 

        this.scrollTo && !this.scrollTo.closed ? 
            this.scrollTo.complete() : 
            null;

        this.scrollTo = null; 
    };

    /**
     * Método para inicializar a instância da classe
     */
    protected _initialize = () : Observable<void> => {
        this._subs.push(
            fromEvent(
                this._elm, 
                "mouseenter"
            ).subscribe(
                this._cbOnMouseEnter
            )
        );
    
        this._subs.push(
            fromEvent(
                this._elm, 
                "mouseleave"
            ).subscribe(
                this._cbOnMouseLeave
            )
        );

        this._subs.push(
            merge(
                fromEvent(
                    this._elm, 
                    "wheel"
                ), 
                // Chrome
                fromEvent(
                    this._elm, 
                    "mousewheel"
                ), 
                // Mozilla Firefox
                fromEvent(
                    this._elm, 
                    "DOMMouseScroll"
                ), 
                // Internet Explorer
                fromEvent(
                    this._elm, 
                    "onmousewheel"
                )
            ).subscribe(
                this._mouseWheel
            )            
        );

        return of(
            undefined
        );
    };

    /**
     * Callback quando 'mouseenter' acontecer
     */
    protected _cbOnMouseEnter = ($event : MouseEvent) : void => {
        this._isHover = true;
    };

    /**
     * Callback quando 'mouseleave' acontecer
     */
    protected _cbOnMouseLeave = ($event : MouseEvent) : void => {
        this._isHover = false;
    };

    /**
     * Callback para execução do mouse wheel 
     * @param $event
     */
    protected _mouseWheel = ($event : any) : Observable<void> => {
        if(!this._isHover) {
            return ; 
        }

        const event = window.event || $event;

        const delta = Math.max(
            -1, 
            Math.min(
                1, 
                (event.wheelDelta || -event.detail)
            )
        );
    
        if(delta > 0) {
            this.scrollTo.next(
                "previous"
            );
        } else if(delta < 0) {
            this.scrollTo.next(
                "next"
            );
        }   

        // Para internet explorer 
        event["returnValue"] = false; 

        // Chrome e Firefox 
        if(event["preventDefault"]) {
            event["preventDefault"]();
        }
        
        return of(
            undefined
        );
    };
};
