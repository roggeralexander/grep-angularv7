import {
    isPlatformServer
} from "@angular/common";

import { 
    fromEvent, 
    Observable,
    of, 
    Subject, 
    Subscription
} from "rxjs";

import { 
    DOWN_ARROW, 
    LEFT_ARROW,
    RIGHT_ARROW, 
    UP_ARROW
} from "@angular/cdk/keycodes";

/**
 * Classe para permitir as interações dos arrow btns
 */
export class ArrowNavs {
    // Propriedade para saber se está acima ou fora do target
    protected _isHover : boolean = false; 

    // Output 
    public scrollTo : Subject<"previous" | "next"> = new Subject();

    // Subscriptions
    protected _subs : Subscription[] = [];

    constructor(
        protected _elm : HTMLElement, 
        protected _platformId : Object, 
        protected _dir : "horizontal" | "vertical" = "horizontal"
    ) { 
        if(isPlatformServer(this._platformId)) {
            return ;
        }

        setTimeout(() => {
            // Subscription para inicializar a instância 
            this._subs.push(
                this._initialize().subscribe(
                    () => console.log("Arrow navs inicializado")
                )
            );
        });
    };

    /**
     * Método para inicializar a instância
     */
    protected _initialize = () : Observable<void> => {
        this._subs.push(
            fromEvent(
                this._elm, 
                "mouseenter"
            ).subscribe(
                this._cbOnMouseEnter
            )
        );
    
        this._subs.push(
            fromEvent(
                this._elm, 
                "mouseleave"
            ).subscribe(
                this._cbOnMouseLeave
            )
        );
    
        this._subs.push(
            fromEvent(
                document, 
                "keydown"
            ).subscribe(
                this._cbOnKeyDown
            )
        );

        return of(
            undefined
        );
    };

    /**
     * Método para destroir a instância
     */
    public destroy = () : void => {
        this._subs.forEach((sub : Subscription) => sub.unsubscribe());

        this.scrollTo && !this.scrollTo.closed ? 
            this.scrollTo.complete() : 
            null;

        this.scrollTo = null; 
    };

    /**
     * Callback quando 'mouseenter' acontecer
     */
    protected _cbOnMouseEnter = ($event : MouseEvent) : void => {
        this._isHover = true;
    };

    /**
     * Callback quando 'mouseleave' acontecer
     */
    protected _cbOnMouseLeave = ($event : MouseEvent) : void => {
        this._isHover = false;
    };

    /**
     * Callback quando 'keydown' acontecer
     */
    protected _cbOnKeyDown = ($event : KeyboardEvent) : void => {
        const keyCode : number = $event.keyCode || $event.which;

        if(!this._isHover) {
            return ; 
        }

        let action : "previous" | "next" = null;
            
        if(this._dir === "horizontal") {
            switch(keyCode) {
                case LEFT_ARROW: 
                    action = "previous";
                    break;
                case RIGHT_ARROW:
                    action = "next";
            }
        } else {
            switch(keyCode) {
                case UP_ARROW: 
                    action = "previous";
                    break; 
                case DOWN_ARROW: 
                    action = "next";
            }    
        }

        if(action) {
            this.scrollTo.next(
                action
            );
        }
    };
};