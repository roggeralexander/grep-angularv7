import {
    Renderer2
} from "@angular/core";

import {
    fromEvent, 
    Subscription
} from "rxjs";

// Interfaces 
import { 
    DragDropData
} from "../models";

/**
 * Classe para aplicação de eventos drag a um determinado elemento
 */
export class Dragger {
    // Array de subscriptions
    protected _subs : Subscription[] = [];

    constructor(
        protected _htmlElm : HTMLElement, 
        protected _renderer : Renderer2, 
        protected _objData: DragDropData
    ) {
        // Seteando os atributos e event listeners
        if(this._htmlElm !== undefined && this._htmlElm && this._objData !== undefined && this._objData) {
            setTimeout(() => {
                this._initialize();
            });
        }
    };

    /**
     * Método para inicializar os atributos e event listeners
     */
    protected _initialize = () : void => {
        this._renderer.setAttribute(
            this._htmlElm, 
            "draggable", 
            "true"
        );

        // Subscription para o drag start 
        this._subs.push(
            fromEvent(
                this._htmlElm, 
                "dragstart"
            ).subscribe(
                this._cbOnDragStart
            )
        );

        // Subscription para o drag end 
        this._subs.push(
            fromEvent(
                this._htmlElm, 
                "dragend"
            ).subscribe(
                this._cbOnDragEnd
            )
        );
    };

    /**
     * Callback quando o drag start acontecer
     * @param $event
     */
    protected _cbOnDragStart = ($event : DragEvent) : void => {
        // Estabelecendo a data a ser transferida 
        $event.dataTransfer.setData(
            this._objData.key, 
            this._objData.value
        );
    };

    /**
     * Callback para quando o drag finalizar 
     * @param $event
     */
    protected _cbOnDragEnd = ($event : DragEvent) : void => { };

    /**
     * Método para destroir  instância
     */
    public destroy = () : void => {
        this._subs.forEach((sub : Subscription) => {
            sub.unsubscribe();
        });

        this._subs = null; 
    };
};