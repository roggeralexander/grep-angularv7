import {
    BehaviorSubject, 
    fromEvent, 
    Subscription
} from "rxjs";

/**
 * Classe para aplicação de eventos droppable a um determinado elemento
 */
export class Droppable {
    // Array de subscriptions
    protected _subs : Subscription[] = [];

    // Subject para emitir a data que foi transferida pelo dragger 
    public data : BehaviorSubject<string> = new BehaviorSubject(null);

    constructor(
        protected _htmlElm : HTMLElement, 
        protected _key : string
    ) { 
        if(this._htmlElm !== undefined && this._htmlElm && this._key !== undefined && this._key) {
            setTimeout(() => {
                this._initialize();
            });
        }
    };

    /**
     * Método para inicializar os event listeners
     */
    protected _initialize = () : void => {
        this._subs.push(
            fromEvent(
                this._htmlElm, 
                "dragenter"
            ).subscribe(
                this._cbOnDragEnter
            )
        );

        this._subs.push(
            fromEvent(
                this._htmlElm, 
                "dragover"
            ).subscribe(
                this._cbOnDragOver
            )
        );

        this._subs.push(
            fromEvent(
                this._htmlElm, 
                "dragleave"
            ).subscribe(
                this._cbOnDragLeave
            )
        );

        this._subs.push(
            fromEvent(
                this._htmlElm, 
                "drop"
            ).subscribe(
                this._cbOnDrop
            )
        );
    };

    /**
     * Callback quando o drag enter acontecer 
     * @param $event
     */
    protected _cbOnDragEnter = ($event : DragEvent) : void => { };

    /**
     * Callback quando o drag leave acontecer 
     * @param $event
     */
    protected _cbOnDragLeave = ($event : DragEvent) : void => { };

    /**
     * Callback quando o drag over acontecer 
     * @param $event
     */
    protected _cbOnDragOver = ($event : DragEvent) : void => {
        $event.preventDefault();
    };

    /**
     * Callback quando o drop acontecer 
     * @param $event
     */
    protected _cbOnDrop = ($event : any) : void => {
        $event.preventDefault();

        let data = $event.dataTransfer.getData(
            this._key
        );

        if(data !== undefined && data) {
            this.data.next(
                data
            );
        }
    };

    /**
     * Método para destroir a instância
     */
    public destroy = () : void => {
        this.data.complete();

        this._subs.forEach((sub : Subscription) => {
            sub.unsubscribe();
        });

        this._subs = null; 
        this.data = null; 
    };
};