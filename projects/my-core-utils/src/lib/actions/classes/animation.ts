import { 
    ElementRef, 
    EventEmitter, 
    Output
} from "@angular/core";

import { 
    isPlatformServer
} from "@angular/common";

// Angular Animations Library
import { 
    animate, 
    AnimationBuilder, 
    AnimationFactory, 
    AnimationPlayer, 
    style
} from "@angular/animations";
  
/**
 * Classe customizada para aplicação de animações a um determinado element Ref
 */
export class Animation {
    // Propriedade para referenciar o animation player 
    protected _player : AnimationPlayer = null;

    // Instância HTMLElement 
    protected _elm : HTMLElement = null;

    @Output() 
    public started : EventEmitter<void> = new EventEmitter();

    @Output() 
    public completed : EventEmitter<void> = new EventEmitter();

    constructor(
        protected _animationBuilder : AnimationBuilder, 
        protected _platformId : Object, 
        protected _targetRef : HTMLElement | ElementRef
    ) { 
        if(!isPlatformServer(this._platformId)) {
            if(this._targetRef === undefined || !this._targetRef) {
                return ;
            }

            this._elm = this._targetRef instanceof ElementRef ? 
                this._targetRef.nativeElement : 
                this._targetRef;
        }
    };


    /**
     * Método para setear o target ao qual a animação será aplicada
     * @param elm
     */
    public setTarget = (elm : HTMLElement | ElementRef) : void => {
        if(elm === undefined || !elm) {
            return ;
        }

        this._elm = elm instanceof ElementRef ? 
            elm.nativeElement : 
            elm;
        
        return ;
    };

    /**
     * Método para solicitar uma instãncia do animation factory 
     * @param timing
     * @param styleObj
     * @param animationType
     */
    protected _buildAnimation = (timing : number, styleObj : any = {}, animationType : string = 'ease-in') : AnimationFactory => this._animationBuilder.build([
        animate(
            `${ timing }ms ${ animationType }`, 
            style(
                styleObj
            )
        )
    ]);

    /**
     * Método para aplicar a animação no target elm 
     * @param styleObj 
     * @param timing
     */
    public animate = (styleObj : any, timing : number = 250) : void => {
        if(!isPlatformServer(this._platformId) && this._elm !== undefined && this._elm) {
            const myAnimation : AnimationFactory = this._buildAnimation(
                timing, 
                styleObj
            );
          
            this._player = myAnimation.create(
                this._elm
            );

            this._player.onDone(() => {
                this.completed.emit();
            });

            this._player.play();

            this.started.emit();
        }
    };

    /**
     * Método para destroir a instância de animation
     */
    public destroy = () : void => {
        this._player = null;
        this.started.complete(); 
        this.completed.complete();
    };
};