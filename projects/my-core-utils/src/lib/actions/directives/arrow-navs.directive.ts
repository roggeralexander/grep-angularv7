import { 
    AfterViewInit,
    Directive, 
    ElementRef, 
    EventEmitter,
    Inject,
    OnDestroy,
    Output,
    PLATFORM_ID
} from '@angular/core';

import { 
    isPlatformServer
} from "@angular/common";

import { 
    Subscription
} from "rxjs";

// Classes 
import { 
    ArrowNavs
} from "../classes/arrow-navs";

@Directive({
    selector: '[mcArrowNavs]'
})
export class ArrowNavsDirective  implements AfterViewInit, OnDestroy {
    // Instância de arrow navs
    protected _arrowNavs : ArrowNavs = null;

    // Subscriptions para os events 
    protected _subs : Subscription[] = [];

    // Output
    @Output() 
    public scrollTo : EventEmitter<"previous" | "next"> = new EventEmitter();

    constructor(
        protected _elmRef : ElementRef, 
        @Inject(PLATFORM_ID) protected _platformId : Object
    ) { };

    ngAfterViewInit() {
        if(!isPlatformServer(this._platformId)) {
            this._arrowNavs = new ArrowNavs(
                this._elmRef.nativeElement, 
                this._platformId
            );

            // Gerando as subscriptions para o arrow navs 
            this._subs.push(
                this._arrowNavs.scrollTo.subscribe((scrollTo : "previous" | "next") => {
                    this.scrollTo.emit(
                        scrollTo
                    );
                })
            );
        }
    };

    ngOnDestroy() {
        this._subs.forEach((sub : Subscription) => {
            sub.unsubscribe();
        });

        if(this._arrowNavs) {
            this._arrowNavs.destroy(); 
            this._arrowNavs = null;
        }
    };
};
