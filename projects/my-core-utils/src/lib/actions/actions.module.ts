import { 
    NgModule
} from "@angular/core"; 

import { 
    CommonModule
} from "@angular/common"; 

// Directives 
import { 
    ArrowNavsDirective
} from "./directives/arrow-navs.directive"; 

@NgModule({
    imports: [
        CommonModule
    ],
    declarations: [
        ArrowNavsDirective
    ], 
    exports: [
        ArrowNavsDirective
    ]
})
export class ActionsModule { };