// Interface para a data a ser transferida a partir do drag drop
export interface DragDropData {
    key: string;
    value: string;
};