import { 
    HttpHeaders, 
    HttpParams
} from "@angular/common/http";

/**
 * Method to create an http headers from any object
 * @param headers 
 */
export const createHttpHeaders = (headers? : any) : HttpHeaders | null => {
    let httpHeaders : HttpHeaders = new HttpHeaders();

    if(typeof headers !== "undefined" && headers && Object.keys(headers).length > 0) {
        Object.keys(headers).forEach(key => {
            httpHeaders = httpHeaders.append(
                key, 
                headers[key]
            );
        });
    } else {
        httpHeaders = httpHeaders.append(
            "Content-Type", 
            "application/json"
        );
    }

    return httpHeaders;
};

/**
 * Method to create an http params from any object
 * @param params 
 */
export const createHttpParams = (params? : any) : HttpParams | null => {
    let httpParams : HttpParams = new HttpParams();

    if(params !== undefined && params && Object.keys(params).length > 0) {
        Object.keys(params).forEach(key => {
            let value = params[key];

            if(value.constructor.name === "Array") {
                value.forEach(val => {
                    httpParams = httpParams.append(
                        key, 
                        val
                    );
                });
            } else {
                httpParams = httpParams.append(
                    key, 
                    value
                );
            }
        });
    } 

    return httpParams;
};

/**
 * Method to add optional params to a http params object
 * @param params 
 * @param optionalParams
 */
export const addOptionalParams = (params : HttpParams, optionalParams : any) : HttpParams => {
    if(params === undefined || !params || !(params instanceof HttpParams)) {
        return new HttpParams();
    }

    if(optionalParams === undefined || !params || !Object.keys(optionalParams).length) {
        return params;
    }

    Object.keys(optionalParams).forEach(key => {
        let value = optionalParams[key];

        if(value.constructor.name === "Array") {
            value.forEach(val => {
                params = params.append(
                    key, 
                    val
                );
            });
        } else {
            params = params.append(
                key, 
                value
            );
        }
    });

    return params;
};