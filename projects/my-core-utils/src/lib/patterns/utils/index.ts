// Third party libraries 
import hexColorRegex from "hex-color-regex";

/**
 * Método para saber se uma cor cumpre ou não com um padrão
 * @param color 
 */
export const isColorOk = (color : string) : boolean => {
    if(color === undefined || !color || !color.trim().length) {
        return false;
    }

    return hexColorRegex().test(
        color
    );
};

/**
 * Método para validar um email
 * @param email 
 */
export const isEmailOk = (email : string) : boolean => {
    if(email === undefined || !email || !email.trim().length) {
        return false;
    }

    const regExp = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

    return email.match(regExp) === null ? 
        false : 
        true;
};

/**
 * Method to confirm is the url follows the pattern
 * @param value 
 */
export const isUrlOk = (value : string) : boolean => {
    try {
        let objUrl = new URL(value);
        let hostname = objUrl.hostname.replace(
            new RegExp("www.|www", "gi"), 
            ""
        );

        // Regex to validate the hostname
        const regExp = /([a-z])([a-z0-9]+\.)*[a-z0-9]+\.[a-z.]+/g;

        if(!hostname.match(regExp)) {
            throw new Error("abc");
        }

        return true;
    } catch(err) {
        return false;
    }
};