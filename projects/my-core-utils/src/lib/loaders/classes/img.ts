import { 
    fromEvent, 
    Observable, 
    of, 
    Subject, 
    Subscription, 
    timer
} from "rxjs";  

import { 
    catchError, 
    switchMap
} from "rxjs/operators"; 

// Constante para o retardo da leitura do arquivo 
export const DELAY_LOADED : number = 200;

/**
 * Classe para implementação do image loader
 */
export class ImgLoader {
    // Propriedade para referenciar à instãncia do image 
    public img : HTMLImageElement = null; 

    // Subscriptions 
    protected _subs : Subscription[] = [];

    // Outputs
    public loading : Subject<boolean> = new Subject(); 
    public loaded : Subject<void> = new Subject();

    constructor(
        protected _src : string, // Source da imagem
        protected _alt?: string // Descrição da imagem
    ) { 
        // Leitura da imagem 
        this._subs.push(
            timer(DELAY_LOADED).pipe(
                switchMap(() => this._load()), 
                switchMap(() => {
                    this.loading.next(false); 
                    this.loaded.next();
                    return of(
                        undefined
                    ); 
                }), 
                catchError((err : Error) => {
                    console.log(`Error loaded img: ${ this._src }:`, err);
                    this.loading.next(false); 
                    return of(
                        undefined
                    ); 
                })
            ).subscribe(() => { 
                console.log(`Image com src: ${ this._src } processada`);
            })
        );
    };

    /**
     * Metodo para solicitar a leitura do arquivo 
     */
    protected _load = () : Observable<void> => new Observable(observer => {
        this.loading.next(true); 

        this.img = new Image();

        const onNext = () => {
            observer.next(); 
            observer.complete();
        };

        const onError = (err : Error) => {
            observer.error(err);
            observer.complete();
        };

        this._subs.push(
            fromEvent(
                this.img, 
                "load"
            ).subscribe(
                onNext, 
                onError
            )
        );

        this.img.src = this._src;
        
        if(this._alt !== undefined && this._alt && this._alt.trim().length > 0) {
            this.img.alt = this._alt;
        }
    });

    /**
     * Método para destroir a instância 
     */
    public destroy = () : void => {
        this._subs.forEach((sub : Subscription) => {
            sub.unsubscribe();
        });

        !this.loaded.closed ? 
            this.loaded.complete() : 
            null; 
        
        !this.loading.closed ? 
            this.loading.complete() : 
            null; 

        this.img = null; 
    };
};