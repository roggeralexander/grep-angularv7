import { 
    Observable, 
    of, 
    throwError
} from "rxjs"; 

import { 
    switchMap
} from "rxjs/operators";

// Third party modules 
import {
    saveAs
} from "file-saver"; 

import uuid from "uuid"; 

/**
* Método para gerar o arquivo em formato base64 - string
* @param canvas 
* @param mimeType 
* @param quality 
*/
export const canvasToBase64 = (canvas : HTMLCanvasElement, mimeType : string, quality : number = 1) : Observable<string | null> => {
    const imageBase64 = canvas.toDataURL(
        mimeType, 
        quality
    );

    return of(
        imageBase64
    );
};

/**
* Método para gerar o arquivo desde o canvas como Blob
* @param canvas 
* @param mimeType 
* @param quality 
*/
export const canvasToBlob = (canvas : HTMLCanvasElement, mimeType : string, quality : number = 1) : Observable<Blob | null> => new Observable(observer => {
    console.log("Canvas passado ao canvas to blob", canvas);

    canvas.toBlob(
        (blob : Blob | null) => {
            console.log("Blob resultante", blob);

            observer.next(blob);
            observer.complete();
        }, 
        mimeType, 
        quality
    )
});

/**
 * Método para solicitar a conversão de um elemento canvas em um arquivo ou objeto de tipo File
 * @param canvas 
 * @param mimeType 
 * @param extension 
 * @param quality 
 */
export const canvasToFile = (canvas : HTMLCanvasElement, mimeType : string, extension : string, quality : number = 1) : Observable<File | null> => canvasToBlob(
    canvas, 
    mimeType, 
    quality
).pipe(
    switchMap((blob : Blob) => {
        const fileName : string = uuid.v4().replace(
            new RegExp("-", "gi"), 
            ""
        );

        return blobToFile(
            blob, 
            fileName, 
            extension
        );
    })
);

/**
 * Método para converter uma instância do blob em base64 encode
 * @param blob 
 */
export const blobToBase64 = (blob : Blob) : Observable<any | null> => new Observable(observer => {
    if(blob === undefined || !blob) {
        observer.error(
            new Error(
                "Please inform the blob"
            )
        );
        observer.complete(); 
        return ; 
    }

    const reader = new FileReader();

    reader.onloadend = () => {
        const dataUrl = reader.result;
        observer.next(dataUrl);
        observer.complete();
    };

    reader.readAsDataURL(blob);
});
 
/**
 * Método para transformar um objeto blob to file
 * @param blob 
 * @param fileName 
 * @param extension
 */
export const blobToFile = (blob : Blob, fileName : string, extension : string) : Observable<File | null> => {
    console.log("Blob to file"); 
    console.log("blob", blob); 
    console.log("File name", fileName); 
    console.log("Extension", extension);
    
    if(blob === undefined || !blob || fileName === undefined || !fileName || extension === undefined || !extension) {
        return throwError(
            new Error(
                "Please inform the blob, fileName or extension"
            )
        );
    }

    let file : File = new File(
        [ blob ], 
        `${ fileName }.${ extension }`, 
        {
            type: blob.type
        }
    );

    return of(
        file
    );
};

/**
 * Método para solicitar que um arquivo seja salvo
 * @param blob 
 * @param extension 
 */
export const saveFileFromBlob = (blob : Blob, extension : string, fileName? : string) : void => {
    if(blob === undefined || !blob || extension === undefined || !extension || !extension.trim().length) {
        return ; 
    }

    fileName = fileName === undefined || !fileName || !fileName.trim().length ? 
        uuid.v4().replace(
            new RegExp("-", "gi"), 
            ""
        ) :
        fileName;

    setTimeout(() => {
        saveAs(
            blob, 
            `${ fileName }.${ extension }`
        );
    });
};

/**
 * Método assíncrono para obter  a extensão de um arquivo
 * @param fileName 
 */
export const extname = (fileName : string) : Observable<string | null> => of(
    fileName.slice((fileName.lastIndexOf(".") - 1 >>> 0) + 2)
);

/**
 * Método síncrono para obter a extensão de um arquivo
 * @param fileName 
 */
export const extnameSync = (fileName : string) : string => fileName.slice((fileName.lastIndexOf(".") - 1 >>> 0) + 2);