import { 
  ModuleWithProviders,
  NgModule 
} from '@angular/core';

import { 
  FormsModule, 
  ReactiveFormsModule
} from "@angular/forms"; 

import { 
  HttpClientModule
} from "@angular/common/http"; 

// Third party library 
import {
  NgPipesModule
} from "ngx-pipes"; 

// Sub modulos 
import { 
  ActionsModule
} from "./actions/actions.module"; 

import {
  DateTimeModule
} from "./date-time/date-time.module"; 

import {
  PaginationModule
} from "./pagination/pagination.module"; 

import { 
  StringModule
} from "./string/string.module";  

// Services 
import { 
  UtilsCheckConnService
} from "./services/utils-check-conn.service";

import { 
  UtilsIpService
} from "./services/utils-ip.service";

import { 
  UtilsLocationService
} from "./services/utils-location.service";

@NgModule({
  imports: [
    FormsModule, 
    ReactiveFormsModule, 
    HttpClientModule
  ],
  exports: [
    ActionsModule, 
    DateTimeModule, 
    NgPipesModule, 
    PaginationModule, 
    StringModule
  ]
})
export class MyCoreUtilsModule { 
  static forRoot() : ModuleWithProviders {
    return {
      ngModule: MyCoreUtilsModule, 
      providers: [
        UtilsCheckConnService, 
        UtilsIpService,
        UtilsLocationService
      ]
    };
  };
};
