import { 
    Pipe, 
    PipeTransform
} from "@angular/core"; 

import { 
    Observable
} from "rxjs"; 

// Utils 
import {
    removeAcent
} from "../utils"; 

@Pipe({
    name: "mcRemoveAcent"
})
export class RemoveAcentPipe implements PipeTransform {
    transform = (value : string, args? : any[]) : Observable<string | null> => removeAcent(value);
};