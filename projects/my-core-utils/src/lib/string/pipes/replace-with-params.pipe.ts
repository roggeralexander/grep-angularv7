import { 
    Pipe, 
    PipeTransform
} from "@angular/core"; 

import { 
    Observable
} from "rxjs"; 

// Utils 
import { 
    replaceWithParams
} from "../utils"; 

@Pipe({
    name: "mcReplaceWithParams"
})
export class ReplaceWithParamsPipe implements PipeTransform {
    transform = (value : string, params : any = {} ) : Observable<string | null> => replaceWithParams(
        value, 
        params
    );
};