import { 
    Pipe, 
    PipeTransform
} from "@angular/core"; 

import { 
    Observable
} from "rxjs"; 

// Utils 
import { 
    stripHtml
} from "../utils"; 

@Pipe({
    name: "mcStripHtml"
})
export class StripHtmlPipe implements PipeTransform {
    transform = (value : string, allowedTags : string[] = []) : Observable<string | null> => stripHtml(
        value, 
        allowedTags
    );
};
