import { 
    Pipe, 
    PipeTransform
} from "@angular/core"; 

import { 
    Observable
} from "rxjs"; 

// Utils 
import { 
    stripString
} from "../utils"; 

@Pipe({
    name: "mcStripString"
})
export class StripStringPipe implements PipeTransform {
    transform = (value : string, options?: { specialChars?: boolean, numberChars?: boolean }) : Observable<string | null> => stripString(
        value, 
        options
    );
};
