// Pipes 
export * from "./pipes/remove-acent.pipe"; 
export * from "./pipes/replace-with-params.pipe"; 
export * from "./pipes/strip-html.pipe"; 

// Utils
export * from "./utils"; 

// Modules
export * from "./string.module";