import { 
    Observable, 
    of
} from "rxjs"; 

// Third party libraries 
import striptags from "striptags"; 

/**
 * Método síncrono para remoção das tags do html informado que não estejam incluídos no allowedTags
 * @param html 
 * @param allowedTags 
 */
export const stripHtmlSync = (html : string, allowedTags : string[]) : string => striptags(html, allowedTags).trim();

/**
 * Método assíncrono para remoção das tags do html informado
 * @param html 
 * @param allowedTags 
 */
export const stripHtml = (html : string, allowedTags : string[] = []) : Observable<string> => of(
    stripHtmlSync(html, allowedTags)
);

/**
 * Método síncrono para solicitar a remoção de acentos de forma síncrona 
 * @param str 
 */
export const removeAcentSync = (str : string) : string | null => {
    if(str === undefined || !str || !str.trim().length) {
        return null; 
    }

    let accents = 'ÀÁÂÃÄÅàáâãäåßÒÓÔÕÕÖØòóôõöøÈÉÊËèéêëðÇçÐÌÍÎÏìíîïÙÚÛÜùúûüÑñŠšŸÿýŽž';
    let accentsOut = "AAAAAAaaaaaaBOOOOOOOooooooEEEEeeeeeCcDIIIIiiiiUUUUuuuuNnSsYyyZz";
    let arrayStr = str.split('');

    arrayStr.forEach((letter, index) => {
        let i = accents.indexOf(letter);
  
        if (i != -1) {
            arrayStr[index] = accentsOut[i];
        }
    });

    return arrayStr.join('');    
};

/**
 * Método assíncrono para solicitar a remoção de acentos de forma síncrona 
 * @param str 
 */
export const removeAcent = (str : string) : Observable<string | null> => of(
    removeAcentSync(str)
);

/**
 * Método para aplicar o strip a uma string de forma síncrona
 * @param str 
 * @param options
 */
export const stripStringSync = (str : string, options?: { specialChars?: boolean , numberChars?: boolean }) : string | null => {
    let value : string = removeAcentSync(
        str
    );

    if(!value) {
        return null; 
    }

    // Removendo todos os caracteres especiais
    if(options !== undefined && options["specialChars"] !== undefined && options["specialChars"] === true) {
        value = value.replace(/[^\w\s]/gi, ' ');
    }

    // Removendo os caracteres numéricos 
    if(options !== undefined && options["numberChars"] !== undefined && options["numberChars"] === true) {
        value = value.replace(/\d+/g, "");
    }

    // Removendo qualquer underline 
    value = value.replace(/_/g, ""); 

    // Removendo mais de dois espaços para um único 
    value = value.replace(/ +/g, " ");

    // Caso o último elemento for " ", remover 
    if(value.charAt(value.length - 1) === " ") {
        value = value.substring(
            0, 
            value.length - 1
        );
    }

    return value.trim();
};

/**
 * Método assíncrono para substituir os acentos, remover caracteres especiais, espaços em branco e caracteres numéricos
 * @param str 
 * @param options
 */
export const stripString = (str : string, options?: { specialChars?: boolean , numberChars?: boolean }) : Observable<string | null> => of(
    stripStringSync(
        str, 
        options
    )
);

/**
 * Método síncrono para substituir uma mensagem com os valores dos parâmetros
 * @param msg 
 * @param params 
 */
export const replaceWithParamsSync = (msg : string, params : any = {}) : string | null=> {
    if(msg === undefined || !msg || !msg.trim().length) {
        return null;
    }

    if(params === undefined || !params || !Object.keys(params).length) {
        return msg;
    }

    let message = msg;

    Object.keys(params).forEach(key => {
        message = message.replace(
            new RegExp(
                `\\$${ key }`, 
                "gi"
            ), 
            params[key]
        );
    });

    return message;
};

/**
 * Método assíncrono para substituir uma mensagem com os valores dos parâmetros
 * @param msg 
 * @param params 
 */
export const replaceWithParams = (msg : string, params : any = {}) : Observable<string | null> => of(
    replaceWithParamsSync(
        msg, 
        params
    )
);