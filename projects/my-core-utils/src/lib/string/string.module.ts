import { 
    NgModule
} from "@angular/core"; 

import { 
    CommonModule
} from "@angular/common";   

// Pipes 
import { 
    RemoveAcentPipe
} from "./pipes/remove-acent.pipe";

import {
    ReplaceWithParamsPipe
} from "./pipes/replace-with-params.pipe"; 

import { 
    StripHtmlPipe
} from "./pipes/strip-html.pipe";

import { 
    StripStringPipe
} from "./pipes/strip-string.pipe";

// ARRAY PIPES 
const ARRAY_PIPES = [
    RemoveAcentPipe, 
    ReplaceWithParamsPipe, 
    StripHtmlPipe, 
    StripStringPipe
];

@NgModule({
    imports: [
        CommonModule
    ], 
    declarations: ARRAY_PIPES, 
    exports: ARRAY_PIPES
})
export class StringModule {};