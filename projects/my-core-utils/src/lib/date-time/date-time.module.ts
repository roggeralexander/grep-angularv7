import { 
    NgModule
} from "@angular/core"; 

import {
    CommonModule
} from "@angular/common"; 

// Pipes 
import { 
    UtilsFormatDatePipe
} from "./pipes/format-date.pipe"; 

import { 
    UtilsFormatDurationPipe
} from "./pipes/format-duration.pipe";

import { 
    UtilsTimeAgoPipe
} from "./pipes/time-ago.pipe";

// ARRAY PIPES 
const ARRAY_PIPES = [
    UtilsFormatDatePipe, 
    UtilsFormatDurationPipe,
    UtilsTimeAgoPipe
];

@NgModule({
    imports: [
        CommonModule
    ],
    declarations: ARRAY_PIPES, 
    exports: ARRAY_PIPES
})
export class DateTimeModule { };