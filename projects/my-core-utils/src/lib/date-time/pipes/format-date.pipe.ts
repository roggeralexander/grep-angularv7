import { 
    Pipe, 
    PipeTransform
} from "@angular/core";

import { 
    Observable, 
    of
} from "rxjs"; 

// Third party libraries 
import moment from "moment";

@Pipe({
    name: "mcFormatDate"
})
export class UtilsFormatDatePipe implements PipeTransform {
    transform = (value : any, args?: any) : Observable<string> | null => of(
        moment(value).format("LLL")
    );
};