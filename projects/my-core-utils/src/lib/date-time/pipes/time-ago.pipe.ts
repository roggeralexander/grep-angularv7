import { 
  ChangeDetectorRef, 
  Inject, 
  NgZone, 
  OnDestroy, 
  PLATFORM_ID, 
  Pipe, 
  PipeTransform
} from "@angular/core";

import { 
  isPlatformServer
} from "@angular/common"; 

import { 
  Observable, 
  of
} from "rxjs"; 

// Utils 
import { 
  getDiffBetweenDates
} from "../utils";

@Pipe({
    name: "mcTimeAgo", 
    pure: false
})
export class UtilsTimeAgoPipe implements PipeTransform, OnDestroy {
  // Propriedade para referenciar o timer
  protected _timer : number = null; 

  constructor(
    protected _changeDetector : ChangeDetectorRef, 
    @Inject(PLATFORM_ID) protected _platformId : Object, 
    protected _ngZone : NgZone
  ) { };

  ngOnDestroy() {
    this._removeTimer(); 
  };

  protected _removeTimer = () : void => {
    if(!isPlatformServer(this._platformId) && this._timer) {
      clearTimeout(this._timer); 
      this._timer = null; 
    }
  };

  transform = (value: any, args?: any) : Observable<string | null> => {
    if(isPlatformServer(this._platformId)) {
      return of(null); 
    }

    const start = value;
    let unit : "days" | "hours" | "minutes" | "seconds" = "days";
    let diffValue : string = getDiffBetweenDates(
      start, 
      Date.now(), 
      unit
    );

    while(diffValue === null) {
      switch(unit) {
        case "days": 
          unit = "hours";
          break;
        case "hours": 
          unit = "minutes";
          break;
        case "minutes": 
          unit = "seconds";
          break;
      }

      diffValue = getDiffBetweenDates(
        start, 
        Date.now(), 
        unit
      );
    }

    // Seteando o timer para poder realizar a atualização do time ago
    this._timer = this._ngZone.runOutsideAngular(() => window.setTimeout(() => {
      this._ngZone.run(() => this._changeDetector.markForCheck())
    }, 30000));

    return of(
      diffValue
    );
  };
};