import { 
    Pipe, 
    PipeTransform
} from "@angular/core";

import { 
    Observable, 
    of
} from "rxjs"; 

// Utils 
import { 
    formattedDuration
} from "../utils"; 

@Pipe({
    name: "mcFormatDuration"
})
export class UtilsFormatDurationPipe implements PipeTransform {
    transform = (value : any, args?: any) : Observable<string> | null => of(
        formattedDuration(value)
    );
};