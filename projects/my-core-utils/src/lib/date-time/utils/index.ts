// Third party libraries 
import moment from "moment";

/**
 * Método para gerar a duração formatada
 * @param duration
 * @param format 
 */
export const formattedDuration = (duration : number, format?: string) : string => {
    let strFormat = "";
    let hours = 0, minutes = 0, seconds = 0;
  
    hours = Math.floor(duration / 3600);
  
    if(hours > 0) {
        if(hours > 9 ) {
            strFormat += `${ hours }:`;
        } else if(hours > 0 && hours <= 9) {
            strFormat += `0${ hours }:`;
        }
  
        minutes = Math.floor((duration - hours * 3600) / 60);
  
        seconds = Math.ceil((duration - hours * 3600) - minutes * 60);
    } else {
        minutes = Math.floor(duration / 60);
  
        if(minutes > 0) {
            seconds = Math.ceil(duration % 60);            
        } else {
            seconds = Math.ceil(duration);
        }
  
        if(format !== undefined && format === "full") {
            strFormat += "00:";
        }
    }
  
    if(minutes > 9) {
        strFormat += `${ minutes }:`;
    } else {
        strFormat += `0${ minutes }:`;
    }
  
    if(seconds > 9) {
        strFormat += seconds;
    } else {
        strFormat += `0${ seconds }`;
    }
  
    return strFormat;
};
  
/**
 * Método para retornar os meses
 */
export const getMonths = () : string[] => moment.months();

/**
 * Método para obter o total de dias de um mês em um determinado ano
 * @param year 
 * @param month 
 */
export const totalDaysInMonth = (year : number, month : number) : number => moment(`${ year }-${ month }`, "YYYY-MM").daysInMonth();

/**
 * Método para retornar uma data em formato Date
 * @param value 
 */
export const getDate = (value : Date | number | string) : Date => {
    let date : Date = null;

    switch(value.constructor.name) {
        case "Date": 
            date = <Date>value;
            break;
        default: 
            date = moment(value).toDate();
            break;
    }

    return date;
};

/**
 * Método para conferir se a data é correta ou não de acordo a idades mínima e máxima
 * @param date 
 * @param minAge 
 * @param maxAge 
 */
export const isDateOkByAges = (date : number | string | Date, minAge : number = 0, maxAge : number = 100) : boolean => {
    if(date === undefined || !date || minAge < 0 || maxAge <= 0 || minAge >= maxAge) {
        return false;
    }

    let a = moment(date);
    let b = moment();
    let years = Math.abs(
        a.diff(
            b, 
            "year"
        )
    );
    
    return years >= minAge && years <= maxAge;
};

/**
 * Método para calcular a diferença entre duas datas, dependendo do unit informado
 * @param start 
 * @param end 
 * @param unit 
 */
export const getDiffBetweenDates = (start : Date | string | number, end : Date | string | number = Date.now(), unit: "days" | "hours" | "minutes" | "seconds" = "days") : string => {
    const startMoment = moment(start);
    const endMoment = moment(end);
    const diff = startMoment.diff(endMoment);
    const duration : any = moment.duration(diff);

    let durationAbs : number = null;
    let returnHumanize : boolean = false;
    let returnFormatted : boolean = false;

    switch(unit) {
        case "days": 
            durationAbs = Math.abs(duration.asDays());
    
            if(durationAbs < 1) {
                return null;
            } else if(durationAbs >= 1 && durationAbs < 7) {
                returnHumanize = true;
            } else {
                returnFormatted = true;
            }

            break;
        case "hours": 
            durationAbs = Math.abs(duration.asHours());
    
            if(durationAbs < 1) {
                return null;
            } else if(durationAbs >= 1 && durationAbs < 24) {
                returnHumanize = true;
            } 

            break; 
        case "minutes": 
            durationAbs = Math.abs(duration.asMinutes());
    
            if(durationAbs < 1) {
                return null;
            } else if(durationAbs >= 1 && durationAbs < 60) {
                returnHumanize = true;
            } 

            break;
        case "seconds": 
            returnHumanize = true;
            break;
    }

    if(returnHumanize) {
        return duration.humanize(
            true
        );
    }

    if(returnFormatted) {
        return startMoment.format(
            "DD MMM - HH:mm"
        );
    }

    return null;
};

/**
 * Método para especificar se uma data se encontra dentro de um intervalo 
 * @param date 
 * @param minDate 
 * @param maxDate
 */
export const isBetweenDates = (date : Date | number | string, minDate : Date | number | string, maxDate : Date | number | string) : boolean => {
    let isOk : boolean = false;

    if(date === undefined || date === null) {
        return false;
    }

    let currentDate : number = moment(date).toDate().getTime();
    let min : number = null;
    let max : number = null;

    if(minDate !== null || maxDate !== null) {
        if(minDate !== null) {
            min = moment(minDate).toDate().getTime();
        }

        if(maxDate !== null) {
            max = moment(maxDate).toDate().getTime();
        }

        if(min === null) {
            isOk = currentDate <= max;
        } else if(max === null) {
            isOk = currentDate >= min;
        } else {
            isOk = currentDate >= min && currentDate <= max;
        }
    }

    return isOk;
};