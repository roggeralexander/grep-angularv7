import {
    isPlatformServer
} from "@angular/common";

import { 
    from, 
    Observable, 
    of
} from "rxjs"; 

import { 
    catchError, 
    switchMap
} from "rxjs/operators"; 

// Models 
import { 
    ConfigUM
} from "../models";

/**
 * Método para obter o get stream da web cam
 * @param platformId 
 * @param configUM
 */
export const getUMStream = (platformId : Object, configUM : ConfigUM) : Observable<any> => {
    if(isPlatformServer(platformId) || configUM === undefined || !configUM || !Object.keys(configUM).length) {
        return of(undefined);
    }

    let n = <any>navigator; 

    n.getUserMedia = n.getUserMedia || 
    n.webkitGetUserMedia || 
    n.mozGetUserMedia || 
    n.msGetUserMedia || 
    n.oGetUserMedia;

    if(n.getUserMedia === undefined || !n.getUserMedia || n.mediaDevices === undefined || !n.mediaDevices || n.mediaDevices.getUserMedia === undefined || !n.mediaDevices.getUserMedia) {   
        return of(
            undefined
        ); 
    }

    return from(
        n.mediaDevices.getUserMedia(
            configUM
        )
    ).pipe(
        switchMap((stream : any) => of(stream))
    );
};