/**
 * Interface para representar a configuração user-media para uso da web cam
 */
export interface ConfigUM {
    video: boolean; 
    audio: boolean;
};

/**
 * Interface para referenciar as configurações da câmera para capturar a imagem
 */
export class CameraConfig {
    public mimeType?: string; 
    public extension?: string; 
    public quality?: number;
};

/**
 * Interface para referenciar o Output da Câmera quando tirara uma foto
 */
export class CaptureOutput {
    // Emissão do item gerado em formato base64
    public base64? : string | null;
 
    // Emissão do item gerado em formato File
    public file?: File | null;

    // Indicar o tipo de item gerado
    public type?: "image" | "audio" | "video";
};