import { 
    isColorOk
} from "../../patterns";

/**
 * Método para converter uma cor hexadecimal to rgba com o seu respectivo opacity
 * @param hex 
 * @param opacity
 */
export const hexToRgba = (hex : string, opacity : number = 1) : string => {
    if(!isColorOk(hex)) {
        return null;
    }

    let c : any = hex.substring(1).split(" ");

    if(c.length== 3){
        c= [c[0], c[0], c[1], c[1], c[2], c[2]];
    }

    c = '0x'+c.join('');

    return 'rgba('+[(c>>16)&255, (c>>8)&255, c&255].join(',')+`,${ opacity })`;
};