## My Core Storage Module 
Versão de Angular: 7

### Importing: 
Adicionar o MyCoreStorageModule.forRoot({ "name": ${ dbName } }) no AppModule, indicando qual será o nome do banco de dados usado como cache.
```
import { 
    NgModule 
} from '@angular/core';

import { 
    MyCoreStorageModule
} from '@my-core/storage';

@NgModule({
    imports: [
        ...
        MyCoreStorageModule.forRoot({
            "name": "ngforage"
        })
    ],
    declarations: [
        ...
    ],
    exports: [
        ...
    ],
    providers: [
        ...
    ]
})
export class AppModule {
}
```

## API - Storage - Service
### Métodos
|  Nome                  | Parâmetros      | Tipo de Retorno      | Description     |
| ---------------------- | ---------- | --------------- | --------------- |
| `getItems`    | `key:string` | `Observable<any> | null` | Retorna o(s) item(s) que estão associados a uma determinada key |
| `setItems`    | `key:string` | `Observable<any> | null` | Associa o(s) item(ns) a ser(em) associado(s) a uma determinada chave, retornando o(s) item(ns) após a sua associação |
| `removeCollection`    | `key:string` | `Observable<any> | null` | Associa o(s) item(ns) a ser(em) associado(s) a uma determinada chave, retornando o(s) item(ns) após a sua associação |
| `addItemToCollection`    | `key : string, item : any, addOn : "first" | "last", primaryKey? : string, maxCollectionLength? : number` | `Observable<any> | null` | Permite adicionar um item à collection obtida a partir do key informado, indicando o conteúdo do item, a posição do item (primeiro ou último lugar). Se o item corresponder a um objeto, deverá ser especificado o primaryKey. Caso a collection tenha um tamanho máximo de items, deverá ser especificado o valor do parâmetro maxCollectionLength |
| `removeItemFromCollectionByValue`    | `key : string, toDelete : { key: string, value : any } | string` | `Observable<any> | null` | Permite realizar a remoção de um item de uma collection a partir do seu valor quando for string; quando for objeto, deverá ser especificado o valor |
| `removeItemFromCollectionByIndex`    | `key : string, index : number` | `Observable<any> | null` | Permite remover um item de uma collection a partir da sua posição no item |
| `clearDB`    | "" | `Observable<void> | null` | Remove o banco de dados que foi gerado a partir do nome informado na importação do módulo. |
| `keysLength`    | "" | `Observable<number> | null` | Obtém o número total de keys que estão associadas ao banco de dados de cache |
| `getKeyByIndex`    | "" | `Observable<string> | null` | Obtém o key a partir do índice informado |
| `getAllKeys`    | "" | `Observable<string[]> | null` | Obtém a lista de keys associadas a um banco de dados |