/*
 * Public API Surface of my-core-storage
 */

// Services 
export * from "./lib/services/storage.service";

// Módulos
export * from './lib/my-core-storage.module';
