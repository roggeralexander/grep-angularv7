import { 
  ModuleWithProviders, 
  NgModule 
} from '@angular/core';

import { 
  CommonModule
} from "@angular/common"; 

// Third party modules 
import { 
  NgForageModule
} from "ngforage";

// Configs & Tokens 
import { 
  STORAGE_NAME
} from "./configs";

// Constants 
import { 
  DEFAULT_SN
} from "./constants";

// Services
import { 
  StorageService
} from "./services/storage.service";

@NgModule({
  imports: [
    CommonModule, 
    NgForageModule
  ]
})
export class MyCoreStorageModule { 
  static forRoot(configs?: Partial<any>) : ModuleWithProviders {
    return {
      ngModule: MyCoreStorageModule, 
      providers: [
        {
          provide: STORAGE_NAME, 
          useValue: configs === undefined || !configs || !Object.keys(configs).length || configs["name"] === undefined || !configs["name"] || !configs["name"].trim().length ?   
            DEFAULT_SN : 
            configs["name"] 
        }, 
        StorageService
      ]
    }
  };
};
