import { 
  Inject, 
  Injectable, 
  PLATFORM_ID
} from "@angular/core";

import { 
  isPlatformServer
} from "@angular/common";

import {
  NgForage, 
  NgForageCache, 
  NgForageConfig
} from "ngforage";

import { 
  from, 
  Observable
} from "rxjs"; 

import { 
  switchMap
} from "rxjs/operators";

// Configs & Tokens 
import { 
  STORAGE_NAME
} from "../configs";

@Injectable()
export class StorageService {
  constructor(
    protected readonly _cache : NgForageCache, 
    protected readonly _ngf : NgForage, 
    ngfConfig : NgForageConfig, 
    @Inject(PLATFORM_ID) protected _platformId : Object, 
    @Inject(STORAGE_NAME) storageName : string
  ) { 
    if(isPlatformServer(this._platformId)) {
      return ; 
    }

    ngfConfig.configure({
      name: storageName, 
      driver: [
        NgForageConfig.DRIVER_INDEXEDDB, 
        NgForageConfig.DRIVER_LOCALSTORAGE, 
        NgForageConfig.DRIVER_WEBSQL, 
        NgForageConfig.DRIVER_SESSIONSTORAGE
      ]
    });
  };

  /**
   * Método para obtenção de itens a partir do key
   * @param key
   */
  public getItems = (key : string) : Observable<any> | null => {
    if(isPlatformServer(this._platformId)) {
      return null; 
    }

    return from(
      this._ngf.getItem(
        key
      )
    );
  };

  /**
   * Método para setear os itens para um determinado key 
   * @param key 
   * @param value
   */
  public setItems = (key: string, value : any) : Observable<any> | null => {
    if(isPlatformServer(this._platformId)) {
      return null;
    }

    return from(
      this._ngf.setItem(
        key, 
        value
      )
    );
  };

  /**
    * Método para remover uma collection a partir do seu key
    * @param key
    */
  public removeCollection = (key : string) : Observable<void> | null => {
    if(isPlatformServer(this._platformId)) {
      return null;
    }

    return from(
      this._ngf.removeItem(key)
    );
  };

  /**
    * Método para adicionar um item a uma collection 
    * @param key 
    * @param item 
    * @param addOn 
    * @param primaryKey 
    * @param maxCollectionLength
    */
  public addItemToCollection = (key : string, item : any, addOn : "first" | "last" = "last", primaryKey?: string, maxCollectionLength? : number) : Observable<any> | null => {
    if(isPlatformServer(this._platformId)) {
      return null;
    } 

    return this.getItems(key).pipe(
      switchMap(results => {
        if(results === undefined || !results || !results.length) {
          results = [ item ];
        } else {
          let index = results.findIndex(obj => {
            if(primaryKey !== undefined && primaryKey && primaryKey.trim().length > 0) {
              return obj[primaryKey] === item[primaryKey];
            } else {
              return obj === item;
            }
          });

          if(index !== -1) {
            results.splice(
              index, 
              1
          );
        } 

        switch(addOn) {
          case "first":
            results.unshift(item);

            if(maxCollectionLength !== undefined && maxCollectionLength && maxCollectionLength > 1) {
              if(results.length > maxCollectionLength) {
                results.pop();
              }
            }                                                    
            
            break; 
          case "last":
            results.push(item);

            if(maxCollectionLength !== undefined && maxCollectionLength && maxCollectionLength > 1) {
              if(results.length > maxCollectionLength) {
                results.shift();
              }
            }                
              
            break;
          }

          return this.setItems(
            key, 
            results
          );
        }        
      })
    );
  };

  /**
   * Método para remover items de uma collection pelo value 
   * @param key 
   * @param toDelete ({ key, value })
   */
  public removeItemFromCollectionByValue = (key : string, toDelete: { key: string, value: any } | string ) : Observable<any> | null => {
    if(isPlatformServer(this._platformId)) {
      return null; 
    }

    return this.getItems(key).pipe(
      switchMap(results => {
        if(results) {
          let index : number = results.findIndex(obj => {
            if(typeof toDelete === "string") {
              return obj === toDelete;
            } else {
              return obj[toDelete.key] === toDelete.value;
            }
          });

          if(index !== -1) {
            results.splice(
              index, 
              1
            );
          } 
        } 

        return this.setItems(
          key, 
          results
        );
      })
    );
  };

  /**
   * Método para remover um item pelo key collection e o seu index 
   * @param key 
   * @param index
   */
  public removeItemFromCollectionByIndex = (key : string, index : number) : Observable<any> | null => {
    if(isPlatformServer(this._platformId)) {
      return ; 
    }

    return this.getItems(key).pipe(
      switchMap(results => {
        if(results) {
          results.splice(
            index, 
            1
          );
        }

        return this.setItems(
          key, 
          results
        );
      })
    );
  };

  /**
   * Método para limpar o database
   */
  public clearDB = () : Observable<void> | null => {
    if(isPlatformServer(this._platformId)) {
      return null;
    }

    return from(
      this._ngf.clear()
    );
  };

  /**
   * Método para obter o total de keys do database
   */
  public keysLength = () : Observable<number> | null => {
    if(isPlatformServer(this._platformId)) {
      return null;
    }

    return from(
      this._ngf.length()
    );
  };

  /**
   * Método para obter o key de um database a partir do index 
   * @param index
   */
  public getKeyByIndex = (index : number) : Observable<string> | null => {
    if(isPlatformServer(this._platformId)) {
      return null;
    }

    return from(
      this._ngf.key(index)
    );
  };

  /**
   * Método para obter todos os nomes das collections do database 
   */
  public getAllKeys = () : Observable<string[]> | null => {
    if(isPlatformServer(this._platformId)) {
      return null;
    }

    return from(
      this._ngf.keys()
    );
  };
};
