import { 
    InjectionToken
} from "@angular/core";

export const STORAGE_NAME = new InjectionToken<string>("./configs/index");