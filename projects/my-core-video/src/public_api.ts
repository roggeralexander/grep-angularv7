/*
 * Public API Surface of my-core-video
 */

// Submodules 
export * from "./lib/capture/index";
export * from "./lib/player/common/index"; 
export * from "./lib/player/embedded/index";

// Módulo principal  
export * from "./lib/my-core-video.module"; 