// Componentes
export { 
    VideoComponent as VideoCaptureComponent
} from "./components/video/video.component";

// Módulo 
export {
    CaptureModule as VideoCaptureModule
} from "./capture.module"; 