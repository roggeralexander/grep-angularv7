import { 
  ChangeDetectionStrategy, 
  ChangeDetectorRef, 
  Component,
  ElementRef, 
  EventEmitter, 
  Inject,  
  OnDestroy, 
  OnInit, 
  Output, 
  PLATFORM_ID, 
  Renderer2, 
  ViewChild
} from '@angular/core';

import { 
  isPlatformServer
} from "@angular/common"; 

import {
  fromEvent, 
  Observable,  
  of, 
  Subscription, 
  throwError, 
  timer
} from "rxjs"; 

import { 
  catchError, 
  switchMap
} from "rxjs/operators"; 

// Angular CDK Modules 
import { 
  ViewportRuler
} from "@angular/cdk/scrolling"; 

// Third party modules 
import MediaStreamRecorder from "msr";

import moment from "moment"; 

import uuid from "uuid"; 

// Toolbar component 
import { 
  ToolbarComponent
} from "../toolbar/toolbar.component"; 

// Constants 
import { 
  DEFAULT_CONFIGS
} from "../../constants"; 

// Models 
import { 
  CaptureOutput
} from "@my-core/utils"; 

// Utilities 
import { 
  blobToFile,
  getUMStream, 
  saveFileFromBlob
} from "@my-core/utils";

@Component({
  selector: 'mc-video-capture',
  templateUrl: './video.component.html',
  styleUrls: ['./video.component.scss'],
  changeDetection: ChangeDetectionStrategy.Default
})
export class VideoComponent implements OnInit, OnDestroy {
  // Instância do arquivo gerado quando tirada uma imagem 
  public file : File = null; 

  // Propriedade para conferir se o capture está permitido ou não 
  public allowed : boolean = false;

  // Propriedade para referenciar o tempo de gravação da mídia 
  public timeRecorded : string = "00:00";

  // Propriedade para referenciar se está sendo gravado ou não 
  public isRecording : boolean = false;

  // Id do component 
  protected _id : string = "mc-video-capture-" + uuid.v1().replace(
    new RegExp("-", "gi"), 
    ""
  );

  // Propriedade para referenciar ao input html 
  protected _inputHtml : HTMLVideoElement = null; 
  protected _outputHtml : HTMLVideoElement = null; 

  // Propriedade quando o media stream recorder gerar um novo arquivo 
  protected _blob : Blob = null; 

  // Propriedade para referenciar ao media recorder
  protected _mediaRecorder : MediaStreamRecorder = null;

  // Propriedade para referenciar o media stream 
  protected _stream : MediaStream = null;

  // Propriedade para especificar se o vídeo está display block ou none
  protected _displayVideo : boolean = false;

  // Propriedade para referenciar o timer 
  protected _timer : any = null; 

  // Subscriptions 
  protected _subs : Subscription[] = [];

  // Video input - view child 
  @ViewChild(
    "videoInput", 
    { read : ElementRef }
  ) protected _inputRef : ElementRef = null; 

  @ViewChild(
    "videoOutput", 
    { read : ElementRef }
  ) protected _outputRef : ElementRef = null; 

  // View child - Toolbar component 
  @ViewChild(
    ToolbarComponent
  ) protected _toolbar : ToolbarComponent = null; 

  // Outputs 
  @Output() 
  public ready : EventEmitter<void> = new EventEmitter();

  @Output() 
  public output : EventEmitter<CaptureOutput> = new EventEmitter();  

  constructor(
    protected _changeDetector : ChangeDetectorRef, 
    protected _elmRef : ElementRef, 
    @Inject(PLATFORM_ID) protected _platformId : Object, 
    protected _renderer : Renderer2, 
    protected _ruler : ViewportRuler
  ) { };

  ngOnInit() {
    if(isPlatformServer(this._platformId)) {
      return ;
    }

    // Subscripion para inicializar o component 
    this._subs.push(
      timer(100).pipe(
        switchMap(() => this._initialize()), 
        catchError((err : Error) => {
          console.log("Video web capture error", err); 
          return of(
            undefined
          );
        })
      ).subscribe(() => { 
        console.log("Video capture component inicializado"); 
      })
    )
  };

  ngOnDestroy() {
    this._subs.forEach((sub : Subscription) => {
      sub.unsubscribe();
    });

    this.ready.complete(); 
    this.output.complete();
  };

  /**
   * Método para inicializar o component 
   */
  protected _initialize = () : Observable<void> => getUMStream(
    this._platformId, 
    {
      audio: true, 
      video: true
    }
  ).pipe(
    switchMap((stream : MediaStream) => {
      if(stream === undefined || !stream) {
        return throwError(
          new Error(
            "Web cam is not allowed to be used"
          )
        );
      }

      this.allowed = true;

      this._renderer.setAttribute(
        this._elmRef.nativeElement, 
        "id", 
        this._id
      );

      this._stream = stream;

      this._changeDetector.detectChanges();

      return this._setElements();
    }),
    switchMap(() => this._declareToolbarListeners())
  );

  /**
   * Método para setear os elementos do component
   */
  protected _setElements = () : Observable<void> => {
    if(this._inputRef === undefined || !this._inputRef || this._outputRef === undefined || !this._outputRef) {
      return throwError(
        new Error(
          "Video input | output reference não existe!"
        )
      );
    }

    this._inputHtml = <HTMLVideoElement>this._inputRef.nativeElement;
    this._outputHtml = <HTMLVideoElement>this._outputRef.nativeElement;

    // Declarando os events para o video input

    this._subs.push(
      fromEvent(
        this._inputHtml, 
        "canplay"
      ).subscribe(() => {
        this._inputHtml.volume = 0; 
        this.isRecording = false; 

        this._changeDetector.markForCheck();
      })  
    );

    // Vinculando o stream ao video 
    this._inputHtml.srcObject = this._stream;

    return timer(25).pipe(
      switchMap(() => {
        this._toolbar.allowBtnCancel.next(false); 
        this._toolbar.allowBtnDownload.next(false); 
        this._toolbar.allowBtnRecord.next(true);
        this._toolbar.allowBtnSave.next(false);
        this._toolbar.allowBtnStop.next(false);

        this._inputHtml.play();
        this._changeDetector.markForCheck(); 

        this.ready.emit();

        return of(
          undefined
        );
      })
    );
  };

  /**
   * Método para declearar os listeners do toolbar
   */
  protected _declareToolbarListeners = () : Observable<void> => {
    // Subscription para realizar a recording de um áudio 
    this._subs.push(
      this._toolbar.record.pipe(
        switchMap(() => this._onRecord())
      ).subscribe(() => {
        console.log("Gravando o vídeo do video capture component");
      })
    );

    // Subscription para parar a reprodução 
    this._subs.push(
      this._toolbar.stop.pipe(
        switchMap(() => this._onStop())
      ).subscribe(() => {
        console.log("Parando o recording do vídeo capture component");
      })
    );

    // Subscription para cancelar a reprodução
    this._subs.push(
      this._toolbar.cancel.pipe(
        switchMap(() => this._onCancel())
      ).subscribe(() => {
        console.log("Cancelando a gravação atual do video capture component");
      })
    );

    // Subscription para salvar o arquivo 
    this._subs.push(
      this._toolbar.save.pipe(
        switchMap(() => this._onSave())
      ).subscribe(() => {
        console.log("Salvando o vídeo do video capture component")
      })
    );

    // Subscription para realizar o download do arquivo 
    this._subs.push(
      this._toolbar.download.pipe(
        switchMap(() => this._onDownload())
      ).subscribe(() => {
        console.log("Realizando o download do arquivo do video capture component"); 
      })
    );

    return timer(25).pipe(
      switchMap(() => of(
        undefined
      ))
    );
  };

  /**
   * Método para cancelar a operação
   */
  protected _onCancel = () : Observable<void> => {
    this._showOutput(false);

    this._blob = null;
    this.isRecording = false;

    // Ativando os botões do toolbar 
    this._toolbar.allowBtnCancel.next(false);
    this._toolbar.allowBtnDownload.next(false);
    this._toolbar.allowBtnRecord.next(true);
    this._toolbar.allowBtnSave.next(false);
    this._toolbar.allowBtnStop.next(false);

    return of(
      undefined
    );
  };

  /**
   * Método para realizar o download do vídeo
   */
  protected _onDownload = () : Observable<void> => {
    if(this._blob === undefined || !this._blob) {
      return throwError(
        new Error(
          "Blob para video capture component não existe!"
        )
      ); 
    }

    saveFileFromBlob(
      this._blob, 
      DEFAULT_CONFIGS["extension"]
    );

    return of(
      undefined
    );
  };

  /**
   * Método para gravar o vídeo
   */
  protected _onRecord = () : Observable<void> => {
    console.log("Desde o onrecord do video capture component");

    this._showOutput(false);

    this._blob = null;
    this.isRecording = true;

    // Ativando os botões do toolbar 
    this._toolbar.allowBtnCancel.next(true);
    this._toolbar.allowBtnDownload.next(false);
    this._toolbar.allowBtnRecord.next(false);
    this._toolbar.allowBtnSave.next(false);
    this._toolbar.allowBtnStop.next(true);

    // Inicializar o media recorder 
    this._mediaRecorder = new MediaStreamRecorder(
      this._stream, 
      {
        mimeType: DEFAULT_CONFIGS["mimeType"]
      }
    );

    this._inputHtml.volume = 1;

    this._mediaRecorder.ondataavailable = (blob : Blob) => {
      this._blob = blob;
      this._outputHtml.src = URL.createObjectURL(blob);
    };

    this._mediaRecorder.start();

    this._startTimer();

    return of(
      undefined
    );
  };

  /**
   * Método para solicitar que o vídeo seja gerado como File e emitido
   */
  protected _onSave = () : Observable<void> => {
    if(this._blob === undefined || !this._blob) {
      return throwError(
        new Error(
          "Blob do video capture component não existe!"
        )
      ); 
    }

    const fileName =uuid.v4().replace(
      new RegExp("-", "gi"), 
      ""
    );

    return blobToFile(
      this._blob, 
      fileName, 
      DEFAULT_CONFIGS['extension']
    ).pipe(
      switchMap((file : File) => {
        console.log("Arquivo gerado do vídeo capture component com sucesso", file);

        const obj : CaptureOutput = {
          file: this.file, 
          type: "video"
        };

        this.output.emit(
          obj
        );

        return of(
          undefined
        );
      })
    );
  };

  /**
   * Método para parar a gravação
   */
  protected _onStop = () : Observable<void> => {
    if(this._mediaRecorder) {
      this._mediaRecorder.stop();
    }

    this._mediaRecorder = null; 
    this._inputHtml.volume = 0; 
    this.isRecording = false; 
    this._stopTimer();

    this._showOutput(
      true
    );

    // Ativando os botões do toolbar 
    this._toolbar.allowBtnCancel.next(true);
    this._toolbar.allowBtnDownload.next(false);
    this._toolbar.allowBtnRecord.next(false);
    this._toolbar.allowBtnSave.next(true);
    this._toolbar.allowBtnStop.next(false);

    return of(
      undefined
    );
  };

  /**
   * Método para iniciar um timer
   */
  protected _startTimer = () : void => {
    this.timeRecorded = "00:00";
      let start = moment();

      this._timer = setInterval(() => {
        let now = moment();
        this.timeRecorded = moment(now.diff(start)).format("mm:ss");
        this._changeDetector.detectChanges();
    }, 1000);
  };

  /**
   * Método para parar o timer
   */
  protected _stopTimer = () : void => {
    if(this._timer === undefined || !this._timer) {
      return ; 
    }

    clearInterval(this._timer);
    this._timer = null;
    this.timeRecorded = "00:00";
    this._changeDetector.detectChanges();
  };

  /**
   * Método para mostrar ou não o output
   */
  protected _showOutput  = (flag : boolean) : void => {
    if(this._outputHtml === undefined || !this._outputHtml) {
      return ; 
    }

    this._renderer.setStyle(
      this._outputHtml, 
      "display", 
      flag ?  
        "block" : 
        "none"
    );

    this._changeDetector.markForCheck();
  };
};
