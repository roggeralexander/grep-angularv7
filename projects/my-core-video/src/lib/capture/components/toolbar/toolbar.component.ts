import { 
  ChangeDetectionStrategy, 
  ChangeDetectorRef, 
  Component, 
  Inject, 
  OnDestroy, 
  OnInit, 
  PLATFORM_ID 
} from '@angular/core';

import { 
  isPlatformServer 
} from '@angular/common';

import { 
  Subscription, 
  Subject
} from "rxjs";

@Component({
  selector: 'mc-video-toolbar-capture',
  templateUrl: './toolbar.component.html',
  styleUrls: ['./toolbar.component.scss'], 
  changeDetection: ChangeDetectionStrategy.Default
})
export class ToolbarComponent implements OnInit, OnDestroy {
  // Subject para cancelar uma operação 
  public readonly cancel : Subject<void> = new Subject();

  // Subject para solicitar que seja gravado o áudio
  public readonly record : Subject<void> = new Subject(); 

  // Subject para solicitar que seja parada a gravação do áudio
  public readonly stop : Subject<void> = new Subject();

  // Subject para solicitar que o arquivo seja salvo 
  public readonly save : Subject<void> = new Subject();

  // Subject para solicitar que realizar o download do arquivo 
  public readonly download : Subject<void> = new Subject();

  // Subject para habilitar o botão take picture 
  public readonly allowBtnRecord : Subject<boolean> = new Subject();

  // Subject para habilitar o botão edit 
  public readonly allowBtnStop : Subject<boolean> = new Subject(); 

  // Subject para habilitar o botão edit 
  public readonly allowBtnDownload : Subject<boolean> = new Subject(); 

  // Subject para habilitar o botão cancel 
  public readonly allowBtnCancel : Subject<boolean> = new Subject(); 

  // Subject para habilitar o botão save 
  public readonly allowBtnSave : Subject<boolean> = new Subject(); 

  // Propriedade para habilitar o botão cancelar 
  public btnCancelAllowed : boolean = false; 

  // Propriedade para habilitar o botão save 
  public btnSaveAllowed : boolean = false; 

  // Propriedade para habilitar o botão edit 
  public btnRecordAllowed : boolean = false; 

  // Propriedade para habilitar o botão take 
  public btnStopAllowed : boolean = false; 

  // Propriedade para habilitar o botão take 
  public btnDownloadAllowed : boolean = false; 

  // Subscription 
  protected _subs : Subscription[] = [];

  constructor(
    protected _changeDetector : ChangeDetectorRef, 
    @Inject(PLATFORM_ID) protected _platformId : Object
  ) { };

  ngOnInit() {
    if(isPlatformServer(this._platformId)) {
      return ;
    }

    // Subscription para permitir o botão save 
    this._subs.push(
      this.allowBtnSave.subscribe((flag : boolean) => {
        console.log("Habilitar o botão save", flag);

        this.btnSaveAllowed = flag; 
        this._changeDetector.markForCheck();
      })
    );

    // Subscription para permitir o botão edit 
    this._subs.push(
      this.allowBtnRecord.subscribe((flag : boolean) => {
        console.log("Habilitar o botão record", flag);

        this.btnRecordAllowed = flag; 
        this._changeDetector.markForCheck();
      })
    );

    // Subscription para permitir o botão take 
    this._subs.push(
      this.allowBtnStop.subscribe((flag : boolean) => {
        console.log("Habilitar o botão stop", flag);

        this.btnStopAllowed = flag; 
        this._changeDetector.markForCheck();
      })
    );

    // Subscription para permitir o botão cancel 
    this._subs.push(
      this.allowBtnCancel.subscribe((flag : boolean) => {
        console.log("Habilitar o botão cancel", flag);

        this.btnCancelAllowed = flag; 
        this._changeDetector.markForCheck();
      })
    );

    // Subscription para permitir o botão download 
    this._subs.push(
      this.allowBtnDownload.subscribe((flag : boolean) => {
        console.log("Habilitar o botão download", flag);

        this.btnDownloadAllowed = flag; 
        this._changeDetector.markForCheck();
      })
    );
  };

  ngOnDestroy() {
    this._subs.forEach((sub : Subscription) => {
      sub.unsubscribe();
    });
  };

  /**
   * Método para aplicar uma operação 
   * @param op
   */
  public applyOperation = (op : string) : void => {
    if(isPlatformServer(this._platformId) || op === undefined || op === null || this[op] === undefined || !this[op]) {
      return ; 
    }

    console.log("Operação solicitada desde o toolbar para o video capture", op);

    setTimeout(() => {
      this[op].next();
    });
  };
};
