import { 
    CameraConfig
} from "@my-core/utils"; 

export const DEFAULT_CONFIGS : CameraConfig = {
    mimeType: "video/webm",
    extension: "webm"
};