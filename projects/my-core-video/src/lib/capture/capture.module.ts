import { 
    NgModule
} from "@angular/core"; 

import { 
    CommonModule
} from "@angular/common";

// Angular CDK Modules 
import { 
    ScrollDispatchModule
} from "@angular/cdk/scrolling"; 

// Components
import { 
    VideoComponent 
} from './components/video/video.component';

import { 
    ToolbarComponent 
} from './components/toolbar/toolbar.component'; 

@NgModule({
    imports: [
        CommonModule, 
        ScrollDispatchModule
    ], 
    declarations: [
        VideoComponent, 
        ToolbarComponent
    ], 
    exports: [
        VideoComponent
    ], 
    entryComponents: [
        VideoComponent
    ]
})  
export class CaptureModule { };