import { 
    NgModule
} from "@angular/core"; 

import { 
    CommonModule
} from "@angular/common"; 

// Components 
import {
    PlayerComponent
} from "./components/player/player.component"; 

@NgModule({
    imports: [
        CommonModule
    ], 
    declarations: [
        PlayerComponent
    ], 
    exports: [
        PlayerComponent
    ], 
    entryComponents: [
        PlayerComponent
    ]
})
export class CommonPlayerModule { };