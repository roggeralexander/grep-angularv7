// Components 
export {
    PlayerComponent as CommonVideoPlayerComponent
} from "./components/player/player.component";

// Modules 
export { 
    CommonPlayerModule as CommonVideoPlayerModule
} from "./common-player.module";