import { 
  AfterViewInit, 
  ChangeDetectionStrategy, 
  ChangeDetectorRef, 
  Component,
  ElementRef, 
  EventEmitter, 
  Inject, 
  Input, 
  OnDestroy, 
  PLATFORM_ID, 
  Output, 
  Renderer2 
} from '@angular/core';

import { 
  isPlatformServer
} from "@angular/common"; 

import {
  from,  
  fromEvent, 
  Observable, 
  of, 
  Subscription, 
  throwError, 
  timer,
} from "rxjs"; 

import { 
  catchError, 
  switchMap, 
  take
} from "rxjs/operators"; 

// Third party modules 
import uuid from "uuid";

@Component({
  selector: 'mc-video-player',
  templateUrl: './player.component.html',
  styleUrls: ['./player.component.scss'], 
  changeDetection: ChangeDetectionStrategy.Default
})
export class PlayerComponent implements AfterViewInit, OnDestroy {
  @Input() 
  get src() : string {
    return this._src;
  };
  set src(src : string) {
    if(src === undefined || !src || !src.trim().length || this._src === src) {
      return ; 
    }

    this._src = src; 
  };
  protected _src : string = null; 

  @Input() 
  get allowFS() : boolean {
    return this._allowFS;
  };
  set allowFS(flag : boolean) {
    if(flag === undefined || flag === null || this._allowFS === flag) {
      return ; 
    }

    this._allowFS = flag;
  };
  protected _allowFS : boolean = false;

  @Input()
  get allowDownload() : boolean {
    return this._allowDownload;
  };
  set allowDownload(flag : boolean) {
    if(flag === undefined || flag === null || this._allowDownload === flag) {
      return ; 
    }

    this._allowDownload = flag;
  };
  protected _allowDownload : boolean = false; 

  // Propriedade para referenciar o id 
  protected readonly _id : string = "mc-video-player-" + uuid.v4().replace(
    new RegExp("-", "gi"), 
    ""
  );

  // Propriedade para indicar que o vídeo foi inicializado e está pronto para uso 
  protected _isReady : boolean = false; 

  // Video Element Player 
  protected _player : HTMLVideoElement = null; 

  // Subscriptions 
  protected _subs : Subscription[] = [];

  // Output que emite quando o player está pronto para uso
  @Output() 
  public ready : EventEmitter<void> = new EventEmitter();

  // Output para indicar que a reprodução do player foi finalizada
  @Output() 
  public ended : EventEmitter<void> = new EventEmitter();

  // Output para indicar que a reprodução está ou não em andamento
  @Output() 
  public playing : EventEmitter<boolean> = new EventEmitter(); 

  // Output para indicar o tempo de reprodução atual
  @Output() 
  public currentTime : EventEmitter<number> = new EventEmitter();

  // Output para emitir o tempo de duração do vídeo 
  @Output() 
  public duration : EventEmitter<number> = new EventEmitter();

  constructor(
    protected _changeDetector : ChangeDetectorRef, 
    protected _elmRef : ElementRef, 
    @Inject(PLATFORM_ID) protected _platformId : Object, 
    protected _renderer : Renderer2
  ) { };

  ngAfterViewInit() {
    if(isPlatformServer(this._platformId)) {
      return ; 
    }

    // Subscription para inicializar o component 
    this._subs.push(
      timer(100).pipe(
        switchMap(() => this._initialize()),
        catchError((err : Error) => {
          console.log("Error na leitura do vídeo", err);
          return of(
            undefined
          );
        })
      ).subscribe(() => {
        console.log("Video component inicializado"); 
      })
    );
  };

  ngOnDestroy() {
    this._subs.forEach((sub : Subscription) => {
      sub.unsubscribe();
    });

    this.ready.complete(); 
    this.ended.complete(); 
    this.playing.complete();
    this.currentTime.complete();
    this.duration.complete();

    this._player = null; 
  };

  /**
   * Método para inicializar o component
   */
  protected _initialize = () : Observable<void> => {
    if(this._src === undefined || !this._src) {
      return throwError(
        new Error(
          "Video src não informado"
        )
      );
    }

    // Gerando o vídeo element 
    return this._createVideo().pipe(
      switchMap(() => this._declareListeners())
    );
  };

  /**
   * Método para gerar o video html element 
   */
  protected _createVideo = () : Observable<void> => {
    this._player = <HTMLVideoElement>this._renderer.createElement("video"); 

    let allowCtrls : string[] = [];

    if(!this._allowDownload) {
      allowCtrls.push("nodownload");
    }

    if(!this._allowFS) {
      allowCtrls.push("nofullscreen"); 
    }

    // Atribuindo propriedades ao video 
    const videoProperties = {
      preload: "auto", 
      oncontextmenu: "return false", 
      controlsList: allowCtrls.join(" ")
    };

    for(let [key, value] of Object.entries(videoProperties)) {
      this._renderer.setAttribute(
        this._player, 
        key, 
        value
      );
    }

    this._player.controls = true; 
    this._player.src = this._src;

    // Adicionando o video ao element ref 
    this._renderer.appendChild(
      this._elmRef.nativeElement, 
      this._player
    );

    // Seteando o element ref com o id gerado 
    this._renderer.setAttribute(
      this._elmRef.nativeElement, 
      "id", 
      this._id
    );

    this._changeDetector.markForCheck();

    return of(
      undefined
    );
  };

  /**
   * Método para declarar os listeners do vídeo 
   */
  protected _declareListeners = () : Observable<void> => {
    // Event Listener para quando o vídeo estiver pronto para uso 
    this._subs.push(
      fromEvent(
        this._player, 
        "canplay"
      ).pipe(
        take(1)
      ).subscribe(() => {
        this._isReady = true;
        this.ready.emit();

        setTimeout(() => {
          // Emitir a informação da duração do vídeo 
          this.duration.emit(
            this._player.duration
          );
        });
      })
    );

    // Event Listener para quando o video estiver sendo reproduzido 
    this._subs.push(
      fromEvent(
        this._player, 
        "play"
      ).subscribe(() => {
        this.playing.emit(true); 
      })
    );

    this._subs.push(
      fromEvent(
        this._player, 
        "playing"
      ).subscribe(() => {
        this.playing.emit(true); 
      })
    );

    // Event Listener para quando o video estiver pausado
    this._subs.push(
      fromEvent(
        this._player, 
        "pause"
      ).subscribe(() => {
        this.playing.emit(false); 
      })
    );

    // Event Listener para quando o video concluir 
    this._subs.push(
      fromEvent(
        this._player, 
        "ended"
      ).subscribe(() => {
        this.ended.emit();
        this.playing.emit(false); 
      })
    );

    // Eent listener para quando for procurado um ponto de reprodução 
    this._subs.push(
      fromEvent(
        this._player, 
        "seeking"
      ).subscribe(() => {

      })
    );

    // Event listener para quando um ponto de reprodução for escolhido 
    this._subs.push(
      fromEvent(
        this._player, 
        "seeked"
      ).subscribe(() => {

      })
    );

    // Event listener para saber qual o tempo de reprodução atual 
    this._subs.push(
      fromEvent(
        this._player, 
        "timeupdate"
      ).subscribe(() => {
        if(this._player["currentTime"] !== undefined && this._player["currentTime"] !== null) {
          this.currentTime.emit(
            this._player["currentTime"]
          );
        }
      })
    );

    return of(
      undefined
    );
  };

  /**
   * Método para reproduzir o vídeo 
   * @param flag
   */
  public play = (flag : boolean) : void => {
    if(!this._isReady) {
      return ; 
    }

    flag ? 
      this._playPlayer() : 
      this._pausePlayer();
  };

  /**
   * Método para reproduzir o vídeo do player
   */
  protected _playPlayer() : void {
    if(!this._isReady) {
      return ; 
    }

    setTimeout(() => {
      from(
        this._player.play()
      ).subscribe(
        () => console.log("show mask as false"), 
        (err : Error) => console.log("Error for playing the video", err)
      );
    });
  };

  /**
   * Método para pausar o vídeo do player
   */
  protected _pausePlayer() : void {
    if(!this._isReady) {
      return ; 
    }

    setTimeout(() => {
      this._player.pause();
    });
  };
};
