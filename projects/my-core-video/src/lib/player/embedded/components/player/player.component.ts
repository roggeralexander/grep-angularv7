import { 
  AfterViewInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component, 
  ElementRef,
  EventEmitter, 
  Inject, 
  Input, 
  OnDestroy,
  OnInit, 
  Output,
  PLATFORM_ID,
  Renderer2, 
  ViewChild 
} from '@angular/core';

import { 
  isPlatformServer
} from "@angular/common";

import { 
  Subscription 
} from 'rxjs';

// Third party
import uuid from "uuid";

// Constants 
import { 
  PLAYERS_DATA
} from "../../constants/data";

// Models 
import { 
  Player
} from "../../models/player";

// Services 
import { 
  PlayerService
} from "../../services/player.service";

import { 
  UrlService
} from "../../services/url.service";

// Utils 
import { 
  getPlayerIframeHTML
} from "../../utils/player";

@Component({
  selector: 'mc-video-player-em',
  templateUrl: './player.component.html',
  styleUrls: ['./player.component.scss'], 
  changeDetection: ChangeDetectionStrategy.Default
})
export class PlayerComponent implements OnInit, AfterViewInit, OnDestroy {
  // Input com a url do vídeo
  @Input() 
  get url() : string {
    return this._url;
  };
  set url(url : string) {
    this._url = url;
  };
  protected _url : string = null;

  // Input para especificar se o full screen será permitido no component ou não
  @Input() 
  get allowFS() : boolean {
    return this._allowFS;
  };
  set allowFS(flag : boolean) {
    this._allowFS = flag;
  };
  protected _allowFS : boolean = false;

  @ViewChild(
    "player", 
    { read: ElementRef }
  ) protected _playerRef : ElementRef = null;

  protected readonly _id : string = uuid.v4().replace(
    new RegExp("-", "gi"), 
    ""
  );

  protected _isReady : boolean = false;
  protected _player : Player = null;

  public serviceName : any = null;
  public embeddedUrl : string = null;

  // Propriedade para indicar se o player já foi gerado
  protected _playerReady : EventEmitter<any> = new EventEmitter();
  protected _playerReadySub : Subscription = Subscription.EMPTY;

  // Output quando foram seteadas todas as propriedades do player após ele ser gerado
  @Output() 
  public ready : EventEmitter<void> = new EventEmitter();

  // Output para emissão de câmbios de estado do player
  @Output() 
  public change : EventEmitter<any> = new EventEmitter();

  // Output para emitir o tempo de reprodução do vídeo 
  @Output() 
  public currentTime : EventEmitter<number> = new EventEmitter();

  // Output para emitir a duração do vídeo 
  @Output() 
  public duration : EventEmitter<number> = new EventEmitter();

  // Output para emissão de algum erro quando o vídeo está sendo carregado 
  @Output() 
  public error : EventEmitter<any> = new EventEmitter();

  constructor(
    protected _changeDetector : ChangeDetectorRef,
    @Inject(PLATFORM_ID) protected _platformId : Object, 
    protected _playerService : PlayerService, 
    protected _renderer : Renderer2, 
    protected _urlService : UrlService
  ) { 
    if(isPlatformServer(this._platformId)) {
      return ;
    }
  };

  ngOnInit() {
    if(isPlatformServer(this._platformId)) {
      return ; 
    }

    this.embeddedUrl = this._urlService.getUrlEmbeddedSync(
      this._url
    );

    // console.log(this.embeddedUrl);

    this.serviceName = Object.keys(PLAYERS_DATA).find((name : string) => this.embeddedUrl.match(
      new RegExp(
        `${ PLAYERS_DATA[name]["urlEmbedded"] }`, 
        "gi"
      )
    ) !== null);
  };

  ngAfterViewInit() {
    if(isPlatformServer(this._platformId) || this._playerRef === undefined || !this._playerRef) {
      return ; 
    }

    this._renderer.setAttribute(
      this._playerRef.nativeElement, 
      "id", 
      this._id
    );
    
    try {
      // Monitorando o service para saber a criação da api
      let managerInstance = this._playerService.getData(
        this.serviceName
      );

      if(!managerInstance["isApiCreated"]) {
        managerInstance["apiCreated"].subscribe(() => {
          this._createPlayer(
            this.embeddedUrl
          );
        });
      } else {
        this._createPlayer(
          this.embeddedUrl
        );
      } 
    } catch(err) {
      console.log(err.message);
    }
  };

  ngOnDestroy() {
    this._playerReadySub && !this._playerReadySub.closed ?
      this._playerReadySub.unsubscribe() : 
      null;

    if(this._player) {
      this._player.destroy();
      this._player = null;
    }
  };

  /**
   * Método para criar o player 
   * @param embeddedUrl
   */
  protected _createPlayer = (embeddedUrl : string) : void => {
    this._player = this._playerService.getPlayer(
      embeddedUrl
    );

    if(this._player) {
      // Seteando o player 
      this._player.setupPlayer(
        this._id,
        {
          change: this.change, 
          ready: this._playerReady, 
          error: this.error, 
          currentTime: this.currentTime, 
          duration: this.duration
        }, 
        this._allowFS
      );

      // Subscription para monitorar se o player está pronto ou não para uso
      this._playerReadySub = this._playerReady.subscribe(($event : any) => {
        // Conferir se o iframe já foi gerado - Directamente quando o service player for youtube ou dailymotion
        // Quando Video, obter o iframe dentro da div com o nome respectivo
        let playerHtmlElement = getPlayerIframeHTML(
          this.serviceName, 
          this._id
        );

        switch(this.serviceName) {
          case "vimeo": 
            this._initForVimeo(
              playerHtmlElement
            );
            break;
          case "youtube": 
            this._initForYoutube(
              playerHtmlElement
            );
            break;
          case "dailymotion": 
            this._initForDailymotion(
              playerHtmlElement
            );
            break;
        }

        // Só quando existir o player, remover os atributos width e height atribuídos 
        // quando o player foi gerado
        if(playerHtmlElement) {
          this._renderer.setAttribute(
            playerHtmlElement, 
            "width", 
            "100%"
          );

          this._renderer.setAttribute(
            playerHtmlElement, 
            "height", 
            "100%"
          );

          this._changeDetector.detectChanges();

          this._isReady = true;

          this.ready.emit();
        }
      });
    }
  };

  /**
   * Callback quando clicado os botões play / pause 
   * @param flag
   */
  public onPlay = (flag : boolean) : void => {
    if(this._isReady) {
      this._player.play(flag);
    }
  };

  /**
   * Método para adicionar funcionalidades para o playerquando o service for vimeo.
   * Neste caso, para remover a funcionalidade de full screen
   */
  protected _initForVimeo = (iframe : HTMLIFrameElement) : void => {
    try {
      if(!this._allowFS) {
        const innerDoc = iframe.contentDocument || iframe.contentWindow.document;
        const fs = innerDoc.querySelector(`button[class="fullscreen"]`);
  
        iframe.contentWindow.postMessage(
          window.location, 
          "*"
        );
        
        if(fs !== undefined && fs) {
          this._renderer.removeChild(
            innerDoc, 
            fs
          );
        }  
      }
    } catch(err) {
      console.log(err.message);
    }
  };

  /**
   * Método para adicionar funcionalidades para o playerquando o service for dailymotion.
   */
  protected _initForDailymotion = (iframe : HTMLIFrameElement) : void => {

  };

  /**
   * Método para adicionar funcionalidades para o player quando o service for youtube.
   */
  protected _initForYoutube = (iframe : HTMLIFrameElement) : void => {

  };
};
