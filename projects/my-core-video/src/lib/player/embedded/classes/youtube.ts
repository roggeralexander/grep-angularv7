import { 
    EventEmitter,
    NgZone,
} from "@angular/core";

// Interfaces
import { 
    Player, 
    PlayerOutputs,
    PlayerStatus,
} from "../models/player";

// Utils 
import { 
    YoutubePlayerRef, 
    YoutubeRef
} from "../utils/player";

/**
 * Classe para gerar uma instância do youtube player
 */
export class YoutubePlayer implements Player {
    // Interval para controle do tempo de reprodução
    public intervalTime : any = null;

    // Variável para referenciar a instância do player 
    protected _player : any = null;

    // Variável para referenciar ao id do vídeo que será carregado
    protected _videoId : string = null;

    // Variável para referenciar ao event emitter para emitir o tempo transcurrido de reprodução
    protected _currentTime : EventEmitter<number> = null;

    // Variável para referenciar ao event emitter para emitir o tempo transcurrido de reprodução
    protected _duration : EventEmitter<number> = null;

    constructor(
        protected _embeddedUrl : string, 
        protected _ngZone : NgZone, 
        protected _videoUrl : string
    ) {
        this._videoId = this._videoUrl.replace(
            new RegExp(
                this._embeddedUrl, 
                "gi"
            ), 
            ""
        );
    };

    /**
     * Método para setear o player
     * @param elmId 
     * @param outputs 
     * @param fullscreen
     */
    public setupPlayer(elmId : string, outputs : PlayerOutputs, fullscreen : boolean = false) : void {
        if(outputs.currentTime !== undefined && outputs.currentTime && outputs.currentTime instanceof EventEmitter) {
            this._currentTime = outputs.currentTime;
        }

        if(outputs.duration !== undefined && outputs.duration && outputs.duration instanceof EventEmitter) {
            this._duration = outputs.duration;
        }

        const player = YoutubePlayerRef();

        this._player = new player(
            elmId, 
            {
                videoId: this._videoId,
                playerVars: {
                    controls: 1, 
                    disablekb: 1, 
                    enablejsapi: 1, 
                    fs: !fullscreen ? 
                        0 : 
                        1,
                    iv_load_policy: 3, 
                    modestbranding: 1, 
                    origin: window.location.origin,
                    rel: 0, 
                    showinfo: 0, 
                }, 
                events: {
                    onReady: ($event : any) => {
                        this._onPlayerReady(
                            outputs.ready, 
                            outputs.error
                        );
                    }, 
                    onStateChange: ($event : any) => {
                        !this._isPlaying() ?
                            this._destroyInterval() : 
                            this._initInterval();

                        this._ngZone.run(
                            () => outputs.change && outputs.change.next($event["data"])
                        );
                    }, 
                    onError: ($event : any) => {
                        this._ngZone.run(
                            () => outputs.error && outputs.error.next($event)
                        );
                    }
                },
//                host: 'https://www.youtube.com',                                  
            }
        );

        return this._player;
    };

    /**
     * Callback quando o evento on Ready acontecer 
     * @param readyEM 
     * @param errorEM
     */
    protected _onPlayerReady = (readyEM : EventEmitter<any>, errorEM : EventEmitter<any>) : void => {
        // Emitindo indicando que o player está pronto para o seu uso
        this._ngZone.run(
            () => readyEM.next()
        );

        if(this._duration) {
            this._duration.emit(
                this._player.getDuration()
            );
        }
    };

    /**
     * Método para play ou pause do player
     * @param flag 
     */
    public play(flag : boolean) : void {
        if(this._player !== undefined && this._player) {
            if(flag) {
                this._player.playVideo();
                this._initInterval();
            } else {
                this._player.pauseVideo();
                this._destroyInterval();
            }
        }
    };

    /**
     * Método para retornar a instância do player
     */
    public getPlayer() : any | null {
        return this._player;
    };

    /**
     * Método para saber se o player está sendo reproduzido ou não
     */
    protected _isPlaying() : boolean {
        const isPlayerReady : boolean = this._player && this._player.getPlayerState;
        const playerState = isPlayerReady ? 
            this._player.getPlayerState() : 
            {};

        const isPlayerPlaying : boolean = isPlayerReady ? 
            (playerState !== YoutubeRef().PlayerState.ENDED && playerState !== YoutubeRef().PlayerState.PAUSED) : 
            false;

        return isPlayerPlaying;
    };

    /**
     * Método para inicializar o interval
     */
    protected _initInterval = () : void => {
        if(!this.intervalTime && this._currentTime) {
            this.intervalTime = setInterval(() => {
                this._currentTime.emit(
                    this._player.getCurrentTime()
                );
            }, 250);
        }
    };

    /**
     * Método para destroir o interval
     */
    protected _destroyInterval = () : void => {
        if(this.intervalTime) {
            clearInterval(this.intervalTime);
            this.intervalTime = null;
        }
    };

    /**
     * Método para destroir a instância do player
     */
    public destroy() : void {
        this._destroyInterval();
    };
};