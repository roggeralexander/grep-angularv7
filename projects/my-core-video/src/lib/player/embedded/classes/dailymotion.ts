import { 
    EventEmitter,
    NgZone, 
} from "@angular/core";

// Interfaces
import { 
    Player, 
    PlayerOutputs,
    PlayerStatus
} from "../models/player";

// Utils 
import { 
    DailyMotionPlayerRef, 
} from "../utils/player";

/**
 * Classe para gerar uma instância do dailymotion player
 */
export class DailyMotionPlayer implements Player {
    // Interval para controle do tempo de reprodução
    public intervalTime : any = null;

    // Variável para referenciar a instância do player 
    protected _player : any = null;

    // Variável para referenciar ao id do vídeo que será carregado
    protected _videoId : string = null;

    // Propriedade para obter o protocolo
    protected get _protocol() : string {
        return "https";
    };    

    // Variável para referenciar ao event emitter para emitir o tempo transcurrido de reprodução
    protected _currentTime : EventEmitter<number> = null;

    // Variável para referenciar ao event emitter para emitir o tempo transcurrido de reprodução
    protected _duration : EventEmitter<number> = null;

    constructor(
        protected _embeddedUrl : string, 
        protected _ngZone : NgZone, 
        protected _videoUrl : string
    ) {
        this._videoId = this._videoUrl.replace(
            new RegExp(
                this._embeddedUrl, 
                "gi"
            ), 
            ""
        );
    };

    /**
     * Método para setear o player
     * @param elmId 
     * @param outputs 
     * @param fullscreen
     */
    public setupPlayer(elmId : string, outputs : PlayerOutputs, fullscreen : boolean = false) : void {
        if(outputs.currentTime !== undefined && outputs.currentTime && outputs.currentTime instanceof EventEmitter) {
            this._currentTime = outputs.currentTime;
        }

        if(outputs.duration !== undefined && outputs.duration && outputs.duration instanceof EventEmitter) {
            this._duration = outputs.duration;
        }

        const player = DailyMotionPlayerRef();

        this._player = player(
            document.getElementById(elmId), 
            {
                video: this._videoId, 
                params: {
                    fullscreen: fullscreen,
                    origin: window.location.origin,
                    "queue-enable": false, 
                    "queue-autoplay-next": false, 
                    "sharing-enable": false, 
                    "ui-logo": false, 
                    "ui-start-screen-info": false, 
                    "ui-theme": "dark"
                }, 
                events: {
                    apiready: ($event : any) => {
                        this._ngZone.run(
                            () => outputs.ready && outputs.ready.next($event.target)
                        );
                    }, 
                    error: ($event : any) => {
                        this._ngZone.run(
                            () => outputs.error && outputs.error.next($event)
                        )
                    }, 
                    end: ($event : any) => this._onStateChange($event, outputs.change), 
                    pause: ($event : any) => this._onStateChange($event, outputs.change), 
                    play: ($event : any) => this._onStateChange($event, outputs.change), 
                    waiting: ($event : any) => this._onStateChange($event, outputs.change), 
                    video_start: ($event : any) => this._onStateChange($event, outputs.change), 
                    video_end : ($event : any) => this._onStateChange($event, outputs.change), 
                    playing: ($event : any) => {
                        this._onStateChange($event, outputs.change);
                    }, 
                    durationchange: ($event : any) => {
                        if(this._duration) {
                            this._duration.emit(
                                this._player.duration
                            );
                        }
                    }, 
                    timeupdate: ($event : any) => {
                        if(this._currentTime && this._player.currentTime !== null) {
                            this._currentTime.emit(
                                this._player.currentTime
                            );
                        }   
                    }
                }
            }
        );

        return this._player;
    };

    /**
     * Método de callback para quando houver mudança de estado do player
     * @param $event
     */
    protected _onStateChange = ($event : any, change : EventEmitter<any>) => {
        let status = null;

        switch($event.type) {
            case "play":
                status = PlayerStatus.PLAYING;
                break;
            case "pause": 
                status = PlayerStatus.PAUSED;
                break;
            case "waiting": 
                status = PlayerStatus.BUFFERING;
                break;
            case "end": 
                status = PlayerStatus.ENDED;
                break;
            case "video_start": 
                status = PlayerStatus.PLAYING;
                break;
            case "video_end": 
                status = PlayerStatus.ENDED;
                break;
        }

        this._ngZone.run(
            () => change && change.emit(status)
        );
    };

    /**
     * Método para play ou pause do player
     * @param flag 
     */
    public play(flag : boolean) {
        if(this._player !== undefined && this._player) {
            flag ? 
                this._player.play() : 
                this._player.pause();
        }
    };

    /**
     * Método para retornar a instância do player
     */
    public getPlayer() : any {
        return this._player;
    };

    /**
     * Método para destroir a instância do player
     */
    public destroy() : void {
        if(this.intervalTime !== undefined && this.intervalTime) {
            clearInterval(this.intervalTime);
        }
    };
};