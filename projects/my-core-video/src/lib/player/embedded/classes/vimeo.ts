import { 
    EventEmitter,
    NgZone,
} from "@angular/core";

// Interfaces
import { 
    Player, 
    PlayerOutputs, 
    PlayerStatus
} from "../models/player";

// Utils 
import { 
    VimeoPlayerRef 
} from "../utils/player";

/**
 * Classe para gerar uma instância do vimeo player
 */
export class VimeoPlayer implements Player {
    // Interval para controle do tempo de reprodução
    public intervalTime : any = null;

    // Variável para referenciar a instância do player 
    protected _player : any = null;

    // Variável para referenciar ao id do vídeo que será carregado
    protected _videoId : string = null;

    // Variável para referenciar ao event emitter para emitir o tempo transcurrido de reprodução
    protected _currentTime : EventEmitter<number> = null;

    // Variável para referenciar ao event emitter para emitir o tempo transcurrido de reprodução
    protected _duration : EventEmitter<number> = null;

    // Propriedade para obter o protocolo
    protected get _protocol() : string {
        const hasWindow = window && window.location;
        const protocol = hasWindow
          ? window.location.protocol.replace(':', '')
          : 'http';
        return protocol;
    };    

    constructor(
        protected _embeddedUrl : string, 
        protected _ngZone : NgZone, 
        protected _videoUrl : string
    ) {
        this._videoId = this._videoUrl.replace(
            new RegExp(
                this._embeddedUrl, 
                "gi"
            ), 
            ""
        );
    };

    /**
     * Método para setear o player
     * @param elmId 
     * @param outputs
     * @param fullscreen
     */
    public setupPlayer(elmId : string, outputs : PlayerOutputs, fullscreen : boolean = false) : void {
        if(outputs.currentTime !== undefined && outputs.currentTime && outputs.currentTime instanceof EventEmitter) {
            this._currentTime = outputs.currentTime;
        }

        if(outputs.duration !== undefined && outputs.duration && outputs.duration instanceof EventEmitter) {
            this._duration = outputs.duration;
        }

        const player = VimeoPlayerRef();

        this._player = new player(
            elmId, 
            {
                api: true,
                autopause: false, 
                autoplay: false,
                byline: false, 
                id: this._videoId, 
                playsinline: false,
                title: false,
            }
        );

        this._registerListeners(
            outputs
        );

        return this._player;
    };

    /**
     * Método para registrar os eventos do player 
     * @param outputs
     */
    protected _registerListeners = (outputs : PlayerOutputs) : void => {
        // Registrando os listeners 
        this._player.on(
            "loaded",
            ($event : any) => {
                this._ngZone.run(
                    () => outputs.ready && outputs.ready.next($event.target)
                );         
                
                // Emitindo a duração do vídeo 
                if(this._duration) {
                    this._player.getDuration()
                        .then((duration : number) => {
                            this._duration.emit(
                                duration
                            );
                        })
                        .catch(err => console.log(err.message));
                }
            } 
        );

        this._player.on(
            "error", 
            ($event : any) => {
                this._ngZone.run(
                    () => outputs.error && outputs.error.next($event)
                )
            }
        );

        this._player.on(
            "play", 
            () => {
                this._ngZone.run(
                    () => outputs.change && outputs.change.emit(
                        PlayerStatus.PLAYING
                    )
                );
            }
        );

        this._player.on(
            "pause", 
            () => {
                this._ngZone.run(
                    () => outputs.change && outputs.change.emit(
                        PlayerStatus.PAUSED
                    )
                );
            }
        );

        this._player.on(
            "ended", 
            () => {
                this._ngZone.run(
                    () => outputs.change && outputs.change.emit(
                        PlayerStatus.ENDED
                    )
                );
            }
        );

        this._player.on(
            "bufferstart", 
            ($event : any) => {
                this._ngZone.run(
                    () => outputs.change && outputs.change.emit(
                        PlayerStatus.BUFFERING
                    )
                );
            }
        );

        this._player.on(
            "timeupdate", 
            ($event : any) => {
                if(this._currentTime && $event["seconds"] !== undefined && $event["seconds"] !== null) {
                    this._currentTime.emit(
                        $event["seconds"]
                    );
                }
            }
        )
    };

    /**
     * Método de callback para quando houver mudança de estado do player
     * @param $event
     */
    protected _onStateChange = ($event : any, change : EventEmitter<any>) : void => {
        this._ngZone.run(
            () => change && change.emit($event)
        );
    };

    /**
     * Método para play ou pause do player
     * @param flag 
     */
    public play(flag : boolean) {
        let promise = flag ?   
            this._player.play() : 
            this._player.pause();

        promise
            .then(() => {
                console.log(flag ? "playing" :  "pausing");
            })
            .catch(err => {
                console.log(err);
            });
    };

    /**
     * Método para retornar a instância do player
     */
    public getPlayer() : any {
        return this._player;
    };

    /**
     * Método para destroir a instância do player
     */
    public destroy() : void {
        if(this.intervalTime !== undefined && this.intervalTime) {
            clearInterval(
                this.intervalTime
            );
        }
    };
};