import { 
    InjectionToken
} from "@angular/core";

export const API_KEYS = new InjectionToken<{ [id : string] : string }>("./configs/tokens");