import { 
    ModuleWithProviders, 
    NgModule
} from "@angular/core"; 

import { 
    CommonModule
} from "@angular/common"; 

// Configs 
import { 
    API_KEYS
} from "./configs/tokens"; 

// Components 
import { 
    PlayerComponent
} from "./components/player/player.component"; 

// Services
import { 
    DataService
} from "./services/data.service"; 

import { 
    PlayerService
} from "./services/player.service"; 

import { 
    UrlService
} from "./services/url.service"; 

@NgModule({
    imports: [
        CommonModule
    ], 
    declarations: [
        PlayerComponent
    ], 
    exports: [
        PlayerComponent
    ], 
    entryComponents: [
        PlayerComponent
    ]
})
export class EmbeddedPlayerModule { 
    static forRoot(configs? : Partial<any>) : ModuleWithProviders {
        return {
            ngModule: EmbeddedPlayerModule, 
            providers: [
                {
                    provide: API_KEYS, 
                    useValue: configs
                }, 
                DataService, 
                PlayerService, 
                UrlService
            ]
        };
    };
};