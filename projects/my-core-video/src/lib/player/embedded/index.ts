// Components 
export { 
    PlayerComponent as EmbeddedVideoPlayerComponent
} from "./components/player/player.component"; 

// Services 
export { 
    DataService as EmbeddedVideoDataService
} from "./services/data.service"; 

export { 
    PlayerService as EmbeddedVideoPlayerService
} from "./services/player.service"; 

export { 
    UrlService as EmbeddedVideoUrlService
} from "./services/url.service"; 

// Modules 
export { 
    EmbeddedPlayerModule as EmbeddedVideoPlayerModule
} from "./embedded-player.module"; 