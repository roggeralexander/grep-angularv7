import { 
  Inject, 
  Injectable, 
  NgZone, 
  PLATFORM_ID
} from '@angular/core';

import {
  isPlatformServer
} from '@angular/common';

import { 
  Subject,
} from "rxjs";

// Configs 
import { 
  API_KEYS
} from "../configs/tokens"; 

// Constants 
import { 
  PLAYERS_DATA
} from "../constants/data";

// Models 
import { 
  Player
} from "../models/player"; 

// Players 
import {
  DailyMotionPlayer
} from "../classes/dailymotion"; 

import { 
  VimeoPlayer
} from "../classes/vimeo";

import { 
  YoutubePlayer
} from "../classes/youtube";

// Utils 
import { 
  DailyMotionRef,
} from "../utils/player";

@Injectable()
export class PlayerService {
  protected _manager : {
    [ id : string ] : {
      scriptLoaded: Subject<void>, 
      apiCreated: Subject<void>, 
      isLoaded: boolean, 
      isApiCreated: boolean
    }
  } = { };

  constructor(
    @Inject(API_KEYS) protected _apiKeys : any, 
    protected _ngZone : NgZone, 
    @Inject(PLATFORM_ID) protected _platformId : Object, 
  ) { 
    if(isPlatformServer(this._platformId)) {
       return ; 
    }

    this._initialize();    
  };

  /**
   * Método para obter a instância do manager geral de player service 
   * @param playerName
   */
  public getData = (playerName : "youtube" | "vimeo" | "dailymotion") : any => this._manager[playerName]; 

  /**
   * Método para solicitar a inicialização dos scripts dos players
   */
  protected _initialize = () : void => {
    Object.keys(PLAYERS_DATA).forEach((playerName : any) => {
      // Gerando a instância
      this._manager[playerName] = {
        scriptLoaded: new Subject(), 
        apiCreated: new Subject(), 
        isApiCreated: false, 
        isLoaded: false
      };

      // Solicitando a criação da api 
      this.createApi(playerName);

      // Solicitando a leitura do script 
      this.loadScript(playerName);
    });
  };

  /**
   * Método para solicitar o carregamento do script de acordo ao nome do player 
   * @param playerName
   */
  public loadScript = (playerName : "youtube" | "vimeo" | "dailymotion" = "youtube") : void => {
    let service = PLAYERS_DATA[playerName];
    
    const script = document.createElement("script");
    script.type = "text/javascript";
    script.id = service.scriptName;
    script.src = service.scriptSrc;
    document.body.appendChild(script);

    this._manager[playerName].scriptLoaded.next();
    this._manager[playerName].isLoaded = true;

    // Quando o service seja especificamente o vimeo, executar o seu callback quando o script estiver lido
    if(playerName === "vimeo") {
      script.onload = window["onVimeoIframeAPIReady"];
    }
  };

  /**
   * Método para a geração da api
   * @param playerName
   */
  public createApi = (playerName : "youtube" | "vimeo" | "dailymotion" = "youtube") : void => {
    switch(playerName) {
      case "dailymotion":
        const dmAsyncInit = () => {
          if(window) {
            DailyMotionRef().init({
              apiKey: this._apiKeys[playerName], 
              status: true, 
              cookie: true
            });

            this._manager[playerName].apiCreated.next();
            this._manager[playerName].isApiCreated = true;
          }
        };

        window["dmAsyncInit"] = dmAsyncInit;

        break;

      case "youtube": 
        const onYouTubeIframeAPIReady = () => {
          if(window) {
            this._manager[playerName].apiCreated.next();
            this._manager[playerName].isApiCreated = true;
          }
        };

        window["onYouTubeIframeAPIReady"] = onYouTubeIframeAPIReady;

        break;

      case "vimeo" :
        const onVimeoIframeAPIReady = () => {
          if(window) {
            this._manager[playerName].apiCreated.next();
            this._manager[playerName].isApiCreated = true;
          }
        };

        window["onVimeoIframeAPIReady"] = onVimeoIframeAPIReady;

        break;
    }
  };  

  /**
   * Método para retornar uma instância do player
   * @param url
   */
  public getPlayer = (url : string) : Player | null => {
    const service = ["youtube", "vimeo", "dailymotion"].find((name : string) => url.match(new RegExp(name, "gi")) !== null);

    let player : any = null;

    switch(service) {
      case "dailymotion": 
        player = DailyMotionPlayer;
        break;
    
      case "vimeo": 
        player = VimeoPlayer;
        break;
        
      case "youtube": 
        player = YoutubePlayer;
        break;
    }

    return new player(
      PLAYERS_DATA[service].urlEmbedded,
      this._ngZone, 
      url
    );
  };
};
