import { 
  Injectable, 
} from "@angular/core";

import { 
  Observable,  
  of
} from "rxjs";

// Utilities 
import { 
   detectDailymotion, 
   detectVimeo, 
   detectYoutube, 
   getEmbeddedUrl, 
   getEmbeddedUrlSync, 
   isEmbeddedUrl
} from "../utils/embedded"; 

@Injectable()
export class UrlService {

  constructor() { };

  /**
   * Obter a url embedded de forma assíncrona: youtube | vimeo | dailymotion
   * @param url 
   * @param options
   */
  public getUrlEmbedded = (strUrl : any, options?: any) : Observable<string | null> => {
    try {
      const url = new URL(strUrl);
    
      if(isEmbeddedUrl(url)) {
        return of(
          strUrl
        );
      }
  
      if(detectYoutube(url)) {
        return getEmbeddedUrl(
          detectYoutube(url), 
          "youtube"
        );
      }
  
      if(detectDailymotion(url)) {
        return getEmbeddedUrl(
          detectDailymotion(url), 
          "dailymotion"
        );
      }
  
      if(detectVimeo(url)) {
        return getEmbeddedUrl(
          detectVimeo(url), 
          "vimeo"
        );
      }
  
      return of(
        null
      );  
    } catch(err) {
      return of(null);
    }
  };

  /**
   * Obter a url embedded de forma síncrona: youtube | vimeo | dailymotion
   * @param url 
   * @param options
   */
  public getUrlEmbeddedSync = (strUrl : any, options?: any) : string | null => {
    try {
      const url = new URL(strUrl);

      if(isEmbeddedUrl(url)) {
        return strUrl;
      }
  
      if(detectYoutube(url)) {
        return getEmbeddedUrlSync(
          detectYoutube(url), 
          "youtube"
        );
      }

      if(detectDailymotion(url)) {
        return getEmbeddedUrlSync(
          detectDailymotion(url), 
          "dailymotion"
        );
      }

      if(detectVimeo(url)) {
        return getEmbeddedUrlSync(
          detectVimeo(url), 
          "vimeo"
        )
      };

      return null;
    } catch(err) {
      return null;
    }
  };
};
