import { 
  Injectable
} from '@angular/core';

import { 
  HttpClient, 
  HttpErrorResponse,
  HttpParams,
  HttpRequest, 
  HttpResponse
} from "@angular/common/http";

import { 
  forkJoin, 
  Observable, 
  of
} from "rxjs"; 

import { 
  switchMap
} from "rxjs/operators"; 

// Constants 
import { 
  DATA
} from "../constants/data"; 

import {
  DAILYMOTION_OPTIONS, 
  VIMEO_OPTIONS,
  YOUTUBE_OPTIONS
} from "../constants/thumb-options"; 

// Models 
import { 
  Details
} from "../models/details"; 

// Services 
import { 
  UrlService
} from "./url.service";

// Utilities 
import { 
  detectDailymotion, 
  detectVimeo, 
  detectYoutube
} from "../utils/embedded"; 

@Injectable()
export class DataService {

  constructor(
    protected _http : HttpClient,
    protected _url : UrlService
  ) { };

  /**
   * Método para solicitar a requisição de data (detalhes + embedded url) do vídeo 
   * @param strUrl
   */
  public request = (strUrl : string) : Observable<Details> => new Observable(observer => {
    setTimeout(() => {
      forkJoin(
        this._requestDetails(
          strUrl
        ),
        this._url.getUrlEmbedded(
          strUrl
        ).pipe(
          switchMap((url : string) => of({
            link: url
          }))
        )
      ).subscribe(
        (results : any) => {
          let obj = {
            ...results[0], 
            ...results[1]
          };

          obj["type"] = "video";
          obj["generatedBy"] = "link";
  
          observer.next(obj);
          observer.complete();
        }, 
        (err : any) => {
          observer.error(err);
          observer.complete();
        }
      );
    });
  });

  /**
   * Método para solicitar a requisição de detalhes do vídeo 
   * @param strUrl
   */
  protected _requestDetails = (strUrl : string) : Observable<any> => new Observable(observer => {
    let observable : Observable<any> = null;

    let onNext = result => {
      observer.next(result);
    };

    let onError = err => {
      observer.error(err);
    };

    let onComplete = () => observer.complete();

    let url = new URL(strUrl);

    if(detectYoutube(url)) {
      observable = this._requestYoutube(
        detectYoutube(url), 
      );
    } else if(detectVimeo(url)) {
      observable = this._requestVimeo(
        detectVimeo(url), 
      );
    } else if(detectDailymotion(url)) {
      observable = this._requestDailymotion(
        detectDailymotion(url), 
      );
    }

    if(!observable) {
      observer.error(`Not supported`); 
      observer.complete();
      return ;
    }

    observable.subscribe(
      onNext, 
      onError, 
      onComplete
    );
  });

  /**
    * Método para requisitar as informações ao youtube 
    * @param id
    */
  protected _requestYoutube = (id : string) : Observable<any> => new Observable(observer => {
    const urlRequest : string = `${ DATA["youtube"]["url"] }`;

    let params = new HttpParams();

    params = params.append(
      "url", 
      `https://www.youtube.com/watch?v=${ id }`
    );

    const onNext = result => {
      let obj = {
        title: result["title"],
        thumb: result["thumbnail_url"].replace(
          new RegExp("hqdefault"), 
            "default"
          ), 
          poster: result["thumbnail_url"].replace(
            new RegExp("hqdefault"), 
            "sddefault"
          )
      };
          
      observer.next(obj);
      observer.complete();
    };

    const onError = err => {
      observer.error(err);
      observer.complete();
    };

    this._executeRequest(
      urlRequest, 
      params
    ).subscribe(
      onNext, 
      onError
    );
  });

  /**
   * Método para realizar o request ao vimeo informando o id do vídeo 
   * @param id 
   * @param options
   */
  protected _requestVimeo = (id : string) : Observable<any> => new Observable(observer => {
    let options = {};
        
    options["thumb"] = VIMEO_OPTIONS.indexOf(options["thumb"]) >= 0 ? options["thumb"] : 'thumbnail_medium';
    options["poster"] = VIMEO_OPTIONS.indexOf(options["poster"]) >= 0 ? options["poster"] : 'thumbnail_large';
          
    const urlRequest : string = `${ DATA["vimeo"]["url"] }/${ id }.json`;

    const onNext = result => {
      let obj = {
        thumb: result[0][options["thumb"]], 
        poster: result[0][options["poster"]], 
        title: result[0]["title"], 
        description: result[0]["description"], 
        duration: result[0]["duration"]
      };

      observer.next(obj);
      observer.complete();
    };

    const onError = err => {
      observer.error(err);
      observer.next();
    };

    this._executeRequest(
      urlRequest
    ).subscribe(
      onNext, 
      onError
    );
  });

  /**
   * Método para realizar o request ao dailymotion informando o id do vídeo 
   * @param id 
   * @param options
   */
  protected _requestDailymotion = (id : string) : Observable<any> => new Observable(observer => {
    let options = {};
        
    options["thumb"] = DAILYMOTION_OPTIONS.indexOf(options["thumb"]) >= 0 ? options["thumb"] : 'thumbnail_120_url';
    options["poster"] = DAILYMOTION_OPTIONS.indexOf(options["poster"]) >= 0 ? options["poster"] : 'thumbnail_720_url';
    options["title"] = "title";
    options["description"]="description"; 
    options["duration"] = "duration";

    const urlRequest : string = `${ DATA["dailymotion"]["url"] }/${ id }`;

    let httpParams : HttpParams = new HttpParams();
          
    httpParams = httpParams.append(
      "fields", 
      Object.values(options).join(",")
    );

    const onNext = result => {
      let obj = {
        thumb: result[options["thumb"]], 
        poster: result[options["poster"]],
        duration: result[options["duration"]], 
        description: result[options["description"]], 
        title: result[options["title"]]   
      }

      observer.next(obj);
      observer.complete();
    };

    const onError = err => {
      observer.error(err);
      observer.complete();
    };

    this._executeRequest(
      urlRequest, 
      httpParams
    ).subscribe(
      onNext, 
      onError
    );
  });

  /**
   * Método para executar o request 
   * @param url
   * @param params
   */
  protected _executeRequest = (url : string, params?: HttpParams) : Observable<any | null> => new Observable(observer => {
    const onNext = $event => {
      if($event instanceof HttpResponse) {
        if($event.status === 200) {
          observer.next($event.body);
          observer.complete();
        }
      }
    };

    const onError = (errorResponse : HttpErrorResponse) => {
      let obj = {};

      for(let [key, value] of Object.entries(errorResponse.error)) {
        obj[key] = value;
      }
          
      observer.error(obj);
      observer.complete();
    };

    let req = null;

    if(params === undefined || !params || !Object.keys(params).length) {
      req = new HttpRequest(
        "GET", 
        url, 
      );
    } else {
      req = new HttpRequest(
        "GET", 
        url, 
        {
          params: params
        }
      );
    }

    this._http.request(req)
      .subscribe(
        onNext, 
        onError
    );
  }); 
};
