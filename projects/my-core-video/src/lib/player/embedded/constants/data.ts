import { 
    EmbeddedData, 
    PlayerData
} from "../models/data";

export const PLAYERS_DATA : { [ id : string ] : PlayerData } = {
    dailymotion: {
        scriptName: "dailymotion-ifplayer", 
        urlEmbedded: "https://www.dailymotion.com/embed/video/",
        scriptSrc: "https://api.dmcdn.net/all.js"
    }, 
    youtube: {
        scriptName: "youtube-ifplayer",
        urlEmbedded: "https://www.youtube.com/embed/", 
        scriptSrc: "https://www.youtube.com/iframe_api", 
    },
    vimeo: {
        scriptName: "vimeo-ifplayer",
        urlEmbedded: "https://player.vimeo.com/video/", 
        scriptSrc: "https://player.vimeo.com/api/player.js"
    }
};

export const DATA : { [ id : string ] : EmbeddedData } = {
    dailymotion: {
        url: "https://api.dailymotion.com/video"
    }, 
    youtube: {
        url: "https://noembed.com/embed", 
    }, 
    vimeo: {
        url: "https://vimeo.com/api/v2/video"
    }
};