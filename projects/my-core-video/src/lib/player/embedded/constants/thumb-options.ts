export const YOUTUBE_OPTIONS = [
    'default',
    'mqdefault',
    'hqdefault',
    'sddefault',
    'maxresdefault'
];

export const VIMEO_OPTIONS = [
    'thumbnail_small',
    'thumbnail_medium',
    'thumbnail_large'
];

export const DAILYMOTION_OPTIONS = [
    'thumbnail_60_url',
    'thumbnail_120_url',
    'thumbnail_180_url',
    'thumbnail_240_url',
    'thumbnail_360_url',
    'thumbnail_480_url',
    'thumbnail_720_url',
    'thumbnail_1080_url'
];