/**
 * Interface para representar a data vinculada ao player
 */
export interface PlayerData {
    scriptName: string; 
    urlEmbedded: string;
    scriptSrc: string; 
};

/**
 * Interface para representar à data embedded
 */
export interface EmbeddedData {
    url: string; 
};