/**
 * Interface para representar o model de dados retornados quando consultado o embedded video
 */
export interface Details {
    link: string; 
    thumb: string;
    poster: string; 
    title: string;
    description?: string; 
    duration?: string | number;
};