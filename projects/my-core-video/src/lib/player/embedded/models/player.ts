import { 
    EventEmitter, 
} from "@angular/core";

/**
 * Enum para especificar o player status
 */
export enum PlayerStatus {
    // Vídeo não inicializado
    UNSTARTED = -1,

    // Vídeo finalizou a sua reprodução
    ENDED = 0,

    // Vídeo em reprodução 
    PLAYING = 1, 

    // Vídeo pausado
    PAUSED = 2, 

    // Buffering do vídeo
    BUFFERING = 3, 

    // Procurando ponto de reprodução do vídeo
    SEEKING = 4, 

    // Ponto de reprodução procurado sendo reproduzido
    SEEKED = 5
};

/**
 * Interface para especificar os eventos que deverão ser monitorados quando gerado o player
 */
export interface PlayerOutputs {
    change: EventEmitter<any>;
    ready: EventEmitter<any>;
    error: EventEmitter<any>;
    currentTime : EventEmitter<any>;
    duration: EventEmitter<any>;
};

/**
 * Interface como contrato de implementação de uma classe para a geração de player
 */
export interface Player {
    // Interval para controle do tempo de reprodução
    intervalTime : any;

    // Método para solicitar o setup do player
    setupPlayer(
        elmId : string, 
        playerOutputs: PlayerOutputs, 
        fullscreen?: boolean
    ) : void;

    // Aplicar play ou pause ao player 
    // flag = true -> play 
    // flag = false -> pause
    play(flag : boolean) : void;

    // Método para obter a instância do player 
    getPlayer() : any;

    // Método para destroir a instância do player 
    destroy() : void;
};