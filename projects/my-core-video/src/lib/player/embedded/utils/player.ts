/**
 * Método para obter uma referência do youtube após carregado o script
 */
export function YoutubeRef() {
    return window["YT"];
};

/**
 * Método para obter uma referência do player de vídeo para o youtube
 */
export function YoutubePlayerRef() {
    return YoutubeRef().Player;
};

/**
 * Método para obter uma referência do vimeo após carregado o script
 */
export function VimeoRef() {
    return window["Vimeo"];
};

/**
 * Método para obter uma referência do player de vídeo para o vimeo
 */
export function VimeoPlayerRef() {
    return VimeoRef().Player;
};

/**
 * Método para obter a referência do dailymotion após carregado o script
 */
export function DailyMotionRef() {
    return window['DM'];
};

/**
 * Método para obter a referência do player do dailymotion
 */
export function DailyMotionPlayerRef() {
    return DailyMotionRef().player;
};

/**
 * Método para obter o element iframe do player
 * @param serviceName 
 */
export function getPlayerIframeHTML(serviceName : any, id : string) : HTMLIFrameElement | null {
    if(serviceName === undefined || !serviceName) {
        return null;
    }

    let htmlElm : HTMLIFrameElement = null;

    switch(serviceName) {
      case "vimeo": 
        htmlElm = document.querySelector(`div[id="${ id }"]`).getElementsByTagName("iframe")[0];
        break;
      default : 
        htmlElm = document.querySelector(`iframe[id="${ id }"]`);
        break;
    }

    return htmlElm;
};
