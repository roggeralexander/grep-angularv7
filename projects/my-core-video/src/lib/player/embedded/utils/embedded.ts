import { 
    Observable, 
    of
} from "rxjs";  

// Constants 
import { 
    PLAYERS_DATA
} from "../constants/data"; 

/**
 * Método para saber se a url informada já é embebbida ou não
 * @param url 
 */
export const isEmbeddedUrl = (url : any) : boolean => {
    const urls : string[] = Object.values(PLAYERS_DATA).map((item : any) => item["urlEmbedded"]);
    
    const regExp = new RegExp(
        urls.join("|"), 
        "gi"
    );

    return url.href.match(regExp) !== null; 
};

/**
 * Parse options
 * @param options
 */
export const parseOptions = (options: any) : any => {
    let queryString = '';
    let attributes = '';

    if(options && options.hasOwnProperty('query')) {
        queryString = '?' + serializeQuery(options.query);
    }

    if (options && options.hasOwnProperty('attr')) {
        let temp = <any>[];

        Object.keys(options.attr).forEach(function (key) {
            temp.push(key + '="' + (options.attr[key]) + '"');
        });

        attributes = ' ' + temp.join(' ');
    }

    return {
        query: queryString,
        attr: attributes
    };
};

/**
 * Query serializer
 * @param query
 */
export const serializeQuery = (query: any): any => {
    let queryString: any = [];

    for (let p in query) {
        if (query.hasOwnProperty(p)) {
            queryString.push(encodeURIComponent(p) + '=' + encodeURIComponent(query[p]));
        }
    }

    return queryString.join('&');
};

/** 
 * Detectar se o vídeo embedded é vimeo
 * @param url
*/
export const detectVimeo = (url : any) : string => {
    let numberRE = /^\d+$/;
    let array : string[] = url.pathname.split("/");
    let id : string = null;
    
    let found = array.some(val => {
      let ok : boolean = false; 

      if(numberRE.test(val)) {
        id = val;
        ok = true;
      }

      return ok;
    });

    return (url.hostname === 'vimeo.com' || url.hostname === 'player.vimeo.com') ? 
      id : 
      null;
};

/** 
 * Detectar se o vídeo embedded é youtube
 * @param url
*/
export const detectYoutube = (url : any) : string => {
    if (url.hostname.indexOf('youtube.com') > -1) {
        return url.search.split('=')[1];
      }
  
      if (url.hostname === 'youtu.be') {
        return url.pathname.split('/')[1];
      }
  
      return null;
};

/** 
 * Detectar se o vídeo embedded é dailymotion
 * @param url
*/
export const detectDailymotion = (url: any): string => {
    if (url.hostname.indexOf('dailymotion.com') > -1) {
        return url.pathname.split('/')[2].split('_')[0];
    }

    if (url.hostname === 'dai.ly') {
        return url.pathname.split('/')[1];
    }

    return null;
};

/**
 * Obter a url de forma assíncrona
 * @param id 
 * @param service
 * @param options
 */
export const getEmbeddedUrl = (id: string, service : "youtube" | "dailymotion" | "vimeo", options?: any) : Observable<string> => {
    options = parseOptions(options);
    let queryString;

    if (options && options.hasOwnProperty('query')) {
      queryString = '?' + serializeQuery(options.query);
    }

    return of(
        `${ PLAYERS_DATA[service]["urlEmbedded"] }${ id }`
    );
};

/**
 * Obter a url de forma síncrona
 * @param id 
 * @param service
 * @param options
 */
export const getEmbeddedUrlSync = (id : string, service : "youtube" | "dailymotion" | "vimeo", options?: any) : string => {
    options = parseOptions(options);
    let queryString;

    if (options && options.hasOwnProperty('query')) {
      queryString = '?' + serializeQuery(options.query);
    }

    return `${ PLAYERS_DATA[service]["urlEmbedded"] }${ id }`;
};

