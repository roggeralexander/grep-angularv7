import { 
  NgModule 
} from '@angular/core';

// Sub modules 
import {
  CaptureModule
} from "./capture/capture.module"; 

import { 
  CommonPlayerModule
} from "./player/common/common-player.module"; 

import { 
  EmbeddedPlayerModule
} from "./player/embedded/embedded-player.module"; 

@NgModule({
  exports: [
    CaptureModule, 
    CommonPlayerModule, 
    EmbeddedPlayerModule
  ]
})
export class MyCoreVideoModule { };
