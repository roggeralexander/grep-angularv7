import { 
    ModuleWithProviders, 
    NgModule 
} from '@angular/core';
  
import { 
    CommonModule
} from "@angular/common"; 
  
// Configs & Tokens 
import { 
    PLUGIN_VIEWER
} from "./configs/tokens";
  
// Components
import { 
    DocComponent 
} from './components/doc/doc.component';
  
@NgModule({
    imports: [
        CommonModule
    ], 
    declarations: [
        DocComponent
    ], 
    exports: [
        DocComponent
    ]
})
export class ViewerModule { 
  static forRoot(configs?: Partial<{ [id : string] : string }>) : ModuleWithProviders {
    return { 
      ngModule: ViewerModule, 
      providers: [
        {
          provide: PLUGIN_VIEWER, 
          useValue: configs
        }
      ]
    };
  };    
};