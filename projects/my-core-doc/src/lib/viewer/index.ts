// Components 
export {
    DocComponent as DocViewerComponent
} from "./components/doc/doc.component";

// Module 
export {
    ViewerModule as DocViewerModule
} from "./viewer.module"; 