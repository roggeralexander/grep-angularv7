import { 
    InjectionToken
} from "@angular/core"; 

export const PLUGIN_VIEWER = new InjectionToken<{ [ id : string ] : string }>("./configs/tokens"); 