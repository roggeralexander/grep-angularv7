import { 
  ChangeDetectionStrategy, 
  ChangeDetectorRef, 
  Component,
  ElementRef, 
  Inject, 
  Input, 
  OnDestroy,  
  OnInit, 
  PLATFORM_ID, 
  Renderer2
} from '@angular/core';

import { 
  isPlatformServer
} from "@angular/common"; 

import { 
  HttpClient, 
  HttpRequest 
} from "@angular/common/http"; 

import { 
  Observable, 
  of, 
  Subscription, 
  throwError, 
  timer
} from "rxjs"; 

import {
  catchError, 
  switchMap,
  tap
} from "rxjs/operators"; 

// Configs & Tokens 
import { 
  PLUGIN_VIEWER
} from "../../configs/tokens"; 

@Component({
  selector: 'mc-doc-viewer',
  templateUrl: './doc.component.html',
  styleUrls: ['./doc.component.scss'], 
  changeDetection: ChangeDetectionStrategy.Default
})
export class DocComponent implements OnInit, OnDestroy {
  // Link a ser visualizado
  @Input() 
  get src() : string { 
    return this._src; 
  };
  set src(src : string) {
    if(src === undefined || !src || !src.trim().length || this._src === src) {
      return ; 
    }

    this._src = src; 
  };
  protected _src : string = null; 

  // Service a ser usado para mostrar o link 
  @Input()
  get service() : "google" | "ms-office" {
    return this._service; 
  };
  set service (service : "google" | "ms-office") {
    if(service === undefined || !service || !service.trim().length || this._service === service) {
      return ; 
    }

    this._service = service; 
  };
  protected _service : "google" | "ms-office" = "google"; 

  // Propriedade para referenciar que o url está sendo lido 
  public isLoading : boolean = false;

  // Subscriptions 
  protected _subs : Subscription[] = [];

  constructor(
    protected _changeDetector : ChangeDetectorRef, 
    protected _elmRef : ElementRef, 
    protected _http : HttpClient, 
    @Inject(PLATFORM_ID) protected _platformId : Object,
    @Inject(PLUGIN_VIEWER) protected _pluginViewer : any, 
    protected _renderer : Renderer2
  ) { 
    console.log("plugins viewer", PLUGIN_VIEWER);
  };

  ngOnInit() {
    if(isPlatformServer(this._platformId)) { 
      return ; 
    }

    this._subs.push(
      timer(100).pipe(
        tap(() => console.log("Observable timer concluído")), 
        switchMap(() => this._initialize()), 
        catchError((err : Error) => {
          console.log("Error na leitura do document", err);
          return of(
            undefined
          );
        })
      ).subscribe(() => {
        console.log("Doc component inicializado");
      })
    )
  };

  ngOnDestroy() {
    this._subs.forEach((sub : Subscription) => {
      sub.unsubscribe(); 
    }); 
  };  

  /**
   * Método para solicitar a inicialização do component
   */
  protected _initialize = () : Observable<void> => {
    if(this._src === undefined || !this._src || !this._src.trim().length || this._service === undefined || !this._service || !this._service.trim().length) {
      return throwError(
        new Error(
          "Src or service for Document viewer not informed"
        )
      );
    }

    // Indicar que a leitura for solicitada 
    this.isLoading = true; 

    // Atualizar o url do iframe 
    const iframeUrl : string = this._pluginViewer[
      this._service
    ].replace(
      new RegExp(`\\$src`, "gi"), 
      this._src
    );

    return timer(100).pipe(
      switchMap(() => this._loadUrl(
        iframeUrl
      ))
    );
  };

  /**
   * Método para solicitar o load do url do document 
   * @param url
   */
  protected _loadUrl = (url : string) : Observable<void> => this._http.request(
    new HttpRequest(
      "GET", 
      url
    )
  ).pipe(
    switchMap(() => this._createIframe(url))
  );

  /**
   * Método para solicitar a criação do iframe do vídeo 
   * @param url
   */
  protected _createIframe = (url : string) : Observable<void> => {

    const iframe : HTMLIFrameElement = <HTMLIFrameElement>this._renderer.createElement("iframe");

    // Generando o iframe
    this._renderer.setAttribute(
      iframe, 
      "frameborder", 
      "0"
    );

    this._renderer.setAttribute(
      iframe, 
      "width", 
      "100%"
    );

    this._renderer.setAttribute(
      iframe, 
      "height", 
      "100%"
    );

    this._renderer.setAttribute(
      iframe, 
      "src", 
      url
    );

    this._renderer.appendChild(
      this._elmRef.nativeElement, 
      iframe
    );

    return timer(25).pipe(
      switchMap(() => {
        this.isLoading = false; 
        this._changeDetector.detectChanges();

        return of(
          undefined
        );
      })
    );
  };
};
