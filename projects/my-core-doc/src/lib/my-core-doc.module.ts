import { 
  NgModule 
} from '@angular/core';

// Sub modules 
import { 
  ViewerModule
} from "./viewer/viewer.module"; 

@NgModule({
  exports: [
    ViewerModule
  ]
})
export class MyCoreDocModule { };
