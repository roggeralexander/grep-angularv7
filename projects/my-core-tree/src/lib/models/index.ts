/**
 * Classe model para referenciar à data associada ao node
 */
export class NodeData {
    id: string | number; // Id do node 
    name: string | Object; // Nome ou label vinculado ao node
    path: string;  // Path do node 
    parentId?: string | number; // Id do parent do node
    badge?: any; // Badge ou informação que será mostrada como bacge
    level?: number; // Level do node na tree view
    blocked?: boolean; // Indicar se está bloqueado para seleção
    parentNode?: NodeData; // Referência ao node pai
    checked?: boolean;  // Indicar se está selecionado ou não 
    indeterminate?: boolean; // Indicar se os filhos do parent foram todos escolhidos ou estão escolhidos de forma partial
    children?: NodeData[]; // Referência aos filhos do node 
    expanded?: boolean; // Indicar que o node está expandido ou não
    slideState?: "down" | "up"; // Indicar o estado da animação - Up ou down
};

/**
 * Interface para especificar os keys que correspondem à data de entrada para geração do tree view
 */
export interface TreeDataKeys {
    idKey: string;
    pathKey: string; 
    separator: string; 
    nameKey: string;
    parentIdKey: string;
    badgeKey?: string;
    childrenKey?: string;
};

