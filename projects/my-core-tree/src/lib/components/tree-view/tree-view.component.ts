import { 
  ChangeDetectionStrategy, 
  ChangeDetectorRef, 
  Component, 
  ComponentFactoryResolver, 
  EventEmitter,
  Inject, 
  Input, 
  OnDestroy, 
  Output, 
  PLATFORM_ID, 
  ViewChild, 
  ViewContainerRef
} from '@angular/core';

import { 
  isPlatformServer
} from "@angular/common";

import { 
  Subscription, 
  Subject
} from "rxjs"; 

// Models 
import { 
  NodeData
} from "../../models";

@Component({
  selector: 'mc-tree-view',
  templateUrl: './tree-view.component.html',
  styleUrls: ['./tree-view.component.scss'], 
  changeDetection: ChangeDetectionStrategy.Default
})
export class TreeViewComponent implements OnDestroy {
  @Input() 
  get data() : NodeData[] {
    return this._data;
  };
  set data(data : NodeData[]) {
    if(isPlatformServer(this._platformId) || data === undefined || this._data === data) {
      return ; 
    }

    this._data = data;

    setTimeout(() => {
      this._worker.next();
    })
  };
  protected _data : NodeData[] = [];

  // Input para saber se é permitido a seleção de elementos do tree ou não 
  @Input() 
  get allowSelect() : boolean {
    return this._allowSelect;
  };
  set allowSelect(flag : boolean) {
    if(flag === undefined || flag === null || this._allowSelect === flag) {
      return ; 
    }

    this._allowSelect = flag;
  };
  protected _allowSelect : boolean = false;

  // Input para indicar que será permitida a ação de edição
  @Input() 
  get allowEdit() : boolean {
    return this._allowEdit;
  };
  set allowEdit(flag : boolean) {
    this._allowEdit = flag; 
  };
  protected _allowEdit : boolean = false;

  // Input para indicar que será permitida a inserção de um novo nó no treeview
  @Input()
  get allowInsert() : boolean {
    return this._allowInsert;
  };
  set allowInsert(flag : boolean) {
    if(flag === undefined || flag === null || this._allowInsert === flag) {
      return ; 
    }

    this._allowInsert = flag;
  };
  protected _allowInsert : boolean = false;

  // Input para indicar que o tree view deve ser mostrado inicialmente expandido ou não
  @Input() 
  get expanded() : boolean {
    return this._expanded;
  };
  set expanded(flag : boolean) {
    this._expanded = flag;
  };
  protected _expanded : boolean = true;

  // Input para indicar quais os itens selecionados
  @Input() 
  get itemsChecked() : any[] {
    return this._itemsChecked;
  };
  set itemsChecked(items : any[]) {
    if(items === undefined || !items || this._itemsChecked === items) {
      return ;
    }

    this._itemsChecked = items;
  };
  protected _itemsChecked : any[] = [];

  // Input para indicar quais os itens que estão bloqueados para seleção 
  @Input() 
  get itemsBlocked() : any[] {
    return this._itemsBlocked;
  };
  set itemsBlocked(items : any[]) {
    if(items === undefined || !items || this._itemsBlocked === items) {
      return ;
    }

    this._itemsBlocked = items;
  };
  protected _itemsBlocked : any[] = [];

  // Dynamic Template para o component dinâmico a ser gerado para aplicar alguma operação a algum nó na tree view
  @ViewChild(
    "dynamic", 
    { read : ViewContainerRef }
  ) protected _entry : ViewContainerRef = null;

  // Propriedade para especificar a data processada 
  public dataProcessed : any = null;

  // Subjects & Subscriptions
  protected _worker : Subject<void> = new Subject();
  protected _workerNodeOp : Subject<void> = new Subject();
  protected _subs : Subscription[] = [];

  // Propriedade para indicar o key de selection 
  protected readonly _keySelection : string = "id"; 

  // Output para emissão das mudanças de escolhas
  @Output() 
  public itemsCheckedChange : EventEmitter<string[] | number[]> = new EventEmitter();

  constructor(
    protected _changeDetector : ChangeDetectorRef, 
    @Inject(PLATFORM_ID) protected _platformId : Object, 
    protected _resolver : ComponentFactoryResolver
  ) { 
    if(isPlatformServer(this._platformId)) {
      return ; 
    }

    // Subscription para saber quando alguma operação foi aplicada 
    this._subs.push(
      this._workerNodeOp.subscribe(
        () => {
          if(this._entry) {
            this._entry.clear();
          }
        }
      )
    );
  };

  ngOnDestroy() {
    this._subs.forEach((sub : Subscription) => sub.unsubscribe());

    this._worker.complete(); 
    this._worker = null; 

    this._workerNodeOp.complete(); 
    this._workerNodeOp = null; 
  };

  /**
   * Método para saber se um node está bloqueado.
   * Caso possuir pai, conferir se ele está bloqueado 
   * Se o pai estiver bloqueado, o filho também o estará
   */
  protected _isBlocked = (node : NodeData) : boolean => {
    if(this._itemsBlocked === undefined || !this._itemsBlocked || !this._itemsBlocked.length) {
      return false;
    }

    // Conferir se está dentro do array 
    let flag : boolean = this._itemsBlocked.some((item : any) => item === node["id"]);

    // Caso não estiver bloqueado, conferir se o pai o está
    if(!flag) {
      const { parentNode } = node;

      flag = parentNode !== undefined && parentNode ? 
        parentNode["blocked"] : 
        false;
    }
    
    return flag;
  };

  /**
   * Método para obter o índice do node e saber se ele está selecionado ou bloqueado 
   * @param node 
   */
  public getIndexCheckedNode = (node : NodeData) : number => {
    if(this._itemsChecked === undefined || !this._itemsChecked || !this._itemsChecked.length) {
      return -1;
    }

    return this._itemsChecked.findIndex((item : string | number) => item === node[this._keySelection]);
  };

  /**
   * Método para pesquisar no node informado e obter a sua respectiva propriedade 
   * @param node 
   * @param prop
   */
  protected _getPropertyValue = (node : NodeData, prop : "indeterminate" | "checked" | "blocked") : boolean => node && node[prop] !== undefined ? 
    node[prop] : 
    false;

  /**
   * Método para atualizar a seleção de itens 
   * @param node
   * @param checked 
   */
  protected _updateitemsChecked = (node : NodeData, checked : boolean) : void => {
    let indexChecked = this.getIndexCheckedNode(
      node
    );

    if(this._itemsChecked === undefined || !this._itemsChecked) {
      this._itemsChecked = [];
    }

    if(checked && indexChecked === -1) {
      this._itemsChecked.push(
        node[this._keySelection]
      );

      this._itemsChecked = Array.from(
        new Set(
          this._itemsChecked
        )
      );
    } else if(!checked && indexChecked !== -1) {
      this._itemsChecked.splice(
        indexChecked, 
        1
      );
    }
  };

  /**
   * Método para solicitar que a propagação de seleção seja realizada nos filhos do node 
   * @param node
   * @param checked
   */
  protected _childrenPropagation = (node : NodeData, checked : boolean) : void => {
    node["indeterminate"] = false;

    const { children } = node;
    
    if(children !== undefined && children && children.length > 0) {
      children.forEach((child : NodeData) => {
        child["blocked"] = this._isBlocked(
          child
        );

        // Sempre e quando o child não estiver bloqueado, atualizar como selecionado ou não 
        !child["blocked"] ? 
          child["checked"] = checked : 
          null;
          
        // Propagar aos filhos
        this._childrenPropagation(
          child, 
          checked
        );
      });
    } else {
      // A ATUALIZAÇÃO DOS ITEMS SELECIONADOS SERÁ REALIZADO DIRETAMENTE SÓ NOS NODE FILHOS, OS QUAIS NÃO POSSUIRÃO MAIS FILHOS
      this._updateitemsChecked(
        node, 
        checked
      );
    }
  };

  /**
   * Método para propagar a seleção do node nos pais, atualizando se vai ser seleção total, parcial ou nula
   * @param node
   * @param toSelect 
   */
  protected _parentsPropagation = (node : NodeData, toSelect : boolean) : void => {
    if(node.parentNode !== undefined && node.parentNode) {
      let parentNode = node["parentNode"];
      
      let totalChildSelected : number = 0;
      let childPartialSelected : boolean = false;

      const { children } = parentNode;

      // PERCORRENDO TODOS OS FILHOS PARA SABER SE ALGUM FOI SELECIONADO PARCIALMENTE OU ESTÁ SELECIONADO 
      if(children !== undefined && children && children.length > 0) {
        children.forEach(child => {
          if(this._getPropertyValue(child, "checked")) {
            totalChildSelected++;
          } else if(this._getPropertyValue(child, "indeterminate")) {
            childPartialSelected = true;
          }
        });
      }
      
      // CONFERINDO SE É PARA O PAI SER SELECIONADO 
      if(toSelect) {
        parentNode["indeterminate"] = totalChildSelected !== children.length;
        parentNode["checked"] = !parentNode["indeterminate"];
      } else {
        parentNode["checked"] = toSelect;

        // CONFERINDO O PARTIAL SELECTED 
        if(childPartialSelected || totalChildSelected > 0 && totalChildSelected !== children.length) {
          parentNode["indeterminate"] = true;
        } else {
          parentNode["indeterminate"] = false;
        }
      }

      this._parentsPropagation(
        parentNode,
        toSelect
      );
    }
  };  

  /**
   * Método para atualizar a seleção de um node, sendo atualizado também para os filhos e os pais
   * @param node
   * @param checked 
   */
  public updateSelection = (node : NodeData, checked : boolean) : void => {
    node["checked"] = checked;

    node["blocked"] = this._isBlocked(
      node
    );

    // Propagar para o children
    this._childrenPropagation(
      node, 
      checked
    );
    
    // Propagar para os parents
    this._parentsPropagation(
      node,
      checked 
    );

    this.itemsCheckedChange.emit(
      this._itemsChecked
    );
  };
};
