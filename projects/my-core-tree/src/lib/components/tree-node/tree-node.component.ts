import { 
  ChangeDetectionStrategy, 
  ChangeDetectorRef, 
  Component, 
  EventEmitter, 
  forwardRef, 
  Inject, 
  Input, 
  OnInit, 
  OnDestroy,
  PLATFORM_ID
} from '@angular/core';

import { 
  isPlatformServer
} from "@angular/common";

import { 
  AnimationEvent, 
  animate, 
  trigger, 
  state, 
  style, 
  transition 
} from "@angular/animations";

import { 
  Subscription
} from "rxjs";

// Component para o forward ref 
import { 
  TreeViewComponent as TreeView
} from "../tree-view/tree-view.component"; 

// Constantes 
import { 
  ANIMATION_TIMINGS
} from "../../constants";

// Modelos 
import { 
  NodeData
} from "../../models"; 

@Component({
  selector: 'mc-tree-node',
  templateUrl: './tree-node.component.html',
  styleUrls: ['./tree-node.component.scss'], 
  animations: [
    trigger("slide", [
      state(
        "void", 
        style({
          opacity: 0, 
          height: "0px"
        })
      ),
      state(
        "down", 
        style({
          opacity: 1, 
          height: '*'
        })
      ), 
      state(
        'up', 
        style({
          opacity: 0, 
          height: '0px'
        })
      ), 
      transition(
        '* => *', 
        animate(ANIMATION_TIMINGS)
      ), 
    ])
  ], 
  changeDetection: ChangeDetectionStrategy.Default
})
export class TreeNodeComponent implements OnInit, OnDestroy {
  // Input para referenciar ao node
  @Input() 
  get node() : NodeData {
    return this._node;
  };
  set node(n : NodeData) {
    this._node = n;
  }; 
  protected _node : NodeData = null; 

  // Input para referenciar ao parent do node
  @Input() 
  get parentNode() : NodeData {
    return this._parentNode;
  };
  set parentNode(pN : NodeData) {
    this._parentNode = pN;
  };
  protected _parentNode : NodeData = null; 

  // Input para indicar que é o primeiro filho
  @Input() 
  get firstChild() : boolean {
    return this._firstChild;
  };
  set firstChild(flag : boolean) {
    this._firstChild = flag;
  };
  protected _firstChild : boolean = false;

  // Input para indicar que é o último filho
  @Input() 
  get lastChild() : boolean {
    return this._lastChild;
  };
  set lastChild(flag : boolean) {
    this._lastChild = flag;
  };
  protected _lastChild : boolean = false;

  // Input para indicar que será permitida a ação de edição
  @Input() 
  get allowEdit() : boolean {
    return this._allowEdit;
  };
  set allowEdit(flag : boolean) {
    this._allowEdit = flag; 
  };
  protected _allowEdit : boolean = false;

  // Input para indicar que será permitida a inserção de um novo nó no treeview
  @Input()
  get allowInsert() : boolean {
    return this._allowInsert;
  };
  set allowInsert(flag : boolean) {
    if(flag === undefined || flag === null || this._allowInsert === flag) {
      return ; 
    }

    this._allowInsert = flag;
  };
  protected _allowInsert : boolean = false;

  // Input para indicar o modo de apresentação do tree view 
  @Input() 
  get mode() : "default" | "checkbox" {
    return this._mode;
  };
  set mode(m : "default" | "checkbox") {
    if(m === undefined || !m || this._mode === m) {
      return ; 
    }

    this._mode = m;
  };
  protected _mode : "default" | "checkbox" = "checkbox";

  // Ícone para indicar se o node (caso for pai) está expandido ou collapsado
  public icon : "keyboard_arrow_down" | "keyboard_arrow_right" = "keyboard_arrow_down";

  // Propriedades para gerenciar a animação
  protected _animationStateChange : EventEmitter<AnimationEvent> = new EventEmitter<AnimationEvent>();
  protected _subs : Subscription[] = [];

  constructor(
    protected _changeDetector : ChangeDetectorRef, 
    @Inject(PLATFORM_ID) protected _platformId : Object, 
    @Inject(forwardRef(() => TreeView)) protected _tree : TreeView
  ) { 
    if(isPlatformServer(this._platformId)) {
      return ; 
    }
  };

  ngOnInit() {
    if(isPlatformServer(this._platformId)) {
      return ; 
    }

    setTimeout(() => {
      // Assinando o parent node 
      this._node.parentNode = this.parentNode;

      // Propriedades adicionais para o node 
      this._node.checked = false; // SE ESTÁ TOTALMENTE SELECIONADO (PARENT NODE - TODOS OS FILHOS SELECIONADOS)
      this._node.blocked = false; // Se está totalmente bloquado (Parent Node - Todos os filhos serão bloqueados)
      this._node.indeterminate = false; // SE ESTÁ PARCIALMENTE SELECIONADO (PARENT NODE- TODOS OS FILHOS NÃO ESTÃO SELECIONADOS)
      this._node.expanded = true; // PARA INDICAR SE O NÓ PAI ESTÁ EXPANDIDO OU COLAPSADO

      // SE O NODE POSSUIR FILHOS, ADICIONAR A PROPRIEDADE DE EXPANDIR OU NÃO 
      // A PROPRIEDADE EXPANDED ESTARÁ VINCULADA AO INPUT EXPANDED DO TREE
      if(this._node.children !== undefined) {
        this._node.expanded = this._tree.expanded;
        this._node.slideState = this._node.expanded ? 
          "down" : 
          "up";
      }

      this._initCheckedAndBlocked(
        this._node
      );

      this._changeDetector.markForCheck();

      // SUBSCRIPTION PARA MONITORAR A ANIMAÇÃO APLICADA 
      this._subs.push(
        this._animationStateChange.subscribe(($event : AnimationEvent) => {
          if($event.phaseName === "done" && $event.fromState && $event.toState) {
            this.icon = $event.toState === "down" ? 
              "keyboard_arrow_down" : 
              "keyboard_arrow_right";   
          }
        })
      );
    });
  };

  ngOnDestroy() {
    this._subs.forEach((sub : Subscription) => sub.unsubscribe());

    this._animationStateChange.complete(); 
    this._animationStateChange = null; 
  };

  /**
   * Método para inicializar a seleção do node atual 
   * @param node
   */
  protected _initCheckedAndBlocked = (node : any) : void => {
    // Conferindo se o node está selecionado ou não
    const checked : boolean = this._tree.getIndexCheckedNode(
      node
    ) !== -1 ? 
      true : 
      false;

    this._tree.updateSelection(
      this.node,
      checked
    );
  };

  /**
   * Método como handler do evento change da seleção do node, permitindo atualizar a seleção em toda a árvore
   * @param $event
   */
  public onChecked = ($event : any) : void => {
    this._tree.updateSelection(
      this._node,
      $event.target.checked
    );
  };

  /**
   * Método para expandir ou colapsar um node 
   * @param node
   */
  public toggleNode = () : void => {
    this._node.expanded = !this._node.expanded;
    this._node.slideState = this._node.expanded ? 
      "down" : 
      "up";
  };

  /**
   * Método listener para quando concluir a animação
   * @param $event 
   */
  public onAnimationDone = ($event : AnimationEvent) : void => this._animationStateChange.emit($event);

  /**
   * Método listener para quando a animação começar
   * @param $event 
   */
  public onAnimationStart = ($event : AnimationEvent) : void => this._animationStateChange.emit($event);
};
