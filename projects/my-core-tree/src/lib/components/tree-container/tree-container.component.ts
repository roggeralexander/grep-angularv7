import { 
  AfterViewInit, 
  ChangeDetectionStrategy, 
  ChangeDetectorRef, 
  Component, 
  ComponentFactoryResolver, 
  ElementRef, 
  EventEmitter,
  Inject, 
  Input, 
  OnDestroy, 
  OnInit, 
  Output, 
  PLATFORM_ID, 
  Renderer2
} from '@angular/core';

import { 
  isPlatformServer
} from "@angular/common";

import { 
  FormControl, 
  FormGroup
} from "@angular/forms";

import { 
  Observable, 
  of, 
  Subscription, 
  Subject
} from "rxjs"; 

import { 
  catchError, 
  debounceTime, 
  switchMap
} from "rxjs/operators";

// Third party modules 
import uuid from "uuid";

// Classe para geração do tree 
import { 
  Tree
} from "../../classes/tree";

// Constants 
import { 
  DEBOUNCE_FILTER_TIME
} from "../../constants";

// Models 
import { 
  NodeData, 
  TreeDataKeys
} from "../../models";

@Component({
  selector: 'mc-tree',
  templateUrl: './tree-container.component.html',
  styleUrls: ['./tree-container.component.scss'], 
  changeDetection: ChangeDetectionStrategy.Default
})
export class TreeContainerComponent implements OnInit, AfterViewInit, OnDestroy {
  // Input para representar a data básica de entrada
  @Input()  
  get data() : any[] {
    return this._data;
  };
  set data(data : any[]) {
    if(isPlatformServer(this._platformId) || data === undefined || !data || !data.length || this._data === data) {
      return ; 
    }

    this._data = data;

    setTimeout(() => {
      this._worker.next();
    });
  };
  protected _data : any[] = null;

  // Input para representar o tree data keys 
  @Input() 
  get keys() : TreeDataKeys {
    return this._keys;
  };
  set keys(keys : TreeDataKeys) {
    if(isPlatformServer(this._platformId) || keys === undefined || !keys || !Object.keys.length) {
      return ; 
    } 

    this._keys = keys;
  };
  protected _keys : TreeDataKeys = null;

  // Input para representar o idioma do sistema
  @Input() 
  get lang() : string {
    return this._lang;
  };
  set lang(lang : string) {
    if(isPlatformServer(this._platformId) || lang === undefined || !lang || !lang.trim().length || this._lang === lang) {
      return ; 
    }

    this._lang = lang;
  };
  protected _lang : string = null;

  // Input para saber se é permitido a seleção de elementos do tree ou não 
  @Input() 
  get allowSelect() : boolean {
    return this._allowSelect;
  };
  set allowSelect(flag : boolean) {
    if(flag === undefined || flag === null || this._allowSelect === flag) {
      return ; 
    }

    this._allowSelect = flag;
  };
  protected _allowSelect : boolean = false;

  // Input para permitir que seja aplicado o filter
  @Input() 
  get allowSearch() : boolean {
    return this._allowSearch;
  };
  set allowSearch(flag : boolean) {
    if(isPlatformServer(this._platformId) || flag === undefined || flag === null || this._allowSearch === flag) {
      return ; 
    }

    this._allowSearch = flag;
  };
  protected _allowSearch : boolean = false;

  // Input para especificar o placeholder do input do allow search 
  @Input() 
  get placeholder() : string | Object {
    return this._placeholder;
  };
  set placeholder(value : string | Object) {
    if(value === undefined || !value || (typeof value === "string" && !value.trim().length) || (typeof value === "object" && !Object.keys(value).length) || this._placeholder === value) {
      return ; 
    }

    this._placeholder = value;
  };
  protected _placeholder : string | Object = null;

  // Input para saber se um node pode ser atualizado ou não 
  @Input() 
  get allowEdit() : boolean {
    return this._allowEdit;
  };
  set allowEdit(flag : boolean) {
    if(flag === undefined || flag === null || this._allowEdit === flag) {
      return ; 
    }

    this._allowEdit = flag;
  };
  protected _allowEdit : boolean = false;

  // Input para permitir a inserção de um novo node 
  @Input() 
  get allowInsert() : boolean {
    return this._allowInsert;
  };
  set allowInsert(flag : boolean) {
    if(flag === undefined || flag === null || this._allowInsert === flag) {
      return ; 
    }

    this._allowInsert = flag;
  };
  protected _allowInsert : boolean = false;

  // Input para indicar os elementos que foram selecionados 
  @Input()
  get selection() : string[] | number[] {
    return this._selection;
  };
  set selection(collection : string[] | number[]) {
    if(collection === undefined || !collection || !collection.length || this._selection === collection) {
      return ; 
    }

    this._selection = collection;
  };
  protected _selection : string[] | number[] = []; 

  // Input para indicar os elementos que estão bloqueados para seleção 
  @Input() 
  get blocked() : string[] | number[] {
    return this._blocked;
  };
  set blocked(collection : string[] | number[]) {
    if(collection === undefined || !collection || !collection.length || this._blocked === collection) {
      return ; 
    }

    this._blocked = collection;
  };
  protected _blocked : string[] | number[] = [];

  // Propriedade para o label que será o placeholder 
  public get labelPlaceholder() : string {
    if(this._placeholder === undefined || !this._placeholder) {
      return "";
    }

    if(typeof this._placeholder === "string") {
      return this._placeholder;
    }

    if(this._lang === undefined || !this._lang || !this._lang.trim().length) {
      return "";
    }

    return this._placeholder[this._lang] ? 
      this._placeholder[this._lang] : 
      "";
  };

  // Propriedade para referenciar o id 
  protected readonly _id : string = `mc-tree-container-` + uuid.v4().replace(
    new RegExp("-", "gi"), 
    ""
  );

  // Propriedade para representar o form group 
  public form : FormGroup = null; 

  // Propriedade para especificar a data qye será enviada para gerar o tree view 
  public dataForTree : NodeData[] = null;

  // Propriedade para referenciar à instância do tree 
  protected _tree : Tree = null;

  // Propriedade para referenciar o input search 
  protected get _searchCtrl() : FormControl {
    if(this.form === undefined || !this.form) {
      return null;
    }

    return <FormControl>this.form.get(
      "search"
    );
  };

  // Subjects & Subscriptions 
  protected _worker : Subject<void> = new Subject();
  protected _subs : Subscription[] = [];

  // Output para indicar as seleções realizadas 
  @Output() 
  public selectionChange : EventEmitter<string[] | number[]> = new EventEmitter();

  constructor(
    protected _changeDetector : ChangeDetectorRef, 
    protected _elmRef : ElementRef, 
    @Inject(PLATFORM_ID) protected _platformId : Object, 
    protected _renderer : Renderer2, 
    protected _resolver : ComponentFactoryResolver
  ) { 
    if(isPlatformServer(this._platformId)) {
      return ; 
    }
  };

  ngOnInit() {
    if(isPlatformServer(this._platformId)) {
      return ; 
    }

    // Subscription para o worker 
    this._subs.push(
      this._worker.pipe(
        switchMap(() => this._initialize()), 
        catchError((err : Error) => {
          console.log("Error processando a data para o tree", err.message); 
          
          return of(
            undefined
          );
        })
      ).subscribe(
        () => {
          console.log("Data processada para gerar o tree", this.dataForTree);
          this._changeDetector.markForCheck();
        }
      )
    );
  };

  ngAfterViewInit() {
    if(isPlatformServer(this._platformId)) {
      return ; 
    }

    this._renderer.setAttribute(
      this._elmRef.nativeElement, 
      "id", 
      this._id
    );
  };

  ngOnDestroy() {
    this._subs.forEach((sub : Subscription) => sub.unsubscribe());

    this.selectionChange.complete();

    this._tree ?  
      this._tree.destroy() : 
      null; 

    this._tree = null; 

    this.form = null; 
  };

  /**
   * Método para inicializar o component
   */
  protected _initialize = () : Observable<void> => this._createFormSchema().pipe(
    switchMap(() => this._processData())
  );

  /**
   * Método para gerar o form schema
   */
  protected _createFormSchema = () : Observable<void> => {
    this.form = new FormGroup({
      search: new FormControl(
        null
      )
    });

    // Subscription para quando houver mudança de valor 
    this._subs.push(
      this._searchCtrl.valueChanges.pipe(
        debounceTime(
          DEBOUNCE_FILTER_TIME
        ), 
        switchMap((query : string) => this._applyFilter(
          query
        )), 
        switchMap((data : NodeData[]) => {
          console.log("Data obtida após o filtro", data);
          
          this.dataForTree = data; 
                    
          return of(
            undefined
          );
        })
      ).subscribe(
        () => {
          this._changeDetector.markForCheck();
          console.log("Busca realizada");
        }
      )
    );

    return of(
      undefined
    );
  };

  /**
   * Método para solicitar que a data seja processada para geração do tree
   */
  protected _processData = () : Observable<void> => {
    if(!this._tree) {
      this._tree = new Tree(
        this._keys
      );
    }

    // Atualizando o idioma
    this._tree.setLanguage(
      this._lang
    );

    // Solicitando a geração da lista de dados
    return this._tree.addLevelToData(
      this._data
    ).pipe(
      switchMap((data : NodeData[]) => this._tree.listToTree(
        data
      )), 
      switchMap((items : NodeData[]) => {
        this._tree.setItems(
          items
        );

        this.dataForTree = items === undefined || !items || !items.length ? 
          [] : 
          items;

        return of(
          undefined
        );
      })
    );
  };

  /**
   * Método para solicitar que seja aplicado o filtro de data do tree quando informado um query 
   * @param query
   */
  protected _applyFilter = (query : string) : Observable<NodeData[] | []> => {
    if(!this._tree) {
      return of(
        []
      );
    }

    return this._tree.filterItems(
      query
    );
  };
};
