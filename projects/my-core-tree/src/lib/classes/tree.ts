import { 
    Observable, 
    of, 
    throwError
} from "rxjs"; 

// Models 
import { 
    NodeData, 
    TreeDataKeys
} from "../models";

// Utilities 
import { 
    getNodeLevel, 
    getParentId, 
} from "../utils";

import { 
    stripStringSync
} from "@my-core/utils";

/**
 * Classe para poder obter uma estrutura de dados em forma hierárquica
 */
export class Tree {
    // Propriedade para representar a data com level (sem adição de children) para efeitos de aplicação de filtros posteriormente 
    protected _items : any[] | NodeData[] = null;

    // Propriedade para especificar o idioma 
    protected _lang : string = null; 

    constructor(
        protected _options : TreeDataKeys
    ) {};

    /**
     * Método para atualizar a collection de dados para o tree 
     * @param items
     */
    public setItems = (items : NodeData[]) : void => {
        this._items = items;
    };

    /**
     * Método para atualizar o idiomar 
     */
    public setLanguage = (lang : string) : void => {
        if(lang === undefined || !lang || this._lang === lang) {
            return ;
        }

        this._lang = lang
    };

    /**
     * Método para aidicionar o level à data
     * @param data
     */
    public addLevelToData = (data : any[]) : Observable<any[]> => {
        if(data === undefined || !data || !data.length) {
            return of(
                []
            );
        }

        // Adicionando o level à data
        const items : any[] = data.map((item : any) => Object.assign(
            {}, 
            item, 
            {
                level: getNodeLevel(
                    item["path"], 
                    this._options["separator"]
                )
            }
        )); 

        return of(
            items
        );
    };

    /**
     * Método para solicitar que seja retornada a data processada de uma list para tree
     * @param data 
     * @param lang
     */
    public listToTree = (data : any[]) : Observable<NodeData[]> => {
        if(this._options === undefined || !this._options || !Object.keys(this._options).length) {
            return throwError(
                new Error(
                    "Please inform the options to build the tree view"
                )
            );
        }

        const ID_KEY = this._options["idKey"] || "id";
        const PARENT_KEY =  this._options["parentIdKey"] || "parent";
        const CHILDREN_KEY = this._options["childrenKey"] || "children"; 
        const PATH_KEY = this._options["pathKey"] || "path";
        const NAME_KEY = this._options["nameKey"] || "name";
        const BADGE_KEY = this._options["badgeKey"] || "badge";

        let tree = [];
        let childrenOf = {};
        let id = null; 
        let parentId = null;
        let name = null; 

        data.forEach((item : NodeData, index : number) => {
            id = item[ID_KEY];

            parentId = item[PARENT_KEY] !== undefined && item[PARENT_KEY] && item[PARENT_KEY] !== 0 ?
                item[PARENT_KEY] : 
                getParentId(
                    id, 
                    this._options["separator"], 
                    item[PATH_KEY]
                ); 

            name = item[NAME_KEY];

            if(name !== undefined && name) {
                if(typeof name === "string") {
                    item["name"] = name;
                } else if(typeof name === "object") {
                    if(this._lang !== undefined && this._lang && this._lang.trim().length && typeof item["name"] === "object") {
                        item["name"] = item["name"][this._lang] !== undefined && item["name"][this._lang] && item["name"][this._lang].trim().length > 0 ? 
                            item["name"][this._lang] : 
                            "";
                    }        
                }
            }

            if(parentId !== undefined && parentId) {
                item["parentId"] = parentId;
            }

            item["badge"] = item[BADGE_KEY] || null; 
            
            childrenOf[id] = childrenOf[id] || [];

            item[CHILDREN_KEY] = childrenOf[id];

            if(parentId) {
                // init its parent's children object
                childrenOf[parentId] = childrenOf[parentId] || [];

                // push it into its parent's children object
                childrenOf[parentId].push(item);
            } else {
                tree.push(
                    item
                );
            }
        });

        return of(
            tree
        );
    };

    /**
     * Método para filtrar a data a partir de um query informado
     * @param query
     */
    public filterItems = (query?: string) : Observable<NodeData[]> => {
        if(this._items === undefined || !this._items) {
            return of(
                []
            );
        } 

        if(query === undefined || !query) {
            return of(
                this._items
            );
        }

        const filterItems : NodeData[] = [];

        this._items.forEach((item : NodeData) => {
            const newItem = this._filterItem(
                item, 
                query
            );

            if(newItem !== undefined && newItem) {
                filterItems.push(
                    newItem
                );
            }
        });

        return of(
            filterItems
        );
    };

    /**
     * Método para filtrar um item 
     * @param item 
     * @param query
     */
    protected _filterItem = (item : NodeData, query : string) : NodeData => {
        const stripQuery = stripStringSync(
            query.toLowerCase(), 
            {
                specialChars: true, 
                numberChars: true
            }
        );

        const stripName = stripStringSync(
            (<string>item["name"]).toLowerCase(), 
            {
                specialChars: true, 
                numberChars: true
            }
        );

        const isMatch = stripName.match(stripQuery) !== null ? true : false;

        if(isMatch) {
            return item;
        }

        if(item["children"] === undefined || !item["children"] || !item["children"].length) {
            return null; 
        }

        const children : NodeData[] = [];

        item["children"].forEach((child : NodeData) => {
            const newChild = this._filterItem(
                child, 
                query
            );

            if(newChild !== undefined && newChild) {
                children.push(
                    newChild
                );
            }
        });

        if(children.length > 0) {
            const newItem : NodeData = Object.assign(
                {}, 
                item, 
                {
                    children: children
                }
            );

            return newItem;
        }

        return null;
    };

    /**
     * Método para destroir a instância
     */
    public destroy = () : void => { 
        this._items = null;
        this._lang = null;
    };
};