/**
 * Método para calcular o level que um node vai possuir
 * @param path 
 * @param separator 
 */
export const getNodeLevel = (path : string, separator : string) : number => {
    if(separator === undefined || !separator || !separator.trim().length) {
        return 1;
    }

    if(path === undefined || !path) {
        return 1;
    } 

    const ocurrences = path.match(
        new RegExp(
            separator, 
            "gi"
        )
    );

    return ocurrences !== null ? 
            ocurrences.length + 1 : 
            1;
};

/**
 * Método para obter o id do parent, caso existir
 * @param id 
 * @param separator 
 * @param path 
 */
export const getParentId = (id : string | number, separator : string, path : string) : string | number => {
    if(id === undefined || id === null || separator === undefined || !separator || !separator.trim().length || path === undefined || !path || !path.trim().length) {
        return null;
    }

    const arr = path.split(
        separator
    );

    if(arr === undefined || !arr || arr.length <= 1) {
        return null;
    }

    const parentId : string = arr[arr.length - 2];

    return typeof id === "string" ? 
         parentId : 
         parseInt(parentId);
};