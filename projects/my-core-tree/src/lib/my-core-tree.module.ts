import { 
  NgModule 
} from '@angular/core';

import { 
  CommonModule
} from "@angular/common";

import { 
  FormsModule, 
  ReactiveFormsModule
} from "@angular/forms";

import {
  BrowserAnimationsModule
} from "@angular/platform-browser/animations";

// Components
import { 
  TreeContainerComponent 
} from './components/tree-container/tree-container.component';

import { 
  TreeNodeComponent 
} from './components/tree-node/tree-node.component';

import { 
  TreeViewComponent 
} from './components/tree-view/tree-view.component';

@NgModule({
  imports: [
    CommonModule, 
    FormsModule, 
    ReactiveFormsModule, 
    BrowserAnimationsModule
  ],
  declarations: [
    TreeContainerComponent,
    TreeNodeComponent,
    TreeViewComponent
  ], 
  exports: [
    TreeContainerComponent, 
    TreeViewComponent
  ], 
  entryComponents: [
    TreeContainerComponent, 
    TreeViewComponent, 
    TreeNodeComponent
  ]
})
export class MyCoreTreeModule { };
