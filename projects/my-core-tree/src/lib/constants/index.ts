// Constante para aplicar o debounce time
export const DEBOUNCE_FILTER_TIME : number = 250;

// Constantes de animação
export const ANIMATION_TIMINGS : string = '200ms cubic-bezier(0.25, 0.8, 0.25, 1)';
